<?php
	/*
	Template Name: FAQ
	*/
	
	get_header();

	get_template_part('src/components/pageheader/pageheader');
?>

<?php while( have_rows('faq_blocs') ): the_row(); ?>
	<?php
		get_template_part('src/components/accordion/accordion');
	?>
<?php endwhile; ?>

<?php
	get_template_part('src/components/help/help');

	get_footer();
?>

