<?php
	/*
	Template Name: default
	*/
	
	get_header();

	get_template_part('src/components/pageheader/pageheader');
	get_template_part('src/components/pagecontent/pagecontent');

	get_footer();
?>