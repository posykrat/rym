<?php
	/*
	Template Name: Accueil template
	*/
	
	get_header();

	get_template_part('src/components/hero/hero');
	get_template_part('src/components/arguments/arguments');
	get_template_part('src/components/livreblanc/livreblanc');
	get_template_part('src/components/features/features');
	get_template_part('src/components/getlead/getlead');
	get_template_part('src/components/whoweare/whoweare');
	get_template_part('src/components/testimonials/testimonials');
	get_template_part('src/components/partners/partners');

	set_query_var( 'getlead_prefix', 'get_lead2_');
	get_template_part('src/components/getlead/getlead');

	get_footer();
?>