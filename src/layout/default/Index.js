import Lazyload from "../../../build/node_modules/vanilla-lazyload/dist/lazyload.min.js";
import { DOMReadyObject } from "../../utils/DOMReadyObject.js";
import { Header } from "../../components/header/Header";
import "../../utils/VHMobile.js";
import { ScrollDirection } from "../../utils/ScrollDirection.js";
import DetailsPolyfill from "../../../build/node_modules/details-element-polyfill/dist/details-element-polyfill.js";
import ScrollReveal from "../../../build/node_modules/scrollreveal/dist/scrollreveal.js";

/**
 * Index
 */
class Index extends DOMReadyObject {

    constructor() {
        console.log('Index.constructor()');
        super();

        //IE 11
        this.isIE11 = !!window.MSInputMethodContext && !!document.documentMode;

        //ladyload
        (this.lazyload === undefined) ? this.lazyload = new Lazyload({ elements_selector: ".lazy" }) : this.lazyload.update();

        //Create component
        this.scrollDirection = new ScrollDirection();
        this.header          = new Header();
        
        window.sr              = new ScrollReveal();
    }

    isDOMReady() {
		console.log('Index.isDOMReady()');
        super.isDOMReady();
        this.animate();
    }

    animate(){
        var slideDown = {
            distance: '150%',
            origin: 'top',
            duration: 400,
            easing:'cubic-bezier(0.390, 0.575, 0.565, 1.000)',
            opacity: 0
        };

        var slideUp = {
            distance: '150%',
            origin: 'bottom',
            duration: 400,
            easing:'cubic-bezier(0.390, 0.575, 0.565, 1.000)',
            opacity: 0
        };

        var show = {
            duration: 1000,
            easing:'cubic-bezier(0.390, 0.575, 0.565, 1.000)',
            opacity: 0
        };

        //Hero
        window.sr.reveal('.hero_title',slideDown);
        window.sr.reveal('.hero_subtitle',{...slideDown, ...{delay:50}});
        window.sr.reveal('.hero_ctas',{...slideDown, ...{delay:150}});
        window.sr.reveal('.hero_illu',{...show, ...{delay:0,duration:450}});
        
        //Livre blanc
        window.sr.reveal('.livreblanc_image',{...show, ...{delay:0,duration:550}});
        window.sr.reveal('.livreblanc_title',{...show, ...{delay:0,duration:350}});
        window.sr.reveal('.livreblanc_desc',{...show, ...{delay:150,duration:350}});
        window.sr.reveal('.livreblanc_cta',{...show, ...{delay:150,duration:350}});

        //Arugments
        window.sr.reveal('.arguments_container',{...show, ...{delay:250,duration:150}});
        window.sr.reveal('.arguments_col',{...slideDown, ...{delay:450,duration:350,distance:'25%',interval: 5}});
        
        //Feature vidéo
        window.sr.reveal('.featurevideo_title',{...show, ...{delay:0,duration:350}});
        window.sr.reveal('.featurevideo_videocontainer',{...show, ...{delay:50,duration:350}});
    
        //Features
        window.sr.reveal('.features_title',{...show, ...{delay:0,duration:350}});
        window.sr.reveal('.features_item',{...show, ...{delay:50,duration:350, distance:'50%',interval: 5}});
        
        //Getlead
        window.sr.reveal('.getlead_title',{...slideDown, ...{delay:0,duration:350}});
        window.sr.reveal('.getlead_subtitle',{...slideDown, ...{delay:150,duration:350}});
        window.sr.reveal('.getleader_cta',{...slideDown, ...{delay:150,duration:350}});
        //window.sr.reveal('.getleader_illu',{...show, ...{delay:0,duration:550}});

        //About
        window.sr.reveal('.whoweare_image',{...show, ...{delay:0,duration:550}});
        window.sr.reveal('.whoweare_title',{...show, ...{delay:0,duration:350}});
        window.sr.reveal('.whoweare_desc',{...show, ...{delay:150,duration:350}});
        window.sr.reveal('.whoweare_cta',{...show, ...{delay:150,duration:350}});
        
        //Testimonials
        window.sr.reveal('.testimonials_title',{...show, ...{delay:0,duration:350}});
        window.sr.reveal('.testimonials_item',{...show, ...{delay:150,duration:350,interval: 5}});

        //Partners
        window.sr.reveal('.partners_title',{...show, ...{delay:0,duration:350}});
        window.sr.reveal('.partners_item',{...show, ...{delay:150,duration:350,interval: 5}});
        
        //Timeline
        window.sr.reveal('.timeline_title',{...show, ...{delay:0,duration:350}});
        window.sr.reveal('.timeline_start',{...show, ...{delay:150,duration:350}});
        window.sr.reveal('.timeline_item',{...show, ...{delay:150,duration:350}});
        window.sr.reveal('.timeline_end',{...show, ...{delay:150,duration:350}});

        //After
        window.sr.reveal('.after_subtitle',{...show, ...{delay:0,duration:350}});
        window.sr.reveal('.after_subtitle',{...show, ...{delay:150,duration:350}});
        window.sr.reveal('.after_item',{...show, ...{delay:150,duration:350,interval: 5}});
        
    }
}

export const index = new Index();