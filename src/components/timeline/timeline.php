<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;

	$timeline_title = esc_html(get_field('timeline_title'));
?>

<section class="timeline">
	<div class="timeline_container container">
		<h2 class="h1 timeline_title load-hidden"><?php echo $timeline_title; ?></h2>
		<div class="timeline_items">
			<div class="timeline_line"></div>
			<div class="timeline_start load-hidden">
				<div class="icon icon_power icon-hasCircle"><svg><use xlink:href="<?php echo Config::get('svg-sprite').'#power'; ?>"></use></svg></div>
			</div>
			<?php while( have_rows('timeline_items') ): the_row(); ?>
				<?php 
					$timeline_item_day = esc_html(get_sub_field('timeline_item_day')); 
					$timeline_item_title = esc_html(get_sub_field('timeline_item_title')); 
					$timeline_item_desc = esc_html(get_sub_field('timeline_item_desc')); 
					$timeline_item_image = get_sub_field('timeline_item_image');
					$timeline_item_image_url = esc_url($timeline_item_image['url']);
					$timeline_item_image_alt = esc_attr($timeline_item_image['alt']);
				?>
				<div class="timeline_item load-hidden">
					<div class="timeline_item_icon">
						<div class="timeline_icon_background">
							<div class="timeline_icon_day"><?php echo $timeline_item_day; ?></div>
						</div>
					</div>
					<div class="timeline_item_content">
						<div class="timeline_item_image">
							<div class="borded borded-small">
								<img src="<?php echo $timeline_item_image_url; ?>" alt="<?php echo $timeline_item_image_alt; ?>">
							</div>
						</div>
						<div class="timeline_item_info">
							<h3 class="timeline_item_title"><?php echo $timeline_item_title; ?></h3>
							<div class="timeline_item_desc"><?php echo $timeline_item_desc; ?></div>
						</div>	
					</div>
				</div>
			<?php endwhile; ?>
			<div class="timeline_end load-hidden">
				<div class="icon icon_rocket icon-hasCircle"><svg><use xlink:href="<?php echo Config::get('svg-sprite').'#rocket'; ?>"></use></svg></div>
			</div>
		</div>
		<div class="sep timeline_sep"></div>
	</div>
</section>