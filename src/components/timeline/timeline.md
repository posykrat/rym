###Components    
    
    @example
    <section class="timeline">
        <div class="timeline_container container">
			<h2 class="h1 timeline_title">Comment ça marche ?</h2>
			<div class="timeline_items">
				<div class="timeline_start">
					<div class="icon icon_power icon-hasCircle"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#power"></use></svg></div>
				</div>
				<div class="timeline_item">
					<div class="timeline_item_icon">
						<div class="timeline_icon_background">
							<div class="timeline_icon_day">J1</div>
						</div>
					</div>
					<div class="timeline_item_content">
						<div class="timeline_item_image">
							<div class="borded borded-small">
								<img src="../wp-content/themes/rym/src/assets/images/timeline1.png" alt="">
							</div>
						</div>
						<div class="timeline_item_info">
							<h3 class="timeline_item_title">Recrutement</h3>
							<div class="timeline_item_desc">
							Nous sélectionnons des futurs utilisateurs potentiels répondant à vos critères.
							</div>
						</div>	
					</div>
				</div>
				<div class="timeline_item">
					<div class="timeline_item_icon">
						<div class="timeline_icon_background">
							<div class="timeline_icon_day">J+7</div>
						</div>
					</div>
					<div class="timeline_item_content">
						<div class="timeline_item_image">
							<div class="borded borded-small">
								<img src="../wp-content/themes/rym/src/assets/images/timeline2.png" alt="">
							</div>
						</div>
						<div class="timeline_item_info">
							<h3 class="timeline_item_title">Collecte des données</h3>
							<div class="timeline_item_desc">
							Nous mettons en place des entretiens individuels, des tables rondes ou utilisons la plateforme digitale Yoomaneo pour collecter des données.
							</div>
						</div>	
					</div>
				</div>
				<div class="timeline_item">
					<div class="timeline_item_icon">
						<div class="timeline_icon_background">
							<div class="timeline_icon_day">J+14</div>
						</div>
					</div>
					<div class="timeline_item_content">
						<div class="timeline_item_image">
							<div class="borded borded-small">
								<img src="../wp-content/themes/rym/src/assets/images/timeline3.png" alt="">
							</div>
						</div>
						<div class="timeline_item_info">
							<h3 class="timeline_item_title">Analyses</h3>
							<div class="timeline_item_desc">
							Nous étudions les données pour déterminer des niveaux d'acceptabilité, des indices de désirabilités et évaluons les fonctions.
							</div>
						</div>	
					</div>
				</div>
				<div class="timeline_item">
					<div class="timeline_item_icon">
						<div class="timeline_icon_background">
							<div class="timeline_icon_day">J+21</div>
						</div>
					</div>
					<div class="timeline_item_content">
						<div class="timeline_item_image">
							<div class="borded borded-small">
								<img src="../wp-content/themes/rym/src/assets/images/timeline4.png" alt="">
							</div>
						</div>
						<div class="timeline_item_info">
							<h3 class="timeline_item_title">Livraison de l'étude</h3>
							<div class="timeline_item_desc">
							Un document complet avec nos recommandations.
							</div>
						</div>	
					</div>
				</div>
				<div class="timeline_end">
					<div class="icon icon_rocket icon-hasCircle"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#rocket"></use></svg></div>
				</div>
			</div>
			<div class="sep timeline_sep"></div>
		</div>
    </section>