###Components    
    
    @example
    <section class="accordion">
        <div class="accordion_container container">
			<h2 class="accordion_title">Demande de devis​</h2>
			<div class="accordion_items">
				<details class="accordion_item">
					<summary class="accordion_summary">
						<div class="accordion_summary_title"><h3>Comment obtenir un devis RyM ?​</h3></div>
						<div class="accordion_summary_open">
							<div class="icon icon_plus icon-isDarkBlue"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#plus"></use></svg></div>
						</div>
					</summary>
					<div class="accordion_content">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis arcu eget nisl bibendum, quis porttitor mauris posuere. Donec auctor tincidunt dolor, quis hendrerit felis tempor sed. Suspendisse volutpat imperdiet elit, nec tincidunt nibh tristique quis. Curabitur lorem est, mattis a congue et, aliquam ac dolor. Etiam ligula ipsum, luctus sed quam nec, finibus vestibulum ante.</p>
						<p> Aenean tristique sagittis commodo. Aenean eu sodales massa. Fusce ac tellus at enim mollis maximus ac nec justo. Aenean id gravida odio. Praesent varius egestas urna eget maximus. Proin euismod efficitur nisl non auctor. Curabitur ac tincidunt enim, sit amet imperdiet erat. Etiam lacinia, dui ac congue mattis, justo libero feugiat diam, et pellentesque tellus velit ac augue. Curabitur imperdiet eros sit amet ex consequat elementum. Praesent venenatis, leo ac scelerisque ultrices, lorem nisi egestas tellus, at mattis neque quam vel turpis. </p>
					</div>
					<div class="accordion_summary_close">
						<div class="icon icon_moins icon-isWhite"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#moins"></use></svg></div>
					</div>
				</details>
				<details class="accordion_item">
					<summary class="accordion_summary">
						<div class="accordion_summary_title"><h3>Les utilisateurs interrogés s’engagent-ils à respecter la confidentialité de ce qu’ils ont évalué ?​​</h3></div>
						<div class="accordion_summary_open">
							<div class="icon icon_plus icon-isDarkBlue"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#plus"></use></svg></div>
						</div>
					</summary>
					<div class="accordion_content">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis arcu eget nisl bibendum, quis porttitor mauris posuere. Donec auctor tincidunt dolor, quis hendrerit felis tempor sed. Suspendisse volutpat imperdiet elit, nec tincidunt nibh tristique quis. Curabitur lorem est, mattis a congue et, aliquam ac dolor. Etiam ligula ipsum, luctus sed quam nec, finibus vestibulum ante.</p>
						<p> Aenean tristique sagittis commodo. Aenean eu sodales massa. Fusce ac tellus at enim mollis maximus ac nec justo. Aenean id gravida odio. Praesent varius egestas urna eget maximus. Proin euismod efficitur nisl non auctor. Curabitur ac tincidunt enim, sit amet imperdiet erat. Etiam lacinia, dui ac congue mattis, justo libero feugiat diam, et pellentesque tellus velit ac augue. Curabitur imperdiet eros sit amet ex consequat elementum. Praesent venenatis, leo ac scelerisque ultrices, lorem nisi egestas tellus, at mattis neque quam vel turpis. </p>
					</div>
					<div class="accordion_summary_close">
						<div class="icon icon_moins icon-isWhite"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#moins"></use></svg></div>
					</div>
				</details>
				<details class="accordion_item">
					<summary class="accordion_summary">
						<div class="accordion_summary_title"><h3>Comment puis-je réaliser une étude sans dévoiler le nom de ma marque / mon entreprise aux utilisateurs ?​</h3></div>
						<div class="accordion_summary_open">
							<div class="icon icon_plus icon-isDarkBlue"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#plus"></use></svg></div>
						</div>
					</summary>
					<div class="accordion_content">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis arcu eget nisl bibendum, quis porttitor mauris posuere. Donec auctor tincidunt dolor, quis hendrerit felis tempor sed. Suspendisse volutpat imperdiet elit, nec tincidunt nibh tristique quis. Curabitur lorem est, mattis a congue et, aliquam ac dolor. Etiam ligula ipsum, luctus sed quam nec, finibus vestibulum ante.</p>
						<p> Aenean tristique sagittis commodo. Aenean eu sodales massa. Fusce ac tellus at enim mollis maximus ac nec justo. Aenean id gravida odio. Praesent varius egestas urna eget maximus. Proin euismod efficitur nisl non auctor. Curabitur ac tincidunt enim, sit amet imperdiet erat. Etiam lacinia, dui ac congue mattis, justo libero feugiat diam, et pellentesque tellus velit ac augue. Curabitur imperdiet eros sit amet ex consequat elementum. Praesent venenatis, leo ac scelerisque ultrices, lorem nisi egestas tellus, at mattis neque quam vel turpis. </p>
					</div>
					<div class="accordion_summary_close">
						<div class="icon icon_moins icon-isWhite"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#moins"></use></svg></div>
					</div>
				</details>
			</div>
		</div>
    </section>