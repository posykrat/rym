<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;

	$faq_bloc_title = esc_html(get_sub_field('faq_bloc_title'));
?>
<section class="accordion">
	<div class="accordion_container container">
		<h2 class="accordion_title"><?php echo $faq_bloc_title; ?></h2>
		<div class="accordion_items">
			<?php while( have_rows('faq_bloc_items') ): the_row(); ?>
			<?php
				$faq_bloc_item_question = esc_html(get_sub_field('faq_bloc_item_question'));
				$faq_bloc_item_response = get_sub_field('faq_bloc_item_response');
			?>
				<details class="accordion_item">
					<summary class="accordion_summary">
						<div class="accordion_summary_title"><h3><?php echo $faq_bloc_item_question; ?></h3></div>
						<div class="accordion_summary_open">
							<div class="icon icon_plus icon-isDarkBlue"><svg><use xlink:href="<?php echo Config::get('svg-sprite').'#plus'?>"></use></svg></div>
						</div>
					</summary>
					<div class="accordion_content"><?php echo $faq_bloc_item_response; ?></div>
					<div class="accordion_summary_close">
						<div class="icon icon_moins icon-isWhite"><svg><use xlink:href="<?php echo Config::get('svg-sprite').'#moins'?>"></use></svg></div>
					</div>
				</details>
			<?php endwhile; ?>
		</div>
	</div>
</section>
