###Components    
    
    @example
    <section class="whoweare">
        <div class="whoweare_container container">
			<div class="whoweare_image">
				<div class="borded">
					<img src="../wp-content/themes/rym/src/assets/images/whoweare.jpg" alt="">
				</div>
			</div>
			 <div class="whoweare_content">
			 	<h2 class="whoweare_title h1">Qui sommes nous ?</h2>
				<div class="whoweare_desc">
					<p><strong>Avec 20 ans d’expérience terrain,</strong> l’équipe pluridisciplinaire d’experts à l’origine de Reach your Market a déjà <strong>accompagné plus de 600 projets d’innovation. </strong>
					</p>
					<p>Reach your Market est aussi le fruit de nombreux travaux de recherche académique soutenant la conception permanente de méthodes d’évaluation des innovations et l’utilisation de l’Intelligence Artificielle, notamment par le biais du recours au Traitement Automatique du Langage (TAL) et de la création d’une plateforme digitale de collecte de l’information.​</p>
				</div>
				<a href="javascript:;" class="whoweare_cta button button-ghost">Contactez nous</a>
			 </div>
        </div>
		<div class="sep whoweare_sep"></div>
    </section>