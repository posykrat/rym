<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;

	$about_image     = get_field('about_image');
	$about_image_src = esc_url($about_image['url']);
	$about_image_alt = esc_attr($about_image['alt']);
	$about_title     = esc_html(get_field('about_title'));
	$about_desc      = get_field('about_desc');
	$about_cta_link  = esc_url(get_field('about_cta_link'));
	$about_cta_label = esc_html(get_field('about_cta_label'));
	
?>
<section class="whoweare">
	<div class="whoweare_container container">
		<div class="whoweare_image load-hidden">
			<div class="borded">
				<img src="<?php echo $about_image_src; ?>" alt="<?php echo $about_image_alt; ?>">
			</div>
		</div>
		<div class="whoweare_content">
			<h2 class="whoweare_title h1 load-hidden"><?php echo $about_title; ?></h2>
			<div class="whoweare_desc load-hidden"><?php echo $about_desc; ?></div>
			<a href="<?php echo $about_cta_link; ?>" class="whoweare_cta button button-ghost load-hidden"><?php echo $about_cta_label; ?></a>
		</div>
	</div>
	<div class="sep whoweare_sep"></div>
</section>