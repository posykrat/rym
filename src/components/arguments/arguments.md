###Components    
    
    @example
    <div class="arguments">
        <div class="arguments_container">
			<div class="arguments_cols">
				<div class="arguments_col">
					<div class="arugments_col_icon">
						<div class="icon icon_checksquare icon-isOrange"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#checksquare"></use></svg></div>
					</div>
					<div class="arguments_col_content">
						<h3 class="arguments_col_title">Tester votre projet d'innovation auprès d’utilisateurs ciblés et variés​</h3>
						<p class="arguments_col_desc">
							Nous recrutons un panel de 30 à 50 utilisateurs potentiels, spécialement sélectionnés par nos équipes selon des critères précis d'âge, de CSP, niveaux de revenus et géolocalisation.
						</p>
					</div>
				</div>
				<div class="arguments_col">
					<div class="arugments_col_icon">
						<div class="icon icon_checksquare icon-isOrange"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#checksquare"></use></svg></div>
					</div>
					<div class="arguments_col_content">
						<h3 class="arguments_col_title">Obtenir des résultats robustes et des recommandations opérationnelles​​</h3>
						<p class="arguments_col_desc">
							Nous garantissons nos résultats grâce à nos méthodologies qui ont fait l’objet de validations scientifiques, en alliant l’intelligence humaine et de l’intelligence artificielle.
						</p>
					</div>
				</div>
			</div>
		</div>
    </div>