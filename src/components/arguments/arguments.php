<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;

?>

<?php if( have_rows('arguments') ): ?>
	<div class="arguments">
		<div class="arguments_container load-hidden">
			<div class="arguments_cols">
				<?php while( have_rows('arguments') ): the_row(); ?>
					<?php
						$title = esc_html(get_sub_field('title'));
						$desc  = esc_html(get_sub_field('desc'));
					?>
					<div class="arguments_col load-hidden">
						<div class="arugments_col_icon">
							<div class="icon icon_checksquare icon-isOrange"><svg><use xlink:href="<?php echo Config::get('svg-sprite').'#checksquare'; ?>"></use></svg></div>
						</div>
						<div class="arguments_col_content">
							<h3 class="arguments_col_title "><?php echo $title; ?></h3>
							<p class="arguments_col_desc">
								<?php echo $desc; ?>
							</p>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
<?php endif; ?>