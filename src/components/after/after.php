<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;

	$after_title = esc_html(get_field('after_title'));
	$after_subtitle = esc_html(get_field('after_subtitle'));
?>
<div class="after">
	<div class="after_container">
		<h2 class="h1 after_subtitle load-hidden"><?php echo $after_title; ?></h2>
		<h2 class="after_subtitle load-hidden"><?php echo $after_subtitle; ?></h2>
		<div class="after_items">
			<?php while( have_rows('after_items') ): the_row(); ?>
				<?php
					$after_item_icon  = esc_html(get_sub_field('after_item_icon'));
					$after_item_titre = esc_html(get_sub_field('after_item_titre'));
					$after_item_tdesc = esc_html(get_sub_field('after_item_tdesc'));
				?>
				<div class="after_item load-hidden">
					<div class="after_item_icon">
						<div class="icon icon_userplus icon-hasCircle"><svg><use xlink:href="<?php echo Config::get('svg-sprite').'#'.$after_item_icon?>"></use></svg></div>
					</div>
					<h4 class="after_item_title"><?php echo $after_item_titre; ?></h4>
					<div class="after_item_desc"><?php echo $after_item_tdesc; ?></div>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</div>
