###Components    
    
    @example
    <div class="after">
        <div class="after_container">
			<h2 class="h1 after_subtitle">Et après ?</h2>
			<h2 class="after_subtitle">Que ce soit au démarrage ou à l’issue de votre étude RyM, vous pouvez accéder à plusieurs options :</h2>
			<div class="after_items">
				<div class="after_item">
					<div class="after_item_icon">
						<div class="icon icon_userplus icon-hasCircle"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#userplus"></use></svg></div>
					</div>
					<h4 class="after_item_title">Interaction</h4>
					<div class="after_item_desc">Vous permet d’interagir avec les participants (durant l’étude) ou de débriefer avec eux (après l’étude).</div>
				</div>
				<div class="after_item">
					<div class="after_item_icon">
						<div class="icon icon_dollarsign icon-hasCircle"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#dollarsign"></use></svg></div>
					</div>
					<h4 class="after_item_title">Consentement à payer</h4>
					<div class="after_item_desc">Pour obtenir des données chiffrées sur le prix d’acceptabilité de votre innovation.</div>
				</div>
				<div class="after_item">
					<div class="after_item_icon">
						<div class="icon icon_edit icon-hasCircle"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#edit"></use></svg></div>
					</div>
					<h4 class="after_item_title">Questionnaire</h4>
					<div class="after_item_desc">Pour aller plus loin en soumettant de nouveaux questionnements, pour interroger une nouvelle cible ou pour tester une alternative à votre innovation.</div>
				</div>
				<div class="after_item">
					<div class="after_item_icon">
						<div class="icon icon_book icon-hasCircle"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#book"></use></svg></div>
					</div>
					<h4 class="after_item_title">Formation</h4>
					<div class="after_item_desc">Vous permet d’accéder à une formation en ligne pour en savoir plus sur les tests d’acceptabilité et/ou de désirabilité des innovations. </div>
				</div>
			</div>
		</div>
    </div>