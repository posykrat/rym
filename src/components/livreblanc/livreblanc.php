<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;
?>

<div class="livreblanc">
	<div class="livreblanc_container">
		<div class="livreblanc_image load-hidden">
			<img src="<?php echo esc_url(get_field('lb_image')['url']); ?>" alt="<?php echo esc_url(get_field('lb_image')['alt']); ?>">
			<a href="<?php echo esc_url(get_field('lb_preview_link')); ?>" target="_blank" class="livreblanc_previewlink">
				<div class="icon icon_find icon-hasCircle icon-hasBgWhite"><svg><use xlink:href="<?php echo get_template_directory_uri().'/src/assets/svg/sprite.svg#find';?>"></use></svg></div>
			</a>
		</div>
		<div class="livreblanc_content">
			<h2 class="livreblanc_title load-hidden"><?php echo esc_html(get_field('lb_title')); ?></h2>
			<div class="livreblanc_desc load-hidden">
				<?php echo get_field('lb_description'); ?>
			</div>
			<a href="<?php echo esc_url(get_field('lb_cta_link')); ?>" class="livreblanc_cta button button-ghost button-small load-hidden"><?php echo esc_html(get_field('lb_cta_label')); ?></a>
		</div>
	</div>
</div>