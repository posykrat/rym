<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;

	$help_title     = esc_html(get_field('help_title'));
	$help_subtile   = esc_html(get_field('help_subtile'));
	$help_cta_link  = esc_url(get_field('help_cta_link'));
	$help_cta_link  = esc_url(get_field('help_cta_link'));
	$help_cta_label = esc_html(get_field('help_cta_label'));
?>

<div class="help">
	<div class="help_container">
		<div class="sep help_sep"></div>
		<h2 class="h1 help_title"><?php echo $help_title; ?></h2>
		<?php if($help_subtile){ ?>
			<h3 class="h2 help_subtitle"><?php echo $help_subtile; ?></h3>
		<?php } ?>
		<a href="<?php echo $help_cta_link; ?>" class="button help_cta"><?php echo $help_cta_label; ?></a>
	</div>
</div>
