###Components    
    
    @example
    <div class="help">
        <div class="help_container">
			<div class="sep help_sep"></div>
			<h2 class="h1 help_title">Besoin d'aide ?</h2>
			<h3 class="h2 help_subtitle">On parle français, anglais et on répond habituellement sous 24h !</h3>
			<a href="javascript:;" class="button help_cta">Obtenir un devis</a>
		</div>
    </div>