<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;

	$page_icon = esc_html(get_field('page_icon'));
	$page_title = esc_html(get_field('page_title'));
	$page_subtitle = esc_html(get_field('page_subtitle'));
?>
<section class="pageheader">
	<div class="pageheader_container container">
		<?php if($page_icon){ ?>
			<div class="pageheader_icon">
				<div class="icon icon_<?php echo $page_icon; ?> icon-hasCircle"><svg><use xlink:href="<?php echo Config::get('svg-sprite').'#'.$page_icon; ?>"></use></svg></div>
			</div>
		<?php } ?>
		<h1 class="pageheader_title"><?php echo $page_title; ?></h1>
		<?php if($page_subtitle){ ?>
			<h2 class="pageheader_subtitle"><?php echo $page_subtitle; ?></h2>
		<?php } ?>
	</div>
</section>
