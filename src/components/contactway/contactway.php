<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;
?>

<div class="contactway">
	<div class="contactway_container">
		<div class="contactway_items">
			<?php while( have_rows('conctactway_items') ): the_row(); ?>
				<?php
					$icon                               = esc_html(get_sub_field('conctactway_item_icon'));
					$conctactway_item_link              = get_sub_field('conctactway_item_link');
					$conctactway_item_link_title        = esc_html( $conctactway_item_link['title'] );
					$conctactway_item_link_title_target = $conctactway_item_link['target'] ? $conctactway_item_link['target'] : '_self';
					$conctactway_item_link_title_target = esc_attr($conctactway_item_link_title_target);
					$conctactway_item_link_url          = esc_html($conctactway_item_link['url']);
					$conctactway_item_desc              = esc_html(get_sub_field('conctactway_item_desc'));
					$conctactway_item_desc              = esc_html(get_sub_field('conctactway_item_desc'));
				?>
				<div class="contactway_item">
					<div class="contactway_item_icon">
						<div class="icon icon_<?php echo $icon; ?> icon-hasCircle">
							<svg><use xlink:href="<?php echo Config::get('svg-sprite').'#'.$icon;?>"></use></svg>
						</div>
					</div>
					<h4 class="contactway_item_title"><a href="<?php echo $conctactway_item_link_url; ?>" class="contactway_item_link" target="<?php echo $conctactway_item_link_title_target; ?>"><?php echo $conctactway_item_link_title; ?></a></h4>
					<div class="contactway_item_desc"><?php echo $conctactway_item_desc; ?></div>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</div>

