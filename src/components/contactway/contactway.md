###Components    
    
    @example
    <div class="contactway">
        <div class="contactway_container">
			<div class="contactway_items">
				<div class="contactway_item">
					<div class="contactway_item_icon">
						<div class="icon icon_phone icon-hasCircle"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#phone"></use></svg></div>
					</div>
					<h4 class="contactway_item_title"><a href="javascript:;" class="contactway_item_link">Nous appeler<br>+33 4 76 25 94 70</a></h4>
					<div class="contactway_item_desc">Du lundi au vendredi, de 8h à 17h.</div>
				</div>
				<div class="contactway_item">
					<div class="contactway_item_icon">
						<div class="icon icon_mail icon-hasCircle"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#mail"></use></svg></div>
					</div>
					<h4 class="contactway_item_title"><a href="javascript:;" class="contactway_item_link">Nous écrire contact@rym.com</a></h4>
					<div class="contactway_item_desc">Une réponse sous 24h !</div>
				</div>
				<div class="contactway_item">
					<div class="contactway_item_icon">
						<div class="icon icon_linkedin icon-hasCircle"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#linkedin"></use></svg></div>
					</div>
					<h4 class="contactway_item_title"><a href="javascript:;" class="contactway_item_link">Retrouvez nous sur LinkedIn</a></h4>
					<div class="contactway_item_desc">Suivez l'actualité de RyM.</div>
				</div>
			</div>
		</div>
    </div>