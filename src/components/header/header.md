###Components    
    
    @example
    <header class="header">
        <div class="header_container container">
            <a href="javascript:;" class="header_logo"><div class="logo"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/logo.svg#logo"></use></svg></div></a>
            <nav class="header_nav">
                <ul class="header_nav_items">
                    <li class="header_nav_item"><a class="header_nav_link" href="javascript:;">Accueil</a></li>
                    <li class="header_nav_item"><a class="header_nav_link" href="javascript:;">Qui sommes nous ?</a></li>
                    <li class="header_nav_item"><a class="header_nav_link" href="javascript:;">FAQ</a></li>
                </ul>
            </nav>
            <nav class="header_toggle">
                <a href="javascript:;" class="header_toggle_burger">
                    <div class="icon icon_burger icon-hasCircle"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#list"></use></svg></div>
                </a>
                <a href="javascript:;" class="header_toggle_close">
                <div class="icon icon_plus icon-hasCircle"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#plus"></use></svg></div></a>
            </nav>
            <a href="javascript:;" class="header_cta button button-ghost button-small">Contactez-nous</a>
        </div>
    </header>