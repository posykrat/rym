import { DOMReadyObject } from '../../utils/DOMReadyObject.js';

/**
 * Header component
 */
export class Header extends DOMReadyObject {

    constructor() {
        console.log('1/ Header', 'constructor()');
        super();
		this.class = '.header';
		this.firstMove = true;
        this.isOpenBurger = false;
    }

    isDOMReady() {
        super.isDOMReady();
        this.initView();
    }

    initView(){
        console.log('Header', 'initView()');
        this.$dom = document.querySelector(this.class);
        this.$burger = document.querySelector('.header_toggle');
        
        //Si on a bien un header
        if (this.$dom != null){
			this.initBurger();
			this.initEvents();
        }
	}
	
	initEvents(){	

		//Au scroll si la direction a changée
        window.addEventListener('ScrollDirection_direction', (e) => {

			console.log(e.detail);

            //Si le menu est pas déjà ouvert
            if (this.$dom.classList.contains('header-open') == false) {

                //Si on va vers le haut
                if (e.detail.direction == "up") {
                    this.show();
                }

                //Si on va vers le bas
                // ou que c'est le premier déplacement et qu'on a suffisament scrollé
                else if ((e.detail.direction == "down") && (e.detail.position > 0)) {
                    this.hide();
                }

            }
        });
	}

	 //Afficher le header
	 show(){
        console.log('Header.show()');
        if (this.$dom.classList.contains('header-show') == false) {
            if (this.$dom.classList.contains('header-hidefirstmove')) {
                this.$dom.classList.remove('header-hidefirstmove');
            }
            else{
                this.$dom.classList.remove('header-hide');
            }
            this.$dom.classList.add('header-show');
        }
    }

    //Cacher le header
    hide(){
        console.log('Header.hide()');
        if (this.$dom.classList.contains('header-show') == true || this.firstMove == true) {
            if (this.firstMove == true){
                this.$dom.classList.add('header-hidefirstmove');
                this.firstMove = false;
            }else{
                this.$dom.classList.add('header-hide');
            }
            this.$dom.classList.remove('header-show');
        }
    }

    //Toggle class header-openburger
    initBurger(){
        console.log('Header', 'initBurger');
        if (this.$burger != null) {
            this.boundOnClickBurger = e => this.onClickBurger(e);
            this.$burger.removeEventListener('click', this.boundOnClickBurger);
            this.$burger.addEventListener('click', this.boundOnClickBurger);
        } 
    }

    onClickBurger(e){

        //State
        this.isOpenBurger = this.$dom.classList.toggle('header-open');
        console.log('Header', 'onClickBurger', ' / open burger ?', this.isOpenBurger);

        //Toggle noscroll
        this.toggleNoScroll();

        e.preventDefault();
        e.stopPropagation();
    }

    //Toggle no scroll sur le body
    toggleNoScroll(){
        console.log('Header.toggleNoScroll()');
        if (this.isOpenBurger === true) {
            (new Promise((resolve) => setTimeout(resolve, 500))).then((e) => {
                document.body.classList.add('noscroll');
            });
        }
        else {
            document.body.classList.remove('noscroll');
        }
    }
}