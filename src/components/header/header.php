<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;

	//Permalien courant
	global $wp;
	/* $currentPermalink = (is_front_page() === true) ? home_url($wp->request) : home_url($wp->request . '/'); */
	$currentPermalink = home_url($wp->request . '/');

	//Un seul élément courant
	$isCurrented = false;
?>

<header class="header">
	<div class="header_container container">
		<a href="<?php echo get_site_url(); ?>" class="header_logo"><div class="logo"><svg><use xlink:href="<?php echo Config::get('svg-path').'logo.svg#logo'; ?>"></use></svg></div></a>
		<?php if( have_rows('elements', 'options') ): ?>
			<nav class="header_nav">
				<ul class="header_nav_items">
					<?php while( have_rows('elements', 'options') ): the_row(); ?>					
						<?php
							$element_link  = esc_url(get_sub_field('element_link', 'options'));
							$element_label = esc_html(get_sub_field('element_label', 'options'));
							$currentClass  = ($currentPermalink == $element_link && $isCurrented === false) ? 'header_nav_link-current' : '';
							printf(
								'<li class="header_nav_item"><a href="%s" class="header_nav_link %s">%s</a></li>',
								$element_link,
								$currentClass,
								$element_label
							);
						?>
					<?php endwhile; ?>
				</ul>
			</nav>
		<?php endif; ?>					
		<nav class="header_toggle">
			<a href="javascript:;" class="header_toggle_burger">
				<div class="icon icon_burger icon-hasCircle"><svg><use xlink:href="<?php echo Config::get('svg-sprite').'#list'; ?>"></use></svg></div>
			</a>
			<a href="javascript:;" class="header_toggle_close">
			<div class="icon icon_plus icon-hasCircle"><svg><use xlink:href="<?php echo Config::get('svg-sprite').'#plus'; ?>"></use></svg></div></a>
		</nav>
		<?php
			$cta_link = get_field('cta_link', 'options');
			$cta_label = get_field('cta_label', 'options');
			printf(
				'<a href="%s" class="header_cta button button-ghost button-small">%s</a>',
				$cta_link,
				$cta_label
			);
		?>
	</div>
</header>