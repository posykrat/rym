<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;

	$title        = get_field('title');
	$subtitle     = get_field('subtitle');
	$cta11_link   = esc_url(get_field('cta11_link'));
	$cta11_label  = esc_html(get_field('cta11_label'));
	$cta22_link   = esc_url(get_field('cta22_link'));
	$cta22_label  = esc_html(get_field('cta22_label'));
	$illustration = get_field('illustration');
?>
<section class="hero hero-<?php echo $illustration; ?>">
	<div class="hero_container container">
		<div class="hero_txt">
			<h1 class="hero_title load-hidden"><?php echo $title; ?></h1>
			<h2 class="hero_subtitle load-hidden"><?php echo $subtitle; ?></h2>
			<?php
				if(is_front_page() === true){
			?>
				<div class="hero_ctas load-hidden">
					<a href="<?php echo $cta11_link; ?>" class="hero_cta button"><?php echo $cta11_label; ?></a>
					<a href="<?php echo $cta22_link; ?>" class="hero_cta button button-ghost"><?php echo $cta22_label; ?></a>
				</div>
			<?php 
				} 
			?>
		</div>
		<?php

			$svgtarget = Config::get('svg-path').$illustration.'.svg#'.$illustration;	
			if(DFWP_ENV === 'preprod'){
				$context = stream_context_create(array (
					'http' => array (
						'header' => 'Authorization: Basic ' . base64_encode(DFWP_HTACCESS_USER.':'.DFWP_HTACCESS_PASS)
					)
				));
				$svgfile = file_get_contents($svgtarget, false, $context);
			}else{
				$svgfile = file_get_contents($svgtarget);
			}
			echo '<div class="hero_illu load-hidden">';			
				echo $svgfile;
			echo '</div>';
		?>
	</div>
	<div class="hero_shadow"></div>
</section>