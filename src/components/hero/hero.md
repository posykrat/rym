###Components    
    
    @example
    <section class="hero hero-radar">
        <div class="hero_container container">
			<div class="hero_txt">
				<h1 class="hero_title">Ã‰valuer le potentiel de marchÃ© de votre innovation.</h1>
				<h2 class="hero_subtitle">Reach your Market est un e-service dâ€™Ã©tude permettant de sonder rapidement les futurs utilisateurs pour augmenter vos chances de rÃ©ussite.</h2>
				<div class="hero_ctas">
					<a href="javascript:;" class="hero_cta button">Obtenir un devis</a>
					<a href="javascript:;" class="hero_cta button button-ghost">Contactez nous</a>
				</div>
			</div>
			<div class="hero_illu hero_illu-radar">
				<svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/radar.svg#radar"></use></svg>
			</div>
        </div>
    </section>