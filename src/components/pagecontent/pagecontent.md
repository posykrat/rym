###Components    
    
    @example
    <section class="pagecontent">
        <div class="pagecontent_container container">
			<p>Le site Internet Reach-your-Market est déclaré à la Commission Nationale Informatique et Libertés (CNIL) sous le numéro xxxx et respecte les dispositions de la loi 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.​</p>

			<p>Reach-your-Market prend en compte et respecte le souhait de ses visiteurs de protéger leur vie privée sur Internet. En ce sens, le site Internet Reach-your-Market protège la confidentialité des informations permettant d’identifier personnellement ses visiteurs et ses utilisateurs.​</p>
			<h4>Lorem ipsum</h4>
			<p>Reach-your-Market est à l’initiative de la société Ixiade :​
			Statut : Société par Actions Simplifiée​
			Siège social : 11 rue Aimé Berey, 38000 Grenoble – France​
			Téléphone : +33 4 76 25 94 70 ​
			Code APE : 7022Z​
			SIRET : 482 549 383 00040​
			Capital social : 52 270€​
			Numéro TVA intracommunautaire : FR82 482 549 383​
			Adresse de courrier électronique : contact@ixiade.com​
			Directeur de la publication : Pascal Pizelle​</p>

			<p>Reach-your-Market est hébergé par OVH :​
			Statut : Société par Actions Simplifiée​
			Siège social : 2 rue Kellermann, 59100 Roubaix – France​
			Code APE : 6202A​
			N° TVA : FR 26 510 258 64​</p>
			<h4>Lorem ipsum</h4>
			<p>Le site Internet Reach-your-Market informe ses visiteurs et ses utilisateurs qu’un ou des cookies sont susceptibles de s’installer automatiquement sur leur ordinateur lors de leur visite. Un cookie est un fichier de petite taille, qui ne permet pas l’identification de l’utilisateur, mais qui enregistre des informations relatives à sa navigation sur un site. Reach-your-Market utilise ces cookies afin de permettre à ses utilisateurs de personnaliser leur expérience en ligne.​</p>
		</div>
    </section>