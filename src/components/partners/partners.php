<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;
	
	$partners_title = esc_html(get_field('partners_title'));
?>

<section class="partners">
	<div class="partners_container container">
		<h2 class="partners_title load-hidden"><?php echo $partners_title; ?></h2>
		<div class="partners_items">
			<?php while( have_rows('partners_items') ): the_row(); ?>
				<?php
					$partners_item_image     = get_sub_field('partners_item_image');
					$partners_item_image_url = esc_url($partners_item_image['url']);
					$partners_item_image_alt = esc_attr($partners_item_image['alt']);
					$partners_item_url       = esc_url(get_sub_field('partners_item_url'))
				?>
				<a class="partners_item load-hidden" href="<?php echo $partners_item_url; ?>" target="_blank">
					<img src="<?php echo $partners_item_image_url; ?>" alt="<?php echo $partners_item_image_alt; ?>">
				</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>