###Components    
    
    @example
    <section class="partners">
        <div class="partners_container container">
			<h2 class="partners_title">Partenariats, affiliations, accréditations</h2>
			<div class="partners_items">
				<a class="partners_item" href="javascript:;">
					<img src="../wp-content/themes/rym/src/assets/images/esomar.png" alt="">
				</a>
				<a class="partners_item" href="javascript:;">
					<img src="../wp-content/themes/rym/src/assets/images/cautic.png" alt="">
				</a>
				<a class="partners_item" href="javascript:;">
					<img src="../wp-content/themes/rym/src/assets/images/cnrs.png" alt="">
				</a>
				<a class="partners_item" href="javascript:;">
					<img src="../wp-content/themes/rym/src/assets/images/eminosa.png" alt="">
				</a>
				<a class="partners_item" href="javascript:;">
					<img src="../wp-content/themes/rym/src/assets/images/cii.png" alt="">
				</a>
				<a class="partners_item" href="javascript:;">
					<img src="../wp-content/themes/rym/src/assets/images/cir.png" alt="">
				</a>
			</div>
        </div>
    </section>