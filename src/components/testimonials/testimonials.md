###Components    
    
    @example
    <section class="testimonials">
        <div class="testimonials_container container">
			<h2 class="h1 testimonials_title">Ils parlent de nous</h2>
			<div class="testimonials_items">
				<div class="testimonials_item">
					<div class="testimonials_item_logo">
						<img src="../wp-content/themes/rym/src/assets/images/e5e5ab65458b01185be93885bf3cbfca.png" alt="">
					</div>
					<div class="testimonials_item_desc">One of the big advantages of Spell was our team could get up and running right away without slowing down ongoing projects. Spell plugged in seamlessly to our existing infrastructure and tools and we could migrate workloads as we saw more and more value.</div>
					<div class="testimonials_item_about">
						<h4 class="testimonials_item_name">Chris Ackerson</h4>
						<div class="testimonials_item_position">Director of Product at Alphasense</div>
					</div>
				</div>
				<div class="testimonials_item">
					<div class="testimonials_item_logo">
						<img src="../wp-content/themes/rym/src/assets/images/b5d832971908ca749046eb69ef74aa1d.png" alt="">
					</div>
					<div class="testimonials_item_desc">At this point, I've trained 100's of models on Spell. It's amazing to never have to think about infrastructure - I feel like I can go directly from idea to training a model. There's a lot of talk about the democratization of machine learning, but in practice it's quite difficult to set up the necessary building blocks to train much more than a toy model. Spell seems like a real step forward in making machine learning approachable for more people.</div>
					<div class="testimonials_item_about">
						<h4 class="testimonials_item_name">Danny Meyer</h4>
						<div class="testimonials_item_position">ML Engineer at Originate</div>
					</div>
				</div>
				<div class="testimonials_item">
					<div class="testimonials_item_logo">
						<img src="../wp-content/themes/rym/src/assets/images/3758d440ba44238889b6bb81321ccd4c.png" alt="">
					</div>
					<div class="testimonials_item_desc">Making developer tools is a huge passion of mine. When I first saw Spell, I was blown away by how simple and elegant it was. Having wrestled with provisioning and configuring ML environments a lot, I immediately understood the power of Spell. It really feels like magic. I'm thrilled to be partnering with Spell and...</div>
					<div class="testimonials_item_about">
						<h4 class="testimonials_item_name">Chris Van Pelt</h4>
						<div class="testimonials_item_position">Co-founder at Weights & Biases</div>
					</div>
				</div>
				<div class="testimonials_item">
					<div class="testimonials_item_logo">
						<img src="../wp-content/themes/rym/src/assets/images/3758d440ba44238889b6bb81321ccd4c.png" alt="">
					</div>
					<div class="testimonials_item_desc">Making developer tools is a huge passion of mine. When I first saw Spell, I was blown away by how simple and elegant it was. Having wrestled with provisioning and configuring ML environments a lot, I immediately understood the power of Spell. It really feels like magic. I'm thrilled to be partnering with Spell and...</div>
					<div class="testimonials_item_about">
						<h4 class="testimonials_item_name">Chris Van Pelt</h4>
						<div class="testimonials_item_position">Co-founder at Weights & Biases</div>
					</div>
				</div>
			</div>
        </div>
    </section>