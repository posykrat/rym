<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;
	
	$testimonials_title = esc_html(get_field('testimonials_title'));
?>

<?php if ( get_field('testimonials_active') ) : ?>
	<section class="testimonials">
		<div class="testimonials_container container">
			<h2 class="h1 testimonials_title load-hidden"><?php echo $testimonials_title; ?></h2>
			<?php if( have_rows('testimonials_items') ): ?>
				<div class="testimonials_items">
					<?php while( have_rows('testimonials_items') ): the_row(); ?>
						<?php
							$testimonials_item_image     = get_sub_field('testimonials_item_image');
							$testimonials_item_image_url = esc_url($testimonials_item_image['url']);
							$testimonials_item_image_alt = esc_attr($testimonials_item_image['alt']);
							$testimonials_item_citation  = esc_html(get_sub_field('testimonials_item_citation'));
							$testimonials_item_name      = esc_html(get_sub_field('testimonials_item_name'));
							$testimonials_item_position  = esc_html(get_sub_field('testimonials_item_position'));
						?>
						<div class="testimonials_item load-hidden">
							<div class="testimonials_item_logo">
								<img src="<?php echo $testimonials_item_image_url; ?>" alt="<?php echo $testimonials_item_image_alt; ?>">
							</div>
							<div class="testimonials_item_desc"><?php echo $testimonials_item_citation; ?></div>
							<div class="testimonials_item_about">
								<h4 class="testimonials_item_name"><?php echo $testimonials_item_name; ?></h4>
								<div class="testimonials_item_position"><?php echo $testimonials_item_position; ?></div>
							</div>
						</div>	
					<?php endwhile; ?>		
				</div>
			<?php endif; ?>
		</div>
	</section>
 <?php endif; ?>
