<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;
?>

<div class="features">
	<div class="features_container">
		<?php
			$title = esc_html(get_field('features_title'));
		?>
		<h2 class="features_title h1 load-hidden"><?php echo $title; ?></h2>
		<div class="features_items">
			<?php while( have_rows('features_items') ): the_row(); ?>
				<?php
					$icon = esc_html(get_sub_field('icon'));
					$desc  = get_sub_field('desc');
				?>
				<div class="features_item load-hidden">
					<div class="features_item_icon">
						<div class="icon icon_<?php echo $icon; ?> icon-hasCircle">
							<svg><use xlink:href="<?php echo get_template_directory_uri().'/src/assets/svg/sprite.svg#'.$icon;?>"></use></svg>
						</div>
					</div>
					<div class="features_item_desc"><?php echo $desc; ?></div>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</div>

