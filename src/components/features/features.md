###Components    
    
    @example
    <div class="features">
        <div class="features_container">
			<h2 class="features_title h1">Maximiser vos chances de succès</h2>
			<div class="features_items">
				<div class="features_item">
					<div class="features_item_icon">
						<div class="icon icon_analyz icon-hasCircle">
  							<svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#analyz"></use></svg>
						</div>
					</div>
					<div class="features_item_desc"><strong>Analyser</strong> l’adéquation de votre projet aux savoir-faire, aux pratiques, à l’identité et au contexte socio-économique des utilisateurs. Quisque odio sapien, auctor ut nisl.</div>
				</div>
				<div class="features_item">
					<div class="features_item_icon">
						<div class="icon icon_piechart icon-hasCircle">
							<svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#piechart"></use></svg>
						</div>
					</div>
					<div class="features_item_desc"><strong>Évaluer</strong> la désirabilité de votre innovation. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dictum turpis velit, vel ultrices nisl pretium ac.</div>
				</div>
				<div class="features_item">
					<div class="features_item_icon">
						<div class="icon icon_edit icon-hasCircle">
							<svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#edit"></use></svg>
						</div>
					</div>
					<div class="features_item_desc"><strong>Spécifier</strong> des axes de design émotionnel. Donec non magna sed mauris scelerisque consequat dignissim at lacus. Donec lobortis tempor lorem vitae porta.</div>
				</div>
				<div class="features_item">
					<div class="features_item_icon">
						<div class="icon icon_shuffle icon-hasCircle">
							<svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#shuffle"></use></svg>
						</div>
					</div>
					<div class="features_item_desc"><strong>Déterminer</strong> les fonctions qui sont le plus et le moins appréciées, leurs forces et leurs faiblesses. Pellentesque venenatis quam vestibulum suscipit consectetur.</div>
				</div>
			</div>
		</div>
    </div>