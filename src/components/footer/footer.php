<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;

	$baseline        = esc_html(get_field('baseline', 'options'));
	$phone           = get_field('phone', 'options');
	$phone_url       = esc_url($phone['url']);
	$phone_title     = esc_html($phone['title']);
	$phone_target    = esc_attr($phone['target']);
	$contact         = get_field('contact', 'options');
	$contact_url     = esc_url($contact['url']);
	$contact_title   = esc_html($contact['title']);
	$contact_target  = esc_attr($contact['target']);
	$linkedin        = get_field('linkedin', 'options');
	$linkedin_url    = esc_url($linkedin['url']);
	$linkedin_title  = esc_html($linkedin['title']);
	$linkedin_target = esc_attr($linkedin['target']);
?>

<footer class="footer">
	<div class="footer_container container">
		<div class="footer_logo">
			<div class="logosmall">
				<svg><use xlink:href="<?php echo Config::get('svg-path').'logosmall.svg#logosmall'; ?>"></use></svg>
			</div>
			<p class="footer_baseline"><?php echo $baseline; ?></p>
		</div>
		<div class="footer_cols">
			<ul class="footer_col">
				<li class="footer_col_item">
					<a class="footer_col_link footer_col_link-hasIcon" href="<?php echo $phone_url; ?>" target="<?php echo $phone_target; ?>">
						<div class="footer_item_icon footer_item_icon-phone icon icon_phone"><svg><use xlink:href="<?php echo Config::get('svg-sprite').'#phone'; ?>"></use></svg></div>
						<span><?php echo $phone_title; ?></span></a>
					</li>
				<li class="footer_col_item">
					<a class="footer_col_link footer_col_link-hasIcon" href="<?php echo $contact_url; ?>" target="<?php echo $contact_target; ?>">
						<div class="footer_item_icon footer_item_icon-mail icon icon_mail"><svg><use xlink:href="<?php echo Config::get('svg-sprite').'#mail'; ?>"></use></svg></div>
						<span><?php echo $contact_title; ?></span>
					</a>
				</li>
				<li class="footer_col_item">
					<a class="footer_col_link footer_col_link-hasIcon" href="<?php echo $linkedin_url; ?>" target="<?php echo $linkedin_target; ?>">
						<div class="footer_item_icon footer_item_icon-social icon icon_lkdin "><svg><use xlink:href="<?php echo Config::get('svg-sprite').'#linkedin'; ?>"></use></svg></div>
						<span><?php echo $linkedin_title; ?></span>
					</a>
				</li>
			</ul>
			<?php if( have_rows('column1_links', 'options') ): ?>
				<ul class="footer_col">
					<?php while( have_rows('column1_links', 'options') ): the_row(); ?>
						<?php
							$link = esc_url(get_sub_field('link', 'options'));
							$label = esc_html(get_sub_field('label', 'options'));

							printf(
								'<li class="footer_col_item"><a class="footer_col_link" href="%s">%s</a></li>',
								$link,
								$label
							);
						?>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>
			<?php if( have_rows('column2_links', 'options') ): ?>
				<ul class="footer_col">
					<?php while( have_rows('column2_links', 'options') ): the_row(); ?>
						<?php
							$link = esc_url(get_sub_field('link', 'options'));
							$label = esc_html(get_sub_field('label', 'options'));

							printf(
								'<li class="footer_col_item"><a class="footer_col_link" href="%s">%s</a></li>',
								$link,
								$label
							);
						?>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>
		<div class="footer_langs">
			<?php 
				$languages = pll_the_languages(array(
					'show_flags' => true,
					'hide_current' => true,
					'force_home'=>true,
					'raw' => true
				));
				foreach ($languages as $language) {
					printf(
						'<a href="%s">%s %s</a>',
						$language['url'],
						$language['flag'],
						$language['name']
					);
				}
			?>
		</div>
	</div>
</footer>