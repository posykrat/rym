###Components    
    
    @example
    <footer class="footer">
        <div class="footer_container container">
            <div class="footer_logo">
                <div class="logosmall">
                    <svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/logosmall.svg#logosmall"></use></svg>
                </div>
                <p class="footer_baseline">L’alliance de l’intelligence humaine et de l’intelligence artificielle au service des innovations​</p>
            </div>
            <div class="footer_cols">
                <ul class="footer_col">
                    <li class="footer_col_item">
                        <a class="footer_col_link footer_col_link-hasIcon" href="tel:+33476259470">
                            <div class="footer_item_icon footer_item_icon-phone icon icon_phone"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#phone"></use></svg></div>
                            <span>Tél. : +33 4 76 25 94 70</span></a>
                        </li>
                    <li class="footer_col_item">
                        <a class="footer_col_link footer_col_link-hasIcon" href="mailto:contact@rym.com">
                            <div class="footer_item_icon footer_item_icon-mail icon icon_mail"><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#mail"></use></svg></div>
                            <span>contact@rym.com</span>
                        </a>
                    </li>
                    <li class="footer_col_item">
                        <a class="footer_col_link footer_col_link-hasIcon" href="https://www.linkedin.com">
                            <div class="footer_item_icon footer_item_icon-linkedin icon icon_linkedin "><svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/sprite.svg#linkedin"></use></svg></div>
                            <span>Retrouvez nous sur LinkedIn</span>
                        </a>
                    </li>
                </ul>
                <ul class="footer_col">
                    <li class="footer_col_item"><a class="footer_col_link" href="javascript:;">Comment ça marche ?</a></li>
                    <li class="footer_col_item"><a class="footer_col_link" href="javascript:;">FAQ</a></li>
                    <li class="footer_col_item"><a class="footer_col_link" href="javascript:;">Obtenir un devis</a></li>
                    <li class="footer_col_item"><a class="footer_col_link" href="javascript:;">Contact</a></li>
                </ul>
                <ul class="footer_col">
                    <li class="footer_col_item"><a class="footer_col_link" href="javascript:;">Mentions légales</a></li>
                    <li class="footer_col_item"><a class="footer_col_link" href="javascript:;">CGU</a></li>
                </ul>
            </div>
            <div class="footer_langs">
                English
            </div>
        </div>
    </footer>