###Components    
    
    @example
    <div class="getlead getlead-batons">
        <div class="getlead_container">
			<div class="getlead_desc">
				<h2 class="h1 getlead_title">Comment ca marche ?</h2>
				<p class="getlead_subtitle">Envie d'en savoir plus sur le déroulement d'une étude RyM ?</p>
			</div>
			<div class="getleader_cta">
				<a href="javascript:;" class="button">En savoir plus</a>
			</div>
			<div class="getleader_illu">
				<!-- <svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/commentcamarche.svg#commentcamarche"></use></svg> -->
				<svg><use xlink:href="../wp-content/themes/rym/src/assets/svg/batons.svg#batons"></use></svg>
			</div>
		</div>
    </div>