<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;

	$getlead_prefix = (isset($getlead_prefix)) ? $getlead_prefix : '';

	$getlead_illustration = get_field($getlead_prefix.'getlead_illustration');
	$getlead_title        = esc_html(get_field($getlead_prefix.'getlead_title'));
	$getlead_subtitle     = esc_html(get_field($getlead_prefix.'getlead_subtitle'));
	$getlead_cta_link     = esc_url(get_field($getlead_prefix.'getlead_cta_link'));
	$getlead_cta_label    = esc_html(get_field($getlead_prefix.'getlead_cta_label'));
?>

<div class="getlead getlead-<?php echo $getlead_illustration; ?>">
	<div class="getlead_container">
		<div class="getlead_desc">
			<h2 class="h1 getlead_title load-hidden"><?php echo $getlead_title; ?></h2>
			<p class="getlead_subtitle load-hidden"><?php echo $getlead_subtitle; ?></p>
		</div>
		<div class="getleader_cta load-hidden">
			<a href="<?php echo $getlead_cta_link; ?>" class="button"><?php echo $getlead_cta_label; ?></a>
		</div>
		<?php
			$svgtarget = Config::get('svg-path').$getlead_illustration.'.svg#'.$getlead_illustration;	
			if(DFWP_ENV === 'preprod'){
				$context = stream_context_create(array (
					'http' => array (
						'header' => 'Authorization: Basic ' . base64_encode(DFWP_HTACCESS_USER.':'.DFWP_HTACCESS_PASS)
					)
				));
				$svgfile = file_get_contents($svgtarget, false, $context);
			}else{
				$svgfile = file_get_contents($svgtarget);
			}
			echo '<div class="getleader_illu">';				
				echo $svgfile;
			echo '</div>';
		?>
	</div>
</div>

