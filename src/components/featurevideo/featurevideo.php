<?php
	use Doublefou\Core\Config;
	use Doublefou\Core\Debug;

	//On affiche le bloc uniquement en francais
	if(pll_current_language() === 'fr')
	{
?>
	<div class="featurevideo">
		<div class="featurevideo_container">
			<h2 class="featurevideo_title h1 load-hidden">Découvrez le service Reach your Market en vidéo</h2>
			<div class="featurevideo_videocontainer">
				<div class='embed-container'>
					<iframe src='https://www.youtube.com/embed/UDURYc3dMwQ' frameborder='0' allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
<?php
	}
?>