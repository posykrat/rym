<div class="contactform">
	<div class="contactform_container">
		<?php 

			$shortcode = (pll_current_language() === "en") ? '[contact-form-7 id="265" title="Contact - EN"]' : '[contact-form-7 id="227" title="Contact"]';
			echo do_shortcode($shortcode); 
		
		?>
	</div>
	<div class="sep contactform_sep"></div>
</div>