# Styleguide options

### Head

    meta(name="viewport" content="width=device-width, initial-scale=1.0")
    link(rel='stylesheet' href='https://cdn.rawgit.com/styledown/styledown/v1.0.2/data/styledown.css')
    script(src='https://cdn.rawgit.com/styledown/styledown/v1.0.2/data/styledown.js')
    link(rel="stylesheet" href="../wp-content/themes/rym/dist/css/index.css")
    link(rel="stylesheet" href="../wp-content/themes/rym/dist/css/styleguide.min.css")
    script(defer src='../wp-content/themes/rym/dist/js/bundle.js')
    script(async src='../wp-content/themes/rym/src/utils/styleguide.js')
    script(async src="../wp-content/themes/rym/build/node_modules/svgxuse/svgxuse.min.js")

### Body

    div(class="dfwp_styleguide_sidebar")
        h1 Styleguide
        
        div(class="dfwp_styleguide_menu")
            h2 Elements
            ul
                li
                    a(href="#colors") Colors
                li
                    a(href="#fonts") Fonts
                li
                    a(href="#icons") Icons
                li
                    a(href="#inline") Inline
                li
                    a(href="#links") Links
                li
                    a(href="#title") Title
                li
                    a(href="#buttons") Buttons
                li
                    a(href="#form") Form elements

        div(class="dfwp_styleguide_menu")
            h2 Composants
            ul
                li
                    a(href="?components=header") Header
                li
                    a(href="?components=footer") Footer
                li
                    a(href="?components=hero") Hero
                li
                    a(href="?components=arguments") Arguments
                li
                    a(href="?components=features") Features
                li
                    a(href="?components=featurevideo") Featurevideo
                li
                    a(href="?components=getlead") Getlead
                li
                    a(href="?components=whoweare") Whoweare
                li
                    a(href="?components=testimonials") testimonials
                li
                    a(href="?components=partners") partners
                li
                    a(href="?components=timeline") timeline
                li
                    a(href="?components=after") after
                li
                    a(href="?components=pageheader") pageheader
                li
                    a(href="?components=contactway") contactway
                li
                    a(href="?components=accordion") accordion
                li
                    a(href="?components=help") help
                li
                    a(href="?components=pagecontent") pagecontent
                li
                    a(href="?components=header,exemple,footer&bodyclass=home") Exemple avec multiple chargement de components + body class
            
    div#dfwp_styleguide_content(sg-content)