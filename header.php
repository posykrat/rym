<!DOCTYPE html>
<html <?php language_attributes(); ?> class="sr">
<head>
	<meta name="Author" content="Clément Biron" />
	<meta name="Robots" content="all" />
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script type="text/javascript"> 
		var ua = window.navigator.userAgent; 
		var msie = ua.indexOf("MSIE "); 
		if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) document.write('<script src=https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js><\/scr'+'ipt>'); 
	</script>
	<?php 
		// For plugins 
		wp_head();
	?>
    
</head>
<body class="<?php echo join(' ', get_body_class()); ?>">

<?php
	//Chargement du header
	get_template_part('src/components/header/header');
?>