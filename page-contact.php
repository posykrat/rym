<?php
	/*
	Template Name: Contact
	*/
	
	get_header();

	get_template_part('src/components/pageheader/pageheader');
	get_template_part('src/components/contactform/contactform');
	get_template_part('src/components/contactway/contactway');

	set_query_var( 'getlead_prefix', 'get_lead4_');
	get_template_part('src/components/getlead/getlead');
?>


<?php 

	if ( have_posts() ) : 
		while ( have_posts() ) : the_post();
			the_content();
		endwhile;  
	endif; 
?>

<?php
	get_footer();
?>