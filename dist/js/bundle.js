(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
/*
Details Element Polyfill 2.4.0
Copyright © 2019 Javan Makhmali
 */
(function() {
  "use strict";
  var element = document.createElement("details");
  var elementIsNative = typeof HTMLDetailsElement != "undefined" && element instanceof HTMLDetailsElement;
  var support = {
    open: "open" in element || elementIsNative,
    toggle: "ontoggle" in element
  };
  var styles = '\ndetails, summary {\n  display: block;\n}\ndetails:not([open]) > *:not(summary) {\n  display: none;\n}\nsummary::before {\n  content: "►";\n  padding-right: 0.3rem;\n  font-size: 0.6rem;\n  cursor: default;\n}\n[open] > summary::before {\n  content: "▼";\n}\n';
  var _ref = [], forEach = _ref.forEach, slice = _ref.slice;
  if (!support.open) {
    polyfillStyles();
    polyfillProperties();
    polyfillToggle();
    polyfillAccessibility();
  }
  if (support.open && !support.toggle) {
    polyfillToggleEvent();
  }
  function polyfillStyles() {
    document.head.insertAdjacentHTML("afterbegin", "<style>" + styles + "</style>");
  }
  function polyfillProperties() {
    var prototype = document.createElement("details").constructor.prototype;
    var setAttribute = prototype.setAttribute, removeAttribute = prototype.removeAttribute;
    var open = Object.getOwnPropertyDescriptor(prototype, "open");
    Object.defineProperties(prototype, {
      open: {
        get: function get() {
          if (this.tagName == "DETAILS") {
            return this.hasAttribute("open");
          } else {
            if (open && open.get) {
              return open.get.call(this);
            }
          }
        },
        set: function set(value) {
          if (this.tagName == "DETAILS") {
            return value ? this.setAttribute("open", "") : this.removeAttribute("open");
          } else {
            if (open && open.set) {
              return open.set.call(this, value);
            }
          }
        }
      },
      setAttribute: {
        value: function value(name, _value) {
          var _this = this;
          var call = function call() {
            return setAttribute.call(_this, name, _value);
          };
          if (name == "open" && this.tagName == "DETAILS") {
            var wasOpen = this.hasAttribute("open");
            var result = call();
            if (!wasOpen) {
              var summary = this.querySelector("summary");
              if (summary) summary.setAttribute("aria-expanded", true);
              triggerToggle(this);
            }
            return result;
          }
          return call();
        }
      },
      removeAttribute: {
        value: function value(name) {
          var _this2 = this;
          var call = function call() {
            return removeAttribute.call(_this2, name);
          };
          if (name == "open" && this.tagName == "DETAILS") {
            var wasOpen = this.hasAttribute("open");
            var result = call();
            if (wasOpen) {
              var summary = this.querySelector("summary");
              if (summary) summary.setAttribute("aria-expanded", false);
              triggerToggle(this);
            }
            return result;
          }
          return call();
        }
      }
    });
  }
  function polyfillToggle() {
    onTogglingTrigger(function(element) {
      element.hasAttribute("open") ? element.removeAttribute("open") : element.setAttribute("open", "");
    });
  }
  function polyfillToggleEvent() {
    if (window.MutationObserver) {
      new MutationObserver(function(mutations) {
        forEach.call(mutations, function(mutation) {
          var target = mutation.target, attributeName = mutation.attributeName;
          if (target.tagName == "DETAILS" && attributeName == "open") {
            triggerToggle(target);
          }
        });
      }).observe(document.documentElement, {
        attributes: true,
        subtree: true
      });
    } else {
      onTogglingTrigger(function(element) {
        var wasOpen = element.getAttribute("open");
        setTimeout(function() {
          var isOpen = element.getAttribute("open");
          if (wasOpen != isOpen) {
            triggerToggle(element);
          }
        }, 1);
      });
    }
  }
  function polyfillAccessibility() {
    setAccessibilityAttributes(document);
    if (window.MutationObserver) {
      new MutationObserver(function(mutations) {
        forEach.call(mutations, function(mutation) {
          forEach.call(mutation.addedNodes, setAccessibilityAttributes);
        });
      }).observe(document.documentElement, {
        subtree: true,
        childList: true
      });
    } else {
      document.addEventListener("DOMNodeInserted", function(event) {
        setAccessibilityAttributes(event.target);
      });
    }
  }
  function setAccessibilityAttributes(root) {
    findElementsWithTagName(root, "SUMMARY").forEach(function(summary) {
      var details = findClosestElementWithTagName(summary, "DETAILS");
      summary.setAttribute("aria-expanded", details.hasAttribute("open"));
      if (!summary.hasAttribute("tabindex")) summary.setAttribute("tabindex", "0");
      if (!summary.hasAttribute("role")) summary.setAttribute("role", "button");
    });
  }
  function eventIsSignificant(event) {
    return !(event.defaultPrevented || event.ctrlKey || event.metaKey || event.shiftKey || event.target.isContentEditable);
  }
  function onTogglingTrigger(callback) {
    addEventListener("click", function(event) {
      if (eventIsSignificant(event)) {
        if (event.which <= 1) {
          var element = findClosestElementWithTagName(event.target, "SUMMARY");
          if (element && element.parentNode && element.parentNode.tagName == "DETAILS") {
            callback(element.parentNode);
          }
        }
      }
    }, false);
    addEventListener("keydown", function(event) {
      if (eventIsSignificant(event)) {
        if (event.keyCode == 13 || event.keyCode == 32) {
          var element = findClosestElementWithTagName(event.target, "SUMMARY");
          if (element && element.parentNode && element.parentNode.tagName == "DETAILS") {
            callback(element.parentNode);
            event.preventDefault();
          }
        }
      }
    }, false);
  }
  function triggerToggle(element) {
    var event = document.createEvent("Event");
    event.initEvent("toggle", false, false);
    element.dispatchEvent(event);
  }
  function findElementsWithTagName(root, tagName) {
    return (root.tagName == tagName ? [ root ] : []).concat(typeof root.getElementsByTagName == "function" ? slice.call(root.getElementsByTagName(tagName)) : []);
  }
  function findClosestElementWithTagName(element, tagName) {
    if (typeof element.closest == "function") {
      return element.closest(tagName);
    } else {
      while (element) {
        if (element.tagName == tagName) {
          return element;
        } else {
          element = element.parentNode;
        }
      }
    }
  }
})();

},{}],2:[function(require,module,exports){
/*! @license ScrollReveal v4.0.6

	Copyright 2020 Fisssion LLC.

	Licensed under the GNU General Public License 3.0 for
	compatible open source projects and non-commercial use.

	For commercial sites, themes, projects, and applications,
	keep your source code private/proprietary by purchasing
	a commercial license from https://scrollrevealjs.org/
*/
(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global = global || self, global.ScrollReveal = factory());
}(this, function () { 'use strict';

	var defaults = {
		delay: 0,
		distance: '0',
		duration: 600,
		easing: 'cubic-bezier(0.5, 0, 0, 1)',
		interval: 0,
		opacity: 0,
		origin: 'bottom',
		rotate: {
			x: 0,
			y: 0,
			z: 0
		},
		scale: 1,
		cleanup: false,
		container: document.documentElement,
		desktop: true,
		mobile: true,
		reset: false,
		useDelay: 'always',
		viewFactor: 0.0,
		viewOffset: {
			top: 0,
			right: 0,
			bottom: 0,
			left: 0
		},
		afterReset: function afterReset() {},
		afterReveal: function afterReveal() {},
		beforeReset: function beforeReset() {},
		beforeReveal: function beforeReveal() {}
	};

	function failure() {
		document.documentElement.classList.remove('sr');

		return {
			clean: function clean() {},
			destroy: function destroy() {},
			reveal: function reveal() {},
			sync: function sync() {},
			get noop() {
				return true
			}
		}
	}

	function success() {
		document.documentElement.classList.add('sr');

		if (document.body) {
			document.body.style.height = '100%';
		} else {
			document.addEventListener('DOMContentLoaded', function () {
				document.body.style.height = '100%';
			});
		}
	}

	var mount = { success: success, failure: failure };

	/*! @license is-dom-node v1.0.4

		Copyright 2018 Fisssion LLC.

		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in all
		copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
		SOFTWARE.

	*/
	function isDomNode(x) {
		return typeof window.Node === 'object'
			? x instanceof window.Node
			: x !== null &&
					typeof x === 'object' &&
					typeof x.nodeType === 'number' &&
					typeof x.nodeName === 'string'
	}

	/*! @license is-dom-node-list v1.2.1

		Copyright 2018 Fisssion LLC.

		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in all
		copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
		SOFTWARE.

	*/

	function isDomNodeList(x) {
		var prototypeToString = Object.prototype.toString.call(x);
		var regex = /^\[object (HTMLCollection|NodeList|Object)\]$/;

		return typeof window.NodeList === 'object'
			? x instanceof window.NodeList
			: x !== null &&
					typeof x === 'object' &&
					typeof x.length === 'number' &&
					regex.test(prototypeToString) &&
					(x.length === 0 || isDomNode(x[0]))
	}

	/*! @license Tealight v0.3.6

		Copyright 2018 Fisssion LLC.

		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in all
		copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
		SOFTWARE.

	*/

	function tealight(target, context) {
	  if ( context === void 0 ) { context = document; }

	  if (target instanceof Array) { return target.filter(isDomNode); }
	  if (isDomNode(target)) { return [target]; }
	  if (isDomNodeList(target)) { return Array.prototype.slice.call(target); }
	  if (typeof target === "string") {
	    try {
	      var query = context.querySelectorAll(target);
	      return Array.prototype.slice.call(query);
	    } catch (err) {
	      return [];
	    }
	  }
	  return [];
	}

	function isObject(x) {
		return (
			x !== null &&
			x instanceof Object &&
			(x.constructor === Object ||
				Object.prototype.toString.call(x) === '[object Object]')
		)
	}

	function each(collection, callback) {
		if (isObject(collection)) {
			var keys = Object.keys(collection);
			return keys.forEach(function (key) { return callback(collection[key], key, collection); })
		}
		if (collection instanceof Array) {
			return collection.forEach(function (item, i) { return callback(item, i, collection); })
		}
		throw new TypeError('Expected either an array or object literal.')
	}

	function logger(message) {
		var details = [], len = arguments.length - 1;
		while ( len-- > 0 ) details[ len ] = arguments[ len + 1 ];

		if (this.constructor.debug && console) {
			var report = "%cScrollReveal: " + message;
			details.forEach(function (detail) { return (report += "\n — " + detail); });
			console.log(report, 'color: #ea654b;'); // eslint-disable-line no-console
		}
	}

	function rinse() {
		var this$1 = this;

		var struct = function () { return ({
			active: [],
			stale: []
		}); };

		var elementIds = struct();
		var sequenceIds = struct();
		var containerIds = struct();

		/**
		 * Take stock of active element IDs.
		 */
		try {
			each(tealight('[data-sr-id]'), function (node) {
				var id = parseInt(node.getAttribute('data-sr-id'));
				elementIds.active.push(id);
			});
		} catch (e) {
			throw e
		}
		/**
		 * Destroy stale elements.
		 */
		each(this.store.elements, function (element) {
			if (elementIds.active.indexOf(element.id) === -1) {
				elementIds.stale.push(element.id);
			}
		});

		each(elementIds.stale, function (staleId) { return delete this$1.store.elements[staleId]; });

		/**
		 * Take stock of active container and sequence IDs.
		 */
		each(this.store.elements, function (element) {
			if (containerIds.active.indexOf(element.containerId) === -1) {
				containerIds.active.push(element.containerId);
			}
			if (element.hasOwnProperty('sequence')) {
				if (sequenceIds.active.indexOf(element.sequence.id) === -1) {
					sequenceIds.active.push(element.sequence.id);
				}
			}
		});

		/**
		 * Destroy stale containers.
		 */
		each(this.store.containers, function (container) {
			if (containerIds.active.indexOf(container.id) === -1) {
				containerIds.stale.push(container.id);
			}
		});

		each(containerIds.stale, function (staleId) {
			var stale = this$1.store.containers[staleId].node;
			stale.removeEventListener('scroll', this$1.delegate);
			stale.removeEventListener('resize', this$1.delegate);
			delete this$1.store.containers[staleId];
		});

		/**
		 * Destroy stale sequences.
		 */
		each(this.store.sequences, function (sequence) {
			if (sequenceIds.active.indexOf(sequence.id) === -1) {
				sequenceIds.stale.push(sequence.id);
			}
		});

		each(sequenceIds.stale, function (staleId) { return delete this$1.store.sequences[staleId]; });
	}

	function clean(target) {
		var this$1 = this;

		var dirty;
		try {
			each(tealight(target), function (node) {
				var id = node.getAttribute('data-sr-id');
				if (id !== null) {
					dirty = true;
					var element = this$1.store.elements[id];
					if (element.callbackTimer) {
						window.clearTimeout(element.callbackTimer.clock);
					}
					node.setAttribute('style', element.styles.inline.generated);
					node.removeAttribute('data-sr-id');
					delete this$1.store.elements[id];
				}
			});
		} catch (e) {
			return logger.call(this, 'Clean failed.', e.message)
		}

		if (dirty) {
			try {
				rinse.call(this);
			} catch (e) {
				return logger.call(this, 'Clean failed.', e.message)
			}
		}
	}

	function destroy() {
		var this$1 = this;

		/**
		 * Remove all generated styles and element ids
		 */
		each(this.store.elements, function (element) {
			element.node.setAttribute('style', element.styles.inline.generated);
			element.node.removeAttribute('data-sr-id');
		});

		/**
		 * Remove all event listeners.
		 */
		each(this.store.containers, function (container) {
			var target =
				container.node === document.documentElement ? window : container.node;
			target.removeEventListener('scroll', this$1.delegate);
			target.removeEventListener('resize', this$1.delegate);
		});

		/**
		 * Clear all data from the store
		 */
		this.store = {
			containers: {},
			elements: {},
			history: [],
			sequences: {}
		};
	}

	/*! @license Rematrix v0.3.0

		Copyright 2018 Julian Lloyd.

		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in
		all copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
		THE SOFTWARE.
	*/
	/**
	 * @module Rematrix
	 */

	/**
	 * Transformation matrices in the browser come in two flavors:
	 *
	 *  - `matrix` using 6 values (short)
	 *  - `matrix3d` using 16 values (long)
	 *
	 * This utility follows this [conversion guide](https://goo.gl/EJlUQ1)
	 * to expand short form matrices to their equivalent long form.
	 *
	 * @param  {array} source - Accepts both short and long form matrices.
	 * @return {array}
	 */
	function format(source) {
		if (source.constructor !== Array) {
			throw new TypeError('Expected array.')
		}
		if (source.length === 16) {
			return source
		}
		if (source.length === 6) {
			var matrix = identity();
			matrix[0] = source[0];
			matrix[1] = source[1];
			matrix[4] = source[2];
			matrix[5] = source[3];
			matrix[12] = source[4];
			matrix[13] = source[5];
			return matrix
		}
		throw new RangeError('Expected array with either 6 or 16 values.')
	}

	/**
	 * Returns a matrix representing no transformation. The product of any matrix
	 * multiplied by the identity matrix will be the original matrix.
	 *
	 * > **Tip:** Similar to how `5 * 1 === 5`, where `1` is the identity.
	 *
	 * @return {array}
	 */
	function identity() {
		var matrix = [];
		for (var i = 0; i < 16; i++) {
			i % 5 == 0 ? matrix.push(1) : matrix.push(0);
		}
		return matrix
	}

	/**
	 * Returns a 4x4 matrix describing the combined transformations
	 * of both arguments.
	 *
	 * > **Note:** Order is very important. For example, rotating 45°
	 * along the Z-axis, followed by translating 500 pixels along the
	 * Y-axis... is not the same as translating 500 pixels along the
	 * Y-axis, followed by rotating 45° along on the Z-axis.
	 *
	 * @param  {array} m - Accepts both short and long form matrices.
	 * @param  {array} x - Accepts both short and long form matrices.
	 * @return {array}
	 */
	function multiply(m, x) {
		var fm = format(m);
		var fx = format(x);
		var product = [];

		for (var i = 0; i < 4; i++) {
			var row = [fm[i], fm[i + 4], fm[i + 8], fm[i + 12]];
			for (var j = 0; j < 4; j++) {
				var k = j * 4;
				var col = [fx[k], fx[k + 1], fx[k + 2], fx[k + 3]];
				var result =
					row[0] * col[0] + row[1] * col[1] + row[2] * col[2] + row[3] * col[3];

				product[i + k] = result;
			}
		}

		return product
	}

	/**
	 * Attempts to return a 4x4 matrix describing the CSS transform
	 * matrix passed in, but will return the identity matrix as a
	 * fallback.
	 *
	 * > **Tip:** This method is used to convert a CSS matrix (retrieved as a
	 * `string` from computed styles) to its equivalent array format.
	 *
	 * @param  {string} source - `matrix` or `matrix3d` CSS Transform value.
	 * @return {array}
	 */
	function parse(source) {
		if (typeof source === 'string') {
			var match = source.match(/matrix(3d)?\(([^)]+)\)/);
			if (match) {
				var raw = match[2].split(', ').map(parseFloat);
				return format(raw)
			}
		}
		return identity()
	}

	/**
	 * Returns a 4x4 matrix describing X-axis rotation.
	 *
	 * @param  {number} angle - Measured in degrees.
	 * @return {array}
	 */
	function rotateX(angle) {
		var theta = Math.PI / 180 * angle;
		var matrix = identity();

		matrix[5] = matrix[10] = Math.cos(theta);
		matrix[6] = matrix[9] = Math.sin(theta);
		matrix[9] *= -1;

		return matrix
	}

	/**
	 * Returns a 4x4 matrix describing Y-axis rotation.
	 *
	 * @param  {number} angle - Measured in degrees.
	 * @return {array}
	 */
	function rotateY(angle) {
		var theta = Math.PI / 180 * angle;
		var matrix = identity();

		matrix[0] = matrix[10] = Math.cos(theta);
		matrix[2] = matrix[8] = Math.sin(theta);
		matrix[2] *= -1;

		return matrix
	}

	/**
	 * Returns a 4x4 matrix describing Z-axis rotation.
	 *
	 * @param  {number} angle - Measured in degrees.
	 * @return {array}
	 */
	function rotateZ(angle) {
		var theta = Math.PI / 180 * angle;
		var matrix = identity();

		matrix[0] = matrix[5] = Math.cos(theta);
		matrix[1] = matrix[4] = Math.sin(theta);
		matrix[4] *= -1;

		return matrix
	}

	/**
	 * Returns a 4x4 matrix describing 2D scaling. The first argument
	 * is used for both X and Y-axis scaling, unless an optional
	 * second argument is provided to explicitly define Y-axis scaling.
	 *
	 * @param  {number} scalar    - Decimal multiplier.
	 * @param  {number} [scalarY] - Decimal multiplier.
	 * @return {array}
	 */
	function scale(scalar, scalarY) {
		var matrix = identity();

		matrix[0] = scalar;
		matrix[5] = typeof scalarY === 'number' ? scalarY : scalar;

		return matrix
	}

	/**
	 * Returns a 4x4 matrix describing X-axis translation.
	 *
	 * @param  {number} distance - Measured in pixels.
	 * @return {array}
	 */
	function translateX(distance) {
		var matrix = identity();
		matrix[12] = distance;
		return matrix
	}

	/**
	 * Returns a 4x4 matrix describing Y-axis translation.
	 *
	 * @param  {number} distance - Measured in pixels.
	 * @return {array}
	 */
	function translateY(distance) {
		var matrix = identity();
		matrix[13] = distance;
		return matrix
	}

	var getPrefixedCssProp = (function () {
		var properties = {};
		var style = document.documentElement.style;

		function getPrefixedCssProperty(name, source) {
			if ( source === void 0 ) source = style;

			if (name && typeof name === 'string') {
				if (properties[name]) {
					return properties[name]
				}
				if (typeof source[name] === 'string') {
					return (properties[name] = name)
				}
				if (typeof source[("-webkit-" + name)] === 'string') {
					return (properties[name] = "-webkit-" + name)
				}
				throw new RangeError(("Unable to find \"" + name + "\" style property."))
			}
			throw new TypeError('Expected a string.')
		}

		getPrefixedCssProperty.clearCache = function () { return (properties = {}); };

		return getPrefixedCssProperty
	})();

	function style(element) {
		var computed = window.getComputedStyle(element.node);
		var position = computed.position;
		var config = element.config;

		/**
		 * Generate inline styles
		 */
		var inline = {};
		var inlineStyle = element.node.getAttribute('style') || '';
		var inlineMatch = inlineStyle.match(/[\w-]+\s*:\s*[^;]+\s*/gi) || [];

		inline.computed = inlineMatch ? inlineMatch.map(function (m) { return m.trim(); }).join('; ') + ';' : '';

		inline.generated = inlineMatch.some(function (m) { return m.match(/visibility\s?:\s?visible/i); })
			? inline.computed
			: inlineMatch.concat( ['visibility: visible']).map(function (m) { return m.trim(); }).join('; ') + ';';

		/**
		 * Generate opacity styles
		 */
		var computedOpacity = parseFloat(computed.opacity);
		var configOpacity = !isNaN(parseFloat(config.opacity))
			? parseFloat(config.opacity)
			: parseFloat(computed.opacity);

		var opacity = {
			computed: computedOpacity !== configOpacity ? ("opacity: " + computedOpacity + ";") : '',
			generated: computedOpacity !== configOpacity ? ("opacity: " + configOpacity + ";") : ''
		};

		/**
		 * Generate transformation styles
		 */
		var transformations = [];

		if (parseFloat(config.distance)) {
			var axis = config.origin === 'top' || config.origin === 'bottom' ? 'Y' : 'X';

			/**
			 * Let’s make sure our our pixel distances are negative for top and left.
			 * e.g. { origin: 'top', distance: '25px' } starts at `top: -25px` in CSS.
			 */
			var distance = config.distance;
			if (config.origin === 'top' || config.origin === 'left') {
				distance = /^-/.test(distance) ? distance.substr(1) : ("-" + distance);
			}

			var ref = distance.match(/(^-?\d+\.?\d?)|(em$|px$|%$)/g);
			var value = ref[0];
			var unit = ref[1];

			switch (unit) {
				case 'em':
					distance = parseInt(computed.fontSize) * value;
					break
				case 'px':
					distance = value;
					break
				case '%':
					/**
					 * Here we use `getBoundingClientRect` instead of
					 * the existing data attached to `element.geometry`
					 * because only the former includes any transformations
					 * current applied to the element.
					 *
					 * If that behavior ends up being unintuitive, this
					 * logic could instead utilize `element.geometry.height`
					 * and `element.geoemetry.width` for the distance calculation
					 */
					distance =
						axis === 'Y'
							? (element.node.getBoundingClientRect().height * value) / 100
							: (element.node.getBoundingClientRect().width * value) / 100;
					break
				default:
					throw new RangeError('Unrecognized or missing distance unit.')
			}

			if (axis === 'Y') {
				transformations.push(translateY(distance));
			} else {
				transformations.push(translateX(distance));
			}
		}

		if (config.rotate.x) { transformations.push(rotateX(config.rotate.x)); }
		if (config.rotate.y) { transformations.push(rotateY(config.rotate.y)); }
		if (config.rotate.z) { transformations.push(rotateZ(config.rotate.z)); }
		if (config.scale !== 1) {
			if (config.scale === 0) {
				/**
				 * The CSS Transforms matrix interpolation specification
				 * basically disallows transitions of non-invertible
				 * matrixes, which means browsers won't transition
				 * elements with zero scale.
				 *
				 * That’s inconvenient for the API and developer
				 * experience, so we simply nudge their value
				 * slightly above zero; this allows browsers
				 * to transition our element as expected.
				 *
				 * `0.0002` was the smallest number
				 * that performed across browsers.
				 */
				transformations.push(scale(0.0002));
			} else {
				transformations.push(scale(config.scale));
			}
		}

		var transform = {};
		if (transformations.length) {
			transform.property = getPrefixedCssProp('transform');
			/**
			 * The default computed transform value should be one of:
			 * undefined || 'none' || 'matrix()' || 'matrix3d()'
			 */
			transform.computed = {
				raw: computed[transform.property],
				matrix: parse(computed[transform.property])
			};

			transformations.unshift(transform.computed.matrix);
			var product = transformations.reduce(multiply);

			transform.generated = {
				initial: ((transform.property) + ": matrix3d(" + (product.join(', ')) + ");"),
				final: ((transform.property) + ": matrix3d(" + (transform.computed.matrix.join(', ')) + ");")
			};
		} else {
			transform.generated = {
				initial: '',
				final: ''
			};
		}

		/**
		 * Generate transition styles
		 */
		var transition = {};
		if (opacity.generated || transform.generated.initial) {
			transition.property = getPrefixedCssProp('transition');
			transition.computed = computed[transition.property];
			transition.fragments = [];

			var delay = config.delay;
			var duration = config.duration;
			var easing = config.easing;

			if (opacity.generated) {
				transition.fragments.push({
					delayed: ("opacity " + (duration / 1000) + "s " + easing + " " + (delay / 1000) + "s"),
					instant: ("opacity " + (duration / 1000) + "s " + easing + " 0s")
				});
			}

			if (transform.generated.initial) {
				transition.fragments.push({
					delayed: ((transform.property) + " " + (duration / 1000) + "s " + easing + " " + (delay / 1000) + "s"),
					instant: ((transform.property) + " " + (duration / 1000) + "s " + easing + " 0s")
				});
			}

			/**
			 * The default computed transition property should be undefined, or one of:
			 * '' || 'none 0s ease 0s' || 'all 0s ease 0s' || 'all 0s 0s cubic-bezier()'
			 */
			var hasCustomTransition =
				transition.computed && !transition.computed.match(/all 0s|none 0s/);

			if (hasCustomTransition) {
				transition.fragments.unshift({
					delayed: transition.computed,
					instant: transition.computed
				});
			}

			var composed = transition.fragments.reduce(
				function (composition, fragment, i) {
					composition.delayed += i === 0 ? fragment.delayed : (", " + (fragment.delayed));
					composition.instant += i === 0 ? fragment.instant : (", " + (fragment.instant));
					return composition
				},
				{
					delayed: '',
					instant: ''
				}
			);

			transition.generated = {
				delayed: ((transition.property) + ": " + (composed.delayed) + ";"),
				instant: ((transition.property) + ": " + (composed.instant) + ";")
			};
		} else {
			transition.generated = {
				delayed: '',
				instant: ''
			};
		}

		return {
			inline: inline,
			opacity: opacity,
			position: position,
			transform: transform,
			transition: transition
		}
	}

	function animate(element, force) {
		if ( force === void 0 ) force = {};

		var pristine = force.pristine || this.pristine;
		var delayed =
			element.config.useDelay === 'always' ||
			(element.config.useDelay === 'onload' && pristine) ||
			(element.config.useDelay === 'once' && !element.seen);

		var shouldReveal = element.visible && !element.revealed;
		var shouldReset = !element.visible && element.revealed && element.config.reset;

		if (force.reveal || shouldReveal) {
			return triggerReveal.call(this, element, delayed)
		}

		if (force.reset || shouldReset) {
			return triggerReset.call(this, element)
		}
	}

	function triggerReveal(element, delayed) {
		var styles = [
			element.styles.inline.generated,
			element.styles.opacity.computed,
			element.styles.transform.generated.final
		];
		if (delayed) {
			styles.push(element.styles.transition.generated.delayed);
		} else {
			styles.push(element.styles.transition.generated.instant);
		}
		element.revealed = element.seen = true;
		element.node.setAttribute('style', styles.filter(function (s) { return s !== ''; }).join(' '));
		registerCallbacks.call(this, element, delayed);
	}

	function triggerReset(element) {
		var styles = [
			element.styles.inline.generated,
			element.styles.opacity.generated,
			element.styles.transform.generated.initial,
			element.styles.transition.generated.instant
		];
		element.revealed = false;
		element.node.setAttribute('style', styles.filter(function (s) { return s !== ''; }).join(' '));
		registerCallbacks.call(this, element);
	}

	function registerCallbacks(element, isDelayed) {
		var this$1 = this;

		var duration = isDelayed
			? element.config.duration + element.config.delay
			: element.config.duration;

		var beforeCallback = element.revealed
			? element.config.beforeReveal
			: element.config.beforeReset;

		var afterCallback = element.revealed
			? element.config.afterReveal
			: element.config.afterReset;

		var elapsed = 0;
		if (element.callbackTimer) {
			elapsed = Date.now() - element.callbackTimer.start;
			window.clearTimeout(element.callbackTimer.clock);
		}

		beforeCallback(element.node);

		element.callbackTimer = {
			start: Date.now(),
			clock: window.setTimeout(function () {
				afterCallback(element.node);
				element.callbackTimer = null;
				if (element.revealed && !element.config.reset && element.config.cleanup) {
					clean.call(this$1, element.node);
				}
			}, duration - elapsed)
		};
	}

	var nextUniqueId = (function () {
		var uid = 0;
		return function () { return uid++; }
	})();

	function sequence(element, pristine) {
		if ( pristine === void 0 ) pristine = this.pristine;

		/**
		 * We first check if the element should reset.
		 */
		if (!element.visible && element.revealed && element.config.reset) {
			return animate.call(this, element, { reset: true })
		}

		var seq = this.store.sequences[element.sequence.id];
		var i = element.sequence.index;

		if (seq) {
			var visible = new SequenceModel(seq, 'visible', this.store);
			var revealed = new SequenceModel(seq, 'revealed', this.store);

			seq.models = { visible: visible, revealed: revealed };

			/**
			 * If the sequence has no revealed members,
			 * then we reveal the first visible element
			 * within that sequence.
			 *
			 * The sequence then cues a recursive call
			 * in both directions.
			 */
			if (!revealed.body.length) {
				var nextId = seq.members[visible.body[0]];
				var nextElement = this.store.elements[nextId];

				if (nextElement) {
					cue.call(this, seq, visible.body[0], -1, pristine);
					cue.call(this, seq, visible.body[0], +1, pristine);
					return animate.call(this, nextElement, { reveal: true, pristine: pristine })
				}
			}

			/**
			 * If our element isn’t resetting, we check the
			 * element sequence index against the head, and
			 * then the foot of the sequence.
			 */
			if (
				!seq.blocked.head &&
				i === [].concat( revealed.head ).pop() &&
				i >= [].concat( visible.body ).shift()
			) {
				cue.call(this, seq, i, -1, pristine);
				return animate.call(this, element, { reveal: true, pristine: pristine })
			}

			if (
				!seq.blocked.foot &&
				i === [].concat( revealed.foot ).shift() &&
				i <= [].concat( visible.body ).pop()
			) {
				cue.call(this, seq, i, +1, pristine);
				return animate.call(this, element, { reveal: true, pristine: pristine })
			}
		}
	}

	function Sequence(interval) {
		var i = Math.abs(interval);
		if (!isNaN(i)) {
			this.id = nextUniqueId();
			this.interval = Math.max(i, 16);
			this.members = [];
			this.models = {};
			this.blocked = {
				head: false,
				foot: false
			};
		} else {
			throw new RangeError('Invalid sequence interval.')
		}
	}

	function SequenceModel(seq, prop, store) {
		var this$1 = this;

		this.head = [];
		this.body = [];
		this.foot = [];

		each(seq.members, function (id, index) {
			var element = store.elements[id];
			if (element && element[prop]) {
				this$1.body.push(index);
			}
		});

		if (this.body.length) {
			each(seq.members, function (id, index) {
				var element = store.elements[id];
				if (element && !element[prop]) {
					if (index < this$1.body[0]) {
						this$1.head.push(index);
					} else {
						this$1.foot.push(index);
					}
				}
			});
		}
	}

	function cue(seq, i, direction, pristine) {
		var this$1 = this;

		var blocked = ['head', null, 'foot'][1 + direction];
		var nextId = seq.members[i + direction];
		var nextElement = this.store.elements[nextId];

		seq.blocked[blocked] = true;

		setTimeout(function () {
			seq.blocked[blocked] = false;
			if (nextElement) {
				sequence.call(this$1, nextElement, pristine);
			}
		}, seq.interval);
	}

	function initialize() {
		var this$1 = this;

		rinse.call(this);

		each(this.store.elements, function (element) {
			var styles = [element.styles.inline.generated];

			if (element.visible) {
				styles.push(element.styles.opacity.computed);
				styles.push(element.styles.transform.generated.final);
				element.revealed = true;
			} else {
				styles.push(element.styles.opacity.generated);
				styles.push(element.styles.transform.generated.initial);
				element.revealed = false;
			}

			element.node.setAttribute('style', styles.filter(function (s) { return s !== ''; }).join(' '));
		});

		each(this.store.containers, function (container) {
			var target =
				container.node === document.documentElement ? window : container.node;
			target.addEventListener('scroll', this$1.delegate);
			target.addEventListener('resize', this$1.delegate);
		});

		/**
		 * Manually invoke delegate once to capture
		 * element and container dimensions, container
		 * scroll position, and trigger any valid reveals
		 */
		this.delegate();

		/**
		 * Wipe any existing `setTimeout` now
		 * that initialization has completed.
		 */
		this.initTimeout = null;
	}

	function isMobile(agent) {
		if ( agent === void 0 ) agent = navigator.userAgent;

		return /Android|iPhone|iPad|iPod/i.test(agent)
	}

	function deepAssign(target) {
		var sources = [], len = arguments.length - 1;
		while ( len-- > 0 ) sources[ len ] = arguments[ len + 1 ];

		if (isObject(target)) {
			each(sources, function (source) {
				each(source, function (data, key) {
					if (isObject(data)) {
						if (!target[key] || !isObject(target[key])) {
							target[key] = {};
						}
						deepAssign(target[key], data);
					} else {
						target[key] = data;
					}
				});
			});
			return target
		} else {
			throw new TypeError('Target must be an object literal.')
		}
	}

	function reveal(target, options, syncing) {
		var this$1 = this;
		if ( options === void 0 ) options = {};
		if ( syncing === void 0 ) syncing = false;

		var containerBuffer = [];
		var sequence$$1;
		var interval = options.interval || defaults.interval;

		try {
			if (interval) {
				sequence$$1 = new Sequence(interval);
			}

			var nodes = tealight(target);
			if (!nodes.length) {
				throw new Error('Invalid reveal target.')
			}

			var elements = nodes.reduce(function (elementBuffer, elementNode) {
				var element = {};
				var existingId = elementNode.getAttribute('data-sr-id');

				if (existingId) {
					deepAssign(element, this$1.store.elements[existingId]);

					/**
					 * In order to prevent previously generated styles
					 * from throwing off the new styles, the style tag
					 * has to be reverted to its pre-reveal state.
					 */
					element.node.setAttribute('style', element.styles.inline.computed);
				} else {
					element.id = nextUniqueId();
					element.node = elementNode;
					element.seen = false;
					element.revealed = false;
					element.visible = false;
				}

				var config = deepAssign({}, element.config || this$1.defaults, options);

				if ((!config.mobile && isMobile()) || (!config.desktop && !isMobile())) {
					if (existingId) {
						clean.call(this$1, element);
					}
					return elementBuffer // skip elements that are disabled
				}

				var containerNode = tealight(config.container)[0];
				if (!containerNode) {
					throw new Error('Invalid container.')
				}
				if (!containerNode.contains(elementNode)) {
					return elementBuffer // skip elements found outside the container
				}

				var containerId;
				{
					containerId = getContainerId(
						containerNode,
						containerBuffer,
						this$1.store.containers
					);
					if (containerId === null) {
						containerId = nextUniqueId();
						containerBuffer.push({ id: containerId, node: containerNode });
					}
				}

				element.config = config;
				element.containerId = containerId;
				element.styles = style(element);

				if (sequence$$1) {
					element.sequence = {
						id: sequence$$1.id,
						index: sequence$$1.members.length
					};
					sequence$$1.members.push(element.id);
				}

				elementBuffer.push(element);
				return elementBuffer
			}, []);

			/**
			 * Modifying the DOM via setAttribute needs to be handled
			 * separately from reading computed styles in the map above
			 * for the browser to batch DOM changes (limiting reflows)
			 */
			each(elements, function (element) {
				this$1.store.elements[element.id] = element;
				element.node.setAttribute('data-sr-id', element.id);
			});
		} catch (e) {
			return logger.call(this, 'Reveal failed.', e.message)
		}

		/**
		 * Now that element set-up is complete...
		 * Let’s commit any container and sequence data we have to the store.
		 */
		each(containerBuffer, function (container) {
			this$1.store.containers[container.id] = {
				id: container.id,
				node: container.node
			};
		});
		if (sequence$$1) {
			this.store.sequences[sequence$$1.id] = sequence$$1;
		}

		/**
		 * If reveal wasn't invoked by sync, we want to
		 * make sure to add this call to the history.
		 */
		if (syncing !== true) {
			this.store.history.push({ target: target, options: options });

			/**
			 * Push initialization to the event queue, giving
			 * multiple reveal calls time to be interpreted.
			 */
			if (this.initTimeout) {
				window.clearTimeout(this.initTimeout);
			}
			this.initTimeout = window.setTimeout(initialize.bind(this), 0);
		}
	}

	function getContainerId(node) {
		var collections = [], len = arguments.length - 1;
		while ( len-- > 0 ) collections[ len ] = arguments[ len + 1 ];

		var id = null;
		each(collections, function (collection) {
			each(collection, function (container) {
				if (id === null && container.node === node) {
					id = container.id;
				}
			});
		});
		return id
	}

	/**
	 * Re-runs the reveal method for each record stored in history,
	 * for capturing new content asynchronously loaded into the DOM.
	 */
	function sync() {
		var this$1 = this;

		each(this.store.history, function (record) {
			reveal.call(this$1, record.target, record.options, true);
		});

		initialize.call(this);
	}

	var polyfill = function (x) { return (x > 0) - (x < 0) || +x; };
	var mathSign = Math.sign || polyfill;

	/*! @license miniraf v1.0.1

		Copyright 2018 Fisssion LLC.

		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in all
		copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
		SOFTWARE.

	*/
	var polyfill$1 = (function () {
		var clock = Date.now();

		return function (callback) {
			var currentTime = Date.now();
			if (currentTime - clock > 16) {
				clock = currentTime;
				callback(currentTime);
			} else {
				setTimeout(function () { return polyfill$1(callback); }, 0);
			}
		}
	})();

	var miniraf = window.requestAnimationFrame ||
		window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame ||
		polyfill$1;

	function getGeometry(target, isContainer) {
		/**
		 * We want to ignore padding and scrollbars for container elements.
		 * More information here: https://goo.gl/vOZpbz
		 */
		var height = isContainer ? target.node.clientHeight : target.node.offsetHeight;
		var width = isContainer ? target.node.clientWidth : target.node.offsetWidth;

		var offsetTop = 0;
		var offsetLeft = 0;
		var node = target.node;

		do {
			if (!isNaN(node.offsetTop)) {
				offsetTop += node.offsetTop;
			}
			if (!isNaN(node.offsetLeft)) {
				offsetLeft += node.offsetLeft;
			}
			node = node.offsetParent;
		} while (node)

		return {
			bounds: {
				top: offsetTop,
				right: offsetLeft + width,
				bottom: offsetTop + height,
				left: offsetLeft
			},
			height: height,
			width: width
		}
	}

	function getScrolled(container) {
		var top, left;
		if (container.node === document.documentElement) {
			top = window.pageYOffset;
			left = window.pageXOffset;
		} else {
			top = container.node.scrollTop;
			left = container.node.scrollLeft;
		}
		return { top: top, left: left }
	}

	function isElementVisible(element) {
		if ( element === void 0 ) element = {};

		var container = this.store.containers[element.containerId];
		if (!container) { return }

		var viewFactor = Math.max(0, Math.min(1, element.config.viewFactor));
		var viewOffset = element.config.viewOffset;

		var elementBounds = {
			top: element.geometry.bounds.top + element.geometry.height * viewFactor,
			right: element.geometry.bounds.right - element.geometry.width * viewFactor,
			bottom: element.geometry.bounds.bottom - element.geometry.height * viewFactor,
			left: element.geometry.bounds.left + element.geometry.width * viewFactor
		};

		var containerBounds = {
			top: container.geometry.bounds.top + container.scroll.top + viewOffset.top,
			right: container.geometry.bounds.right + container.scroll.left - viewOffset.right,
			bottom:
				container.geometry.bounds.bottom + container.scroll.top - viewOffset.bottom,
			left: container.geometry.bounds.left + container.scroll.left + viewOffset.left
		};

		return (
			(elementBounds.top < containerBounds.bottom &&
				elementBounds.right > containerBounds.left &&
				elementBounds.bottom > containerBounds.top &&
				elementBounds.left < containerBounds.right) ||
			element.styles.position === 'fixed'
		)
	}

	function delegate(
		event,
		elements
	) {
		var this$1 = this;
		if ( event === void 0 ) event = { type: 'init' };
		if ( elements === void 0 ) elements = this.store.elements;

		miniraf(function () {
			var stale = event.type === 'init' || event.type === 'resize';

			each(this$1.store.containers, function (container) {
				if (stale) {
					container.geometry = getGeometry.call(this$1, container, true);
				}
				var scroll = getScrolled.call(this$1, container);
				if (container.scroll) {
					container.direction = {
						x: mathSign(scroll.left - container.scroll.left),
						y: mathSign(scroll.top - container.scroll.top)
					};
				}
				container.scroll = scroll;
			});

			/**
			 * Due to how the sequencer is implemented, it’s
			 * important that we update the state of all
			 * elements, before any animation logic is
			 * evaluated (in the second loop below).
			 */
			each(elements, function (element) {
				if (stale) {
					element.geometry = getGeometry.call(this$1, element);
				}
				element.visible = isElementVisible.call(this$1, element);
			});

			each(elements, function (element) {
				if (element.sequence) {
					sequence.call(this$1, element);
				} else {
					animate.call(this$1, element);
				}
			});

			this$1.pristine = false;
		});
	}

	function isTransformSupported() {
		var style = document.documentElement.style;
		return 'transform' in style || 'WebkitTransform' in style
	}

	function isTransitionSupported() {
		var style = document.documentElement.style;
		return 'transition' in style || 'WebkitTransition' in style
	}

	var version = "4.0.6";

	var boundDelegate;
	var boundDestroy;
	var boundReveal;
	var boundClean;
	var boundSync;
	var config;
	var debug;
	var instance;

	function ScrollReveal(options) {
		if ( options === void 0 ) options = {};

		var invokedWithoutNew =
			typeof this === 'undefined' ||
			Object.getPrototypeOf(this) !== ScrollReveal.prototype;

		if (invokedWithoutNew) {
			return new ScrollReveal(options)
		}

		if (!ScrollReveal.isSupported()) {
			logger.call(this, 'Instantiation failed.', 'This browser is not supported.');
			return mount.failure()
		}

		var buffer;
		try {
			buffer = config
				? deepAssign({}, config, options)
				: deepAssign({}, defaults, options);
		} catch (e) {
			logger.call(this, 'Invalid configuration.', e.message);
			return mount.failure()
		}

		try {
			var container = tealight(buffer.container)[0];
			if (!container) {
				throw new Error('Invalid container.')
			}
		} catch (e) {
			logger.call(this, e.message);
			return mount.failure()
		}

		config = buffer;

		if ((!config.mobile && isMobile()) || (!config.desktop && !isMobile())) {
			logger.call(
				this,
				'This device is disabled.',
				("desktop: " + (config.desktop)),
				("mobile: " + (config.mobile))
			);
			return mount.failure()
		}

		mount.success();

		this.store = {
			containers: {},
			elements: {},
			history: [],
			sequences: {}
		};

		this.pristine = true;

		boundDelegate = boundDelegate || delegate.bind(this);
		boundDestroy = boundDestroy || destroy.bind(this);
		boundReveal = boundReveal || reveal.bind(this);
		boundClean = boundClean || clean.bind(this);
		boundSync = boundSync || sync.bind(this);

		Object.defineProperty(this, 'delegate', { get: function () { return boundDelegate; } });
		Object.defineProperty(this, 'destroy', { get: function () { return boundDestroy; } });
		Object.defineProperty(this, 'reveal', { get: function () { return boundReveal; } });
		Object.defineProperty(this, 'clean', { get: function () { return boundClean; } });
		Object.defineProperty(this, 'sync', { get: function () { return boundSync; } });

		Object.defineProperty(this, 'defaults', { get: function () { return config; } });
		Object.defineProperty(this, 'version', { get: function () { return version; } });
		Object.defineProperty(this, 'noop', { get: function () { return false; } });

		return instance ? instance : (instance = this)
	}

	ScrollReveal.isSupported = function () { return isTransformSupported() && isTransitionSupported(); };

	Object.defineProperty(ScrollReveal, 'debug', {
		get: function () { return debug || false; },
		set: function (value) { return (debug = typeof value === 'boolean' ? value : debug); }
	});

	ScrollReveal();

	return ScrollReveal;

}));

},{}],3:[function(require,module,exports){
!function(t,n){"object"==typeof exports&&"undefined"!=typeof module?module.exports=n():"function"==typeof define&&define.amd?define(n):(t=t||self).LazyLoad=n()}(this,(function(){"use strict";function t(){return(t=Object.assign||function(t){for(var n=1;n<arguments.length;n++){var e=arguments[n];for(var i in e)Object.prototype.hasOwnProperty.call(e,i)&&(t[i]=e[i])}return t}).apply(this,arguments)}var n="undefined"!=typeof window,e=n&&!("onscroll"in window)||"undefined"!=typeof navigator&&/(gle|ing|ro)bot|crawl|spider/i.test(navigator.userAgent),i=n&&"IntersectionObserver"in window,o=n&&"classList"in document.createElement("p"),a=n&&window.devicePixelRatio>1,r={elements_selector:"img",container:e||n?document:null,threshold:300,thresholds:null,data_src:"src",data_srcset:"srcset",data_sizes:"sizes",data_bg:"bg",data_bg_hidpi:"bg-hidpi",data_bg_multi:"bg-multi",data_bg_multi_hidpi:"bg-multi-hidpi",data_poster:"poster",class_applied:"applied",class_loading:"loading",class_loaded:"loaded",class_error:"error",load_delay:0,auto_unobserve:!0,cancel_on_exit:!1,callback_enter:null,callback_exit:null,callback_applied:null,callback_loading:null,callback_loaded:null,callback_error:null,callback_finish:null,callback_cancel:null,use_native:!1},c=function(n){return t({},r,n)},l=function(t,n){var e,i=new t(n);try{e=new CustomEvent("LazyLoad::Initialized",{detail:{instance:i}})}catch(t){(e=document.createEvent("CustomEvent")).initCustomEvent("LazyLoad::Initialized",!1,!1,{instance:i})}window.dispatchEvent(e)},s=function(t,n){return t.getAttribute("data-"+n)},u=function(t,n,e){var i="data-"+n;null!==e?t.setAttribute(i,e):t.removeAttribute(i)},d=function(t){return s(t,"ll-status")},f=function(t,n){return u(t,"ll-status",n)},_=function(t){return f(t,null)},g=function(t){return null===d(t)},v=function(t){return"delayed"===d(t)},b=["loading","applied","loaded","error"],p=function(t){return b.indexOf(d(t))>-1},m=function(t,n){return u(t,"ll-timeout",n)},h=function(t){return s(t,"ll-timeout")},E=function(t,n,e,i){t&&(void 0===i?void 0===e?t(n):t(n,e):t(n,e,i))},y=function(t,n){o?t.classList.add(n):t.className+=(t.className?" ":"")+n},L=function(t,n){o?t.classList.remove(n):t.className=t.className.replace(new RegExp("(^|\\s+)"+n+"(\\s+|$)")," ").replace(/^\s+/,"").replace(/\s+$/,"")},I=function(t){return t.llTempImage},k=function(t,n,e){if(e){var i=e._observer;i&&n.auto_unobserve&&i.unobserve(t)}},A=function(t){t&&(t.loadingCount+=1)},w=function(t){for(var n,e=[],i=0;n=t.children[i];i+=1)"SOURCE"===n.tagName&&e.push(n);return e},z=function(t,n,e){e&&t.setAttribute(n,e)},C=function(t,n){t.removeAttribute(n)},O=function(t){return!!t.llOriginalAttrs},x=function(t){if(!O(t)){var n={};n.src=t.getAttribute("src"),n.srcset=t.getAttribute("srcset"),n.sizes=t.getAttribute("sizes"),t.llOriginalAttrs=n}},N=function(t){if(O(t)){var n=t.llOriginalAttrs;z(t,"src",n.src),z(t,"srcset",n.srcset),z(t,"sizes",n.sizes)}},M=function(t,n){z(t,"sizes",s(t,n.data_sizes)),z(t,"srcset",s(t,n.data_srcset)),z(t,"src",s(t,n.data_src))},R=function(t){C(t,"src"),C(t,"srcset"),C(t,"sizes")},T=function(t,n){var e=t.parentNode;e&&"PICTURE"===e.tagName&&w(e).forEach(n)},G={IMG:function(t,n){T(t,(function(t){x(t),M(t,n)})),x(t),M(t,n)},IFRAME:function(t,n){z(t,"src",s(t,n.data_src))},VIDEO:function(t,n){w(t).forEach((function(t){z(t,"src",s(t,n.data_src))})),z(t,"poster",s(t,n.data_poster)),z(t,"src",s(t,n.data_src)),t.load()}},S=function(t,n,e){var i=G[t.tagName];i&&(i(t,n),A(e),y(t,n.class_loading),f(t,"loading"),E(n.callback_loading,t,e),E(n.callback_reveal,t,e))},j=["IMG","IFRAME","VIDEO"],D=function(t){t&&(t.loadingCount-=1)},F=function(t,n){!n||n.toLoadCount||n.loadingCount||E(t.callback_finish,n)},P=function(t,n,e){t.addEventListener(n,e),t.llEvLisnrs[n]=e},V=function(t,n,e){t.removeEventListener(n,e)},U=function(t){return!!t.llEvLisnrs},$=function(t){if(U(t)){var n=t.llEvLisnrs;for(var e in n){var i=n[e];V(t,e,i)}delete t.llEvLisnrs}},q=function(t,n,e){!function(t){delete t.llTempImage}(t),D(e),L(t,n.class_loading),k(t,n,e)},H=function(t,n,e){var i=I(t)||t;if(!U(i)){!function(t,n,e){U(t)||(t.llEvLisnrs={}),P(t,"load",n),P(t,"error",e),"VIDEO"===t.tagName&&P(t,"loadeddata",n)}(i,(function(o){!function(t,n,e,i){q(n,e,i),y(n,e.class_loaded),f(n,"loaded"),E(e.callback_loaded,n,i),F(e,i)}(0,t,n,e),$(i)}),(function(o){!function(t,n,e,i){q(n,e,i),y(n,e.class_error),f(n,"error"),E(e.callback_error,n,i),F(e,i)}(0,t,n,e),$(i)}))}},B=function(t){t&&(t.toLoadCount-=1)},J=function(t,n,e){!function(t){t.llTempImage=document.createElement("img")}(t),H(t,n,e),function(t,n,e){var i=s(t,n.data_bg),o=s(t,n.data_bg_hidpi),r=a&&o?o:i;r&&(t.style.backgroundImage='url("'.concat(r,'")'),I(t).setAttribute("src",r),A(e),y(t,n.class_loading),f(t,"loading"),E(n.callback_loading,t,e),E(n.callback_reveal,t,e))}(t,n,e),function(t,n,e){var i=s(t,n.data_bg_multi),o=s(t,n.data_bg_multi_hidpi),r=a&&o?o:i;r&&(t.style.backgroundImage=r,y(t,n.class_applied),f(t,"applied"),k(t,n,e),E(n.callback_applied,t,e))}(t,n,e)},K=function(t,n,e){!function(t){return j.indexOf(t.tagName)>-1}(t)?J(t,n,e):function(t,n,e){H(t,n,e),S(t,n,e)}(t,n,e),B(e),F(n,e)},Q=function(t){var n=h(t);n&&(v(t)&&_(t),clearTimeout(n),m(t,null))},W=function(t,n,e,i){"IMG"===t.tagName&&($(t),function(t){T(t,(function(t){R(t)})),R(t)}(t),function(t){T(t,(function(t){N(t)})),N(t)}(t),L(t,e.class_loading),D(i),E(e.callback_cancel,t,n,i),setTimeout((function(){i.resetElementStatus(t,i)}),0))},X=function(t,n,e,i){E(e.callback_enter,t,n,i),p(t)||(e.load_delay?function(t,n,e){var i=n.load_delay,o=h(t);o||(o=setTimeout((function(){K(t,n,e),Q(t)}),i),f(t,"delayed"),m(t,o))}(t,e,i):K(t,e,i))},Y=function(t,n,e,i){g(t)||(e.cancel_on_exit&&function(t){return"loading"===d(t)}(t)&&W(t,n,e,i),E(e.callback_exit,t,n,i),e.load_delay&&v(t)&&Q(t))},Z=["IMG","IFRAME"],tt=function(t){return t.use_native&&"loading"in HTMLImageElement.prototype},nt=function(t,n,e){t.forEach((function(t){-1!==Z.indexOf(t.tagName)&&(t.setAttribute("loading","lazy"),function(t,n,e){H(t,n,e),S(t,n,e),B(e),f(t,"native"),F(n,e)}(t,n,e))})),e.toLoadCount=0},et=function(t){var n=t._settings;i&&!tt(t._settings)&&(t._observer=new IntersectionObserver((function(e){!function(t,n,e){t.forEach((function(t){return function(t){return t.isIntersecting||t.intersectionRatio>0}(t)?X(t.target,t,n,e):Y(t.target,t,n,e)}))}(e,n,t)}),function(t){return{root:t.container===document?null:t.container,rootMargin:t.thresholds||t.threshold+"px"}}(n)))},it=function(t){return Array.prototype.slice.call(t)},ot=function(t){return t.container.querySelectorAll(t.elements_selector)},at=function(t){return function(t){return"error"===d(t)}(t)},rt=function(t,n){return function(t){return it(t).filter(g)}(t||ot(n))},ct=function(t){var n,e=t._settings;(n=ot(e),it(n).filter(at)).forEach((function(t){L(t,e.class_error),_(t)})),t.update()},lt=function(t,e){var i;this._settings=c(t),this.loadingCount=0,et(this),i=this,n&&window.addEventListener("online",(function(t){ct(i)})),this.update(e)};return lt.prototype={update:function(t){var n,o,a=this._settings,r=rt(t,a);(this.toLoadCount=r.length,!e&&i)?tt(a)?nt(r,a,this):(n=this._observer,o=r,function(t){t.disconnect()}(n),function(t,n){n.forEach((function(n){t.observe(n)}))}(n,o)):this.loadAll(r)},destroy:function(){this._observer&&this._observer.disconnect(),delete this._observer,delete this._settings,delete this.loadingCount,delete this.toLoadCount},loadAll:function(t){var n=this,e=this._settings;rt(t,e).forEach((function(t){K(t,e,n)}))},resetElementStatus:function(t){!function(t,n){p(t)&&function(t){t&&(t.toLoadCount+=1)}(n),f(t,null)}(t,this)},load:function(t){K(t,this._settings,this)}},lt.load=function(t,n){var e=c(n);K(t,e)},n&&function(t,n){if(n)if(n.length)for(var e,i=0;e=n[i];i+=1)l(t,e);else l(t,n)}(lt,window.lazyLoadOptions),lt}));

},{}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Header = void 0;

var _DOMReadyObject2 = require("../../utils/DOMReadyObject.js");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/**
 * Header component
 */
var Header =
/*#__PURE__*/
function (_DOMReadyObject) {
  _inherits(Header, _DOMReadyObject);

  function Header() {
    var _this;

    _classCallCheck(this, Header);

    console.log('1/ Header', 'constructor()');
    _this = _possibleConstructorReturn(this, _getPrototypeOf(Header).call(this));
    _this.class = '.header';
    _this.firstMove = true;
    _this.isOpenBurger = false;
    return _this;
  }

  _createClass(Header, [{
    key: "isDOMReady",
    value: function isDOMReady() {
      _get(_getPrototypeOf(Header.prototype), "isDOMReady", this).call(this);

      this.initView();
    }
  }, {
    key: "initView",
    value: function initView() {
      console.log('Header', 'initView()');
      this.$dom = document.querySelector(this.class);
      this.$burger = document.querySelector('.header_toggle'); //Si on a bien un header

      if (this.$dom != null) {
        this.initBurger();
        this.initEvents();
      }
    }
  }, {
    key: "initEvents",
    value: function initEvents() {
      var _this2 = this;

      //Au scroll si la direction a changée
      window.addEventListener('ScrollDirection_direction', function (e) {
        console.log(e.detail); //Si le menu est pas déjà ouvert

        if (_this2.$dom.classList.contains('header-open') == false) {
          //Si on va vers le haut
          if (e.detail.direction == "up") {
            _this2.show();
          } //Si on va vers le bas
          // ou que c'est le premier déplacement et qu'on a suffisament scrollé
          else if (e.detail.direction == "down" && e.detail.position > 0) {
              _this2.hide();
            }
        }
      });
    } //Afficher le header

  }, {
    key: "show",
    value: function show() {
      console.log('Header.show()');

      if (this.$dom.classList.contains('header-show') == false) {
        if (this.$dom.classList.contains('header-hidefirstmove')) {
          this.$dom.classList.remove('header-hidefirstmove');
        } else {
          this.$dom.classList.remove('header-hide');
        }

        this.$dom.classList.add('header-show');
      }
    } //Cacher le header

  }, {
    key: "hide",
    value: function hide() {
      console.log('Header.hide()');

      if (this.$dom.classList.contains('header-show') == true || this.firstMove == true) {
        if (this.firstMove == true) {
          this.$dom.classList.add('header-hidefirstmove');
          this.firstMove = false;
        } else {
          this.$dom.classList.add('header-hide');
        }

        this.$dom.classList.remove('header-show');
      }
    } //Toggle class header-openburger

  }, {
    key: "initBurger",
    value: function initBurger() {
      var _this3 = this;

      console.log('Header', 'initBurger');

      if (this.$burger != null) {
        this.boundOnClickBurger = function (e) {
          return _this3.onClickBurger(e);
        };

        this.$burger.removeEventListener('click', this.boundOnClickBurger);
        this.$burger.addEventListener('click', this.boundOnClickBurger);
      }
    }
  }, {
    key: "onClickBurger",
    value: function onClickBurger(e) {
      //State
      this.isOpenBurger = this.$dom.classList.toggle('header-open');
      console.log('Header', 'onClickBurger', ' / open burger ?', this.isOpenBurger); //Toggle noscroll

      this.toggleNoScroll();
      e.preventDefault();
      e.stopPropagation();
    } //Toggle no scroll sur le body

  }, {
    key: "toggleNoScroll",
    value: function toggleNoScroll() {
      console.log('Header.toggleNoScroll()');

      if (this.isOpenBurger === true) {
        new Promise(function (resolve) {
          return setTimeout(resolve, 500);
        }).then(function (e) {
          document.body.classList.add('noscroll');
        });
      } else {
        document.body.classList.remove('noscroll');
      }
    }
  }]);

  return Header;
}(_DOMReadyObject2.DOMReadyObject);

exports.Header = Header;

},{"../../utils/DOMReadyObject.js":7}],5:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = void 0;

var _lazyloadMin = _interopRequireDefault(require("../../../build/node_modules/vanilla-lazyload/dist/lazyload.min.js"));

var _DOMReadyObject2 = require("../../utils/DOMReadyObject.js");

var _Header = require("../../components/header/Header");

require("../../utils/VHMobile.js");

var _ScrollDirection = require("../../utils/ScrollDirection.js");

var _detailsElementPolyfill = _interopRequireDefault(require("../../../build/node_modules/details-element-polyfill/dist/details-element-polyfill.js"));

var _scrollreveal = _interopRequireDefault(require("../../../build/node_modules/scrollreveal/dist/scrollreveal.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/**
 * Index
 */
var Index =
/*#__PURE__*/
function (_DOMReadyObject) {
  _inherits(Index, _DOMReadyObject);

  function Index() {
    var _this;

    _classCallCheck(this, Index);

    console.log('Index.constructor()');
    _this = _possibleConstructorReturn(this, _getPrototypeOf(Index).call(this)); //IE 11

    _this.isIE11 = !!window.MSInputMethodContext && !!document.documentMode; //ladyload

    _this.lazyload === undefined ? _this.lazyload = new _lazyloadMin.default({
      elements_selector: ".lazy"
    }) : _this.lazyload.update(); //Create component

    _this.scrollDirection = new _ScrollDirection.ScrollDirection();
    _this.header = new _Header.Header();
    window.sr = new _scrollreveal.default();
    return _this;
  }

  _createClass(Index, [{
    key: "isDOMReady",
    value: function isDOMReady() {
      console.log('Index.isDOMReady()');

      _get(_getPrototypeOf(Index.prototype), "isDOMReady", this).call(this);

      this.animate();
    }
  }, {
    key: "animate",
    value: function animate() {
      var slideDown = {
        distance: '150%',
        origin: 'top',
        duration: 400,
        easing: 'cubic-bezier(0.390, 0.575, 0.565, 1.000)',
        opacity: 0
      };
      var slideUp = {
        distance: '150%',
        origin: 'bottom',
        duration: 400,
        easing: 'cubic-bezier(0.390, 0.575, 0.565, 1.000)',
        opacity: 0
      };
      var show = {
        duration: 1000,
        easing: 'cubic-bezier(0.390, 0.575, 0.565, 1.000)',
        opacity: 0
      }; //Hero

      window.sr.reveal('.hero_title', slideDown);
      window.sr.reveal('.hero_subtitle', _objectSpread({}, slideDown, {
        delay: 50
      }));
      window.sr.reveal('.hero_ctas', _objectSpread({}, slideDown, {
        delay: 150
      }));
      window.sr.reveal('.hero_illu', _objectSpread({}, show, {
        delay: 0,
        duration: 450
      })); //Livre blanc

      window.sr.reveal('.livreblanc_image', _objectSpread({}, show, {
        delay: 0,
        duration: 550
      }));
      window.sr.reveal('.livreblanc_title', _objectSpread({}, show, {
        delay: 0,
        duration: 350
      }));
      window.sr.reveal('.livreblanc_desc', _objectSpread({}, show, {
        delay: 150,
        duration: 350
      }));
      window.sr.reveal('.livreblanc_cta', _objectSpread({}, show, {
        delay: 150,
        duration: 350
      })); //Arugments

      window.sr.reveal('.arguments_container', _objectSpread({}, show, {
        delay: 250,
        duration: 150
      }));
      window.sr.reveal('.arguments_col', _objectSpread({}, slideDown, {
        delay: 450,
        duration: 350,
        distance: '25%',
        interval: 5
      })); //Feature vidéo

      window.sr.reveal('.featurevideo_title', _objectSpread({}, show, {
        delay: 0,
        duration: 350
      }));
      window.sr.reveal('.featurevideo_videocontainer', _objectSpread({}, show, {
        delay: 50,
        duration: 350
      })); //Features

      window.sr.reveal('.features_title', _objectSpread({}, show, {
        delay: 0,
        duration: 350
      }));
      window.sr.reveal('.features_item', _objectSpread({}, show, {
        delay: 50,
        duration: 350,
        distance: '50%',
        interval: 5
      })); //Getlead

      window.sr.reveal('.getlead_title', _objectSpread({}, slideDown, {
        delay: 0,
        duration: 350
      }));
      window.sr.reveal('.getlead_subtitle', _objectSpread({}, slideDown, {
        delay: 150,
        duration: 350
      }));
      window.sr.reveal('.getleader_cta', _objectSpread({}, slideDown, {
        delay: 150,
        duration: 350
      })); //window.sr.reveal('.getleader_illu',{...show, ...{delay:0,duration:550}});
      //About

      window.sr.reveal('.whoweare_image', _objectSpread({}, show, {
        delay: 0,
        duration: 550
      }));
      window.sr.reveal('.whoweare_title', _objectSpread({}, show, {
        delay: 0,
        duration: 350
      }));
      window.sr.reveal('.whoweare_desc', _objectSpread({}, show, {
        delay: 150,
        duration: 350
      }));
      window.sr.reveal('.whoweare_cta', _objectSpread({}, show, {
        delay: 150,
        duration: 350
      })); //Testimonials

      window.sr.reveal('.testimonials_title', _objectSpread({}, show, {
        delay: 0,
        duration: 350
      }));
      window.sr.reveal('.testimonials_item', _objectSpread({}, show, {
        delay: 150,
        duration: 350,
        interval: 5
      })); //Partners

      window.sr.reveal('.partners_title', _objectSpread({}, show, {
        delay: 0,
        duration: 350
      }));
      window.sr.reveal('.partners_item', _objectSpread({}, show, {
        delay: 150,
        duration: 350,
        interval: 5
      })); //Timeline

      window.sr.reveal('.timeline_title', _objectSpread({}, show, {
        delay: 0,
        duration: 350
      }));
      window.sr.reveal('.timeline_start', _objectSpread({}, show, {
        delay: 150,
        duration: 350
      }));
      window.sr.reveal('.timeline_item', _objectSpread({}, show, {
        delay: 150,
        duration: 350
      }));
      window.sr.reveal('.timeline_end', _objectSpread({}, show, {
        delay: 150,
        duration: 350
      })); //After

      window.sr.reveal('.after_subtitle', _objectSpread({}, show, {
        delay: 0,
        duration: 350
      }));
      window.sr.reveal('.after_subtitle', _objectSpread({}, show, {
        delay: 150,
        duration: 350
      }));
      window.sr.reveal('.after_item', _objectSpread({}, show, {
        delay: 150,
        duration: 350,
        interval: 5
      }));
    }
  }]);

  return Index;
}(_DOMReadyObject2.DOMReadyObject);

var index = new Index();
exports.index = index;

},{"../../../build/node_modules/details-element-polyfill/dist/details-element-polyfill.js":1,"../../../build/node_modules/scrollreveal/dist/scrollreveal.js":2,"../../../build/node_modules/vanilla-lazyload/dist/lazyload.min.js":3,"../../components/header/Header":4,"../../utils/DOMReadyObject.js":7,"../../utils/ScrollDirection.js":8,"../../utils/VHMobile.js":9}],6:[function(require,module,exports){
"use strict";

var _Index = _interopRequireDefault(require("../layout/default/Index.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

},{"../layout/default/Index.js":5}],7:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DOMReadyObject = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

//Document ready Promise
document.ready = function () {
  return new Promise(function (resolve) {
    if (document.readyState === 'complete') {
      resolve();
    } else {
      document.addEventListener('DOMContentLoaded', resolve);
    }
  });
};
/**
 * DOMReadyObject
 * abstract class
 */


var DOMReadyObject =
/*#__PURE__*/
function () {
  function DOMReadyObject() {
    var _this = this;

    _classCallCheck(this, DOMReadyObject);

    //On ne peut pas instancier la class directement
    if (this.constructor === DOMReadyObject) {
      throw new Error('Cannot construct DOMReadyObject instances directly');
    }

    document.ready().then(function () {
      _this.isDOMReady();
    }).catch(function (err) {
      return console.warn(err);
    });
  }

  _createClass(DOMReadyObject, [{
    key: "isDOMReady",
    value: function isDOMReady() {}
  }]);

  return DOMReadyObject;
}();

exports.DOMReadyObject = DOMReadyObject;

},{}],8:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ScrollDirection = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var ScrollDirection =
/*#__PURE__*/
function () {
  function ScrollDirection() {
    var _this = this;

    _classCallCheck(this, ScrollDirection);

    this.scrollData = {
      position: null,
      direction: null,
      hasDirectionChange: false
    };
    window.addEventListener('scroll', function (e) {
      return _this.setScrollDirection(e);
    });
  }

  _createClass(ScrollDirection, [{
    key: "setScrollDirection",
    value: function setScrollDirection(e) {
      var newScrollData = {};
      newScrollData.position = (window.pageYOffset || document.documentElement.scrollTop) - (document.documentElement.clientTop || 0); //On stocke la direction

      if (newScrollData.position > this.scrollData.position) {
        newScrollData.direction = 'down';
      } else {
        newScrollData.direction = 'up';
      } //Si on change de direction


      if (this.scrollData.direction != newScrollData.direction) {
        newScrollData.hasDirectionChange = true; //On dispatch un event lorsque la direction du scroll a changée

        window.dispatchEvent(new CustomEvent('ScrollDirection_direction', {
          detail: newScrollData
        }));
      } //On dispatch un event pour chaque mouvement du scroll


      window.dispatchEvent(new CustomEvent('ScrollDirection_scroll', {
        detail: newScrollData
      })); //On stocke les données

      this.scrollData = newScrollData;
    }
  }]);

  return ScrollDirection;
}();

exports.ScrollDirection = ScrollDirection;

},{}],9:[function(require,module,exports){
/**
 * From https://css-tricks.com/the-trick-to-viewport-units-on-mobile/
 */
var vh = window.innerHeight * 0.01; // Then we set the value in the --vh custom property to the root of the document

document.documentElement.style.setProperty('--vh', "".concat(vh, "px")); // We listen to the resize event

window.addEventListener('resize', function () {
  // We execute the same script as before
  var vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', "".concat(vh, "px"));
});

},{}]},{},[6])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJub2RlX21vZHVsZXMvZGV0YWlscy1lbGVtZW50LXBvbHlmaWxsL2Rpc3QvZGV0YWlscy1lbGVtZW50LXBvbHlmaWxsLmpzIiwibm9kZV9tb2R1bGVzL3Njcm9sbHJldmVhbC9kaXN0L3Njcm9sbHJldmVhbC5qcyIsIm5vZGVfbW9kdWxlcy92YW5pbGxhLWxhenlsb2FkL2Rpc3QvbGF6eWxvYWQubWluLmpzIiwiLi4vc3JjL2NvbXBvbmVudHMvaGVhZGVyL0hlYWRlci5qcyIsIi4uL3NyYy9sYXlvdXQvZGVmYXVsdC9JbmRleC5qcyIsIi4uL3NyYy9sb2FkZXIvbWFpbi5qcyIsIi4uL3NyYy91dGlscy9ET01SZWFkeU9iamVjdC5qcyIsIi4uL3NyYy91dGlscy9TY3JvbGxEaXJlY3Rpb24uanMiLCIuLi9zcmMvdXRpbHMvVkhNb2JpbGUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbE1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDMWdEQTtBQUNBOzs7Ozs7Ozs7QUNEQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUE7OztJQUdhLE07Ozs7O0FBRVQsb0JBQWM7QUFBQTs7QUFBQTs7QUFDVixJQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksV0FBWixFQUF5QixlQUF6QjtBQUNBO0FBQ04sVUFBSyxLQUFMLEdBQWEsU0FBYjtBQUNBLFVBQUssU0FBTCxHQUFpQixJQUFqQjtBQUNNLFVBQUssWUFBTCxHQUFvQixLQUFwQjtBQUxVO0FBTWI7Ozs7aUNBRVk7QUFDVDs7QUFDQSxXQUFLLFFBQUw7QUFDSDs7OytCQUVTO0FBQ04sTUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLFFBQVosRUFBc0IsWUFBdEI7QUFDQSxXQUFLLElBQUwsR0FBWSxRQUFRLENBQUMsYUFBVCxDQUF1QixLQUFLLEtBQTVCLENBQVo7QUFDQSxXQUFLLE9BQUwsR0FBZSxRQUFRLENBQUMsYUFBVCxDQUF1QixnQkFBdkIsQ0FBZixDQUhNLENBS047O0FBQ0EsVUFBSSxLQUFLLElBQUwsSUFBYSxJQUFqQixFQUFzQjtBQUMzQixhQUFLLFVBQUw7QUFDQSxhQUFLLFVBQUw7QUFDTTtBQUNQOzs7aUNBRVc7QUFBQTs7QUFFWDtBQUNNLE1BQUEsTUFBTSxDQUFDLGdCQUFQLENBQXdCLDJCQUF4QixFQUFxRCxVQUFDLENBQUQsRUFBTztBQUVqRSxRQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksQ0FBQyxDQUFDLE1BQWQsRUFGaUUsQ0FJeEQ7O0FBQ0EsWUFBSSxNQUFJLENBQUMsSUFBTCxDQUFVLFNBQVYsQ0FBb0IsUUFBcEIsQ0FBNkIsYUFBN0IsS0FBK0MsS0FBbkQsRUFBMEQ7QUFFdEQ7QUFDQSxjQUFJLENBQUMsQ0FBQyxNQUFGLENBQVMsU0FBVCxJQUFzQixJQUExQixFQUFnQztBQUM1QixZQUFBLE1BQUksQ0FBQyxJQUFMO0FBQ0gsV0FGRCxDQUlBO0FBQ0E7QUFMQSxlQU1LLElBQUssQ0FBQyxDQUFDLE1BQUYsQ0FBUyxTQUFULElBQXNCLE1BQXZCLElBQW1DLENBQUMsQ0FBQyxNQUFGLENBQVMsUUFBVCxHQUFvQixDQUEzRCxFQUErRDtBQUNoRSxjQUFBLE1BQUksQ0FBQyxJQUFMO0FBQ0g7QUFFSjtBQUNKLE9BbkJEO0FBb0JOLEssQ0FFQTs7OzsyQkFDTTtBQUNBLE1BQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxlQUFaOztBQUNBLFVBQUksS0FBSyxJQUFMLENBQVUsU0FBVixDQUFvQixRQUFwQixDQUE2QixhQUE3QixLQUErQyxLQUFuRCxFQUEwRDtBQUN0RCxZQUFJLEtBQUssSUFBTCxDQUFVLFNBQVYsQ0FBb0IsUUFBcEIsQ0FBNkIsc0JBQTdCLENBQUosRUFBMEQ7QUFDdEQsZUFBSyxJQUFMLENBQVUsU0FBVixDQUFvQixNQUFwQixDQUEyQixzQkFBM0I7QUFDSCxTQUZELE1BR0k7QUFDQSxlQUFLLElBQUwsQ0FBVSxTQUFWLENBQW9CLE1BQXBCLENBQTJCLGFBQTNCO0FBQ0g7O0FBQ0QsYUFBSyxJQUFMLENBQVUsU0FBVixDQUFvQixHQUFwQixDQUF3QixhQUF4QjtBQUNIO0FBQ0osSyxDQUVEOzs7OzJCQUNNO0FBQ0YsTUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLGVBQVo7O0FBQ0EsVUFBSSxLQUFLLElBQUwsQ0FBVSxTQUFWLENBQW9CLFFBQXBCLENBQTZCLGFBQTdCLEtBQStDLElBQS9DLElBQXVELEtBQUssU0FBTCxJQUFrQixJQUE3RSxFQUFtRjtBQUMvRSxZQUFJLEtBQUssU0FBTCxJQUFrQixJQUF0QixFQUEyQjtBQUN2QixlQUFLLElBQUwsQ0FBVSxTQUFWLENBQW9CLEdBQXBCLENBQXdCLHNCQUF4QjtBQUNBLGVBQUssU0FBTCxHQUFpQixLQUFqQjtBQUNILFNBSEQsTUFHSztBQUNELGVBQUssSUFBTCxDQUFVLFNBQVYsQ0FBb0IsR0FBcEIsQ0FBd0IsYUFBeEI7QUFDSDs7QUFDRCxhQUFLLElBQUwsQ0FBVSxTQUFWLENBQW9CLE1BQXBCLENBQTJCLGFBQTNCO0FBQ0g7QUFDSixLLENBRUQ7Ozs7aUNBQ1k7QUFBQTs7QUFDUixNQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksUUFBWixFQUFzQixZQUF0Qjs7QUFDQSxVQUFJLEtBQUssT0FBTCxJQUFnQixJQUFwQixFQUEwQjtBQUN0QixhQUFLLGtCQUFMLEdBQTBCLFVBQUEsQ0FBQztBQUFBLGlCQUFJLE1BQUksQ0FBQyxhQUFMLENBQW1CLENBQW5CLENBQUo7QUFBQSxTQUEzQjs7QUFDQSxhQUFLLE9BQUwsQ0FBYSxtQkFBYixDQUFpQyxPQUFqQyxFQUEwQyxLQUFLLGtCQUEvQztBQUNBLGFBQUssT0FBTCxDQUFhLGdCQUFiLENBQThCLE9BQTlCLEVBQXVDLEtBQUssa0JBQTVDO0FBQ0g7QUFDSjs7O2tDQUVhLEMsRUFBRTtBQUVaO0FBQ0EsV0FBSyxZQUFMLEdBQW9CLEtBQUssSUFBTCxDQUFVLFNBQVYsQ0FBb0IsTUFBcEIsQ0FBMkIsYUFBM0IsQ0FBcEI7QUFDQSxNQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksUUFBWixFQUFzQixlQUF0QixFQUF1QyxrQkFBdkMsRUFBMkQsS0FBSyxZQUFoRSxFQUpZLENBTVo7O0FBQ0EsV0FBSyxjQUFMO0FBRUEsTUFBQSxDQUFDLENBQUMsY0FBRjtBQUNBLE1BQUEsQ0FBQyxDQUFDLGVBQUY7QUFDSCxLLENBRUQ7Ozs7cUNBQ2dCO0FBQ1osTUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLHlCQUFaOztBQUNBLFVBQUksS0FBSyxZQUFMLEtBQXNCLElBQTFCLEVBQWdDO0FBQzNCLFlBQUksT0FBSixDQUFZLFVBQUMsT0FBRDtBQUFBLGlCQUFhLFVBQVUsQ0FBQyxPQUFELEVBQVUsR0FBVixDQUF2QjtBQUFBLFNBQVosQ0FBRCxDQUFxRCxJQUFyRCxDQUEwRCxVQUFDLENBQUQsRUFBTztBQUM3RCxVQUFBLFFBQVEsQ0FBQyxJQUFULENBQWMsU0FBZCxDQUF3QixHQUF4QixDQUE0QixVQUE1QjtBQUNILFNBRkQ7QUFHSCxPQUpELE1BS0s7QUFDRCxRQUFBLFFBQVEsQ0FBQyxJQUFULENBQWMsU0FBZCxDQUF3QixNQUF4QixDQUErQixVQUEvQjtBQUNIO0FBQ0o7Ozs7RUFsSHVCLCtCOzs7Ozs7Ozs7Ozs7QUNMNUI7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBOzs7SUFHTSxLOzs7OztBQUVGLG1CQUFjO0FBQUE7O0FBQUE7O0FBQ1YsSUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLHFCQUFaO0FBQ0EsZ0ZBRlUsQ0FJVjs7QUFDQSxVQUFLLE1BQUwsR0FBYyxDQUFDLENBQUMsTUFBTSxDQUFDLG9CQUFULElBQWlDLENBQUMsQ0FBQyxRQUFRLENBQUMsWUFBMUQsQ0FMVSxDQU9WOztBQUNDLFVBQUssUUFBTCxLQUFrQixTQUFuQixHQUFnQyxNQUFLLFFBQUwsR0FBZ0IsSUFBSSxvQkFBSixDQUFhO0FBQUUsTUFBQSxpQkFBaUIsRUFBRTtBQUFyQixLQUFiLENBQWhELEdBQStGLE1BQUssUUFBTCxDQUFjLE1BQWQsRUFBL0YsQ0FSVSxDQVVWOztBQUNBLFVBQUssZUFBTCxHQUF1QixJQUFJLGdDQUFKLEVBQXZCO0FBQ0EsVUFBSyxNQUFMLEdBQXVCLElBQUksY0FBSixFQUF2QjtBQUVBLElBQUEsTUFBTSxDQUFDLEVBQVAsR0FBeUIsSUFBSSxxQkFBSixFQUF6QjtBQWRVO0FBZWI7Ozs7aUNBRVk7QUFDZixNQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksb0JBQVo7O0FBQ007O0FBQ0EsV0FBSyxPQUFMO0FBQ0g7Ozs4QkFFUTtBQUNMLFVBQUksU0FBUyxHQUFHO0FBQ1osUUFBQSxRQUFRLEVBQUUsTUFERTtBQUVaLFFBQUEsTUFBTSxFQUFFLEtBRkk7QUFHWixRQUFBLFFBQVEsRUFBRSxHQUhFO0FBSVosUUFBQSxNQUFNLEVBQUMsMENBSks7QUFLWixRQUFBLE9BQU8sRUFBRTtBQUxHLE9BQWhCO0FBUUEsVUFBSSxPQUFPLEdBQUc7QUFDVixRQUFBLFFBQVEsRUFBRSxNQURBO0FBRVYsUUFBQSxNQUFNLEVBQUUsUUFGRTtBQUdWLFFBQUEsUUFBUSxFQUFFLEdBSEE7QUFJVixRQUFBLE1BQU0sRUFBQywwQ0FKRztBQUtWLFFBQUEsT0FBTyxFQUFFO0FBTEMsT0FBZDtBQVFBLFVBQUksSUFBSSxHQUFHO0FBQ1AsUUFBQSxRQUFRLEVBQUUsSUFESDtBQUVQLFFBQUEsTUFBTSxFQUFDLDBDQUZBO0FBR1AsUUFBQSxPQUFPLEVBQUU7QUFIRixPQUFYLENBakJLLENBdUJMOztBQUNBLE1BQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxNQUFWLENBQWlCLGFBQWpCLEVBQStCLFNBQS9CO0FBQ0EsTUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLE1BQVYsQ0FBaUIsZ0JBQWpCLG9CQUFzQyxTQUF0QyxFQUFvRDtBQUFDLFFBQUEsS0FBSyxFQUFDO0FBQVAsT0FBcEQ7QUFDQSxNQUFBLE1BQU0sQ0FBQyxFQUFQLENBQVUsTUFBVixDQUFpQixZQUFqQixvQkFBa0MsU0FBbEMsRUFBZ0Q7QUFBQyxRQUFBLEtBQUssRUFBQztBQUFQLE9BQWhEO0FBQ0EsTUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLE1BQVYsQ0FBaUIsWUFBakIsb0JBQWtDLElBQWxDLEVBQTJDO0FBQUMsUUFBQSxLQUFLLEVBQUMsQ0FBUDtBQUFTLFFBQUEsUUFBUSxFQUFDO0FBQWxCLE9BQTNDLEdBM0JLLENBNkJMOztBQUNBLE1BQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxNQUFWLENBQWlCLG1CQUFqQixvQkFBeUMsSUFBekMsRUFBa0Q7QUFBQyxRQUFBLEtBQUssRUFBQyxDQUFQO0FBQVMsUUFBQSxRQUFRLEVBQUM7QUFBbEIsT0FBbEQ7QUFDQSxNQUFBLE1BQU0sQ0FBQyxFQUFQLENBQVUsTUFBVixDQUFpQixtQkFBakIsb0JBQXlDLElBQXpDLEVBQWtEO0FBQUMsUUFBQSxLQUFLLEVBQUMsQ0FBUDtBQUFTLFFBQUEsUUFBUSxFQUFDO0FBQWxCLE9BQWxEO0FBQ0EsTUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLE1BQVYsQ0FBaUIsa0JBQWpCLG9CQUF3QyxJQUF4QyxFQUFpRDtBQUFDLFFBQUEsS0FBSyxFQUFDLEdBQVA7QUFBVyxRQUFBLFFBQVEsRUFBQztBQUFwQixPQUFqRDtBQUNBLE1BQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxNQUFWLENBQWlCLGlCQUFqQixvQkFBdUMsSUFBdkMsRUFBZ0Q7QUFBQyxRQUFBLEtBQUssRUFBQyxHQUFQO0FBQVcsUUFBQSxRQUFRLEVBQUM7QUFBcEIsT0FBaEQsR0FqQ0ssQ0FtQ0w7O0FBQ0EsTUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLE1BQVYsQ0FBaUIsc0JBQWpCLG9CQUE0QyxJQUE1QyxFQUFxRDtBQUFDLFFBQUEsS0FBSyxFQUFDLEdBQVA7QUFBVyxRQUFBLFFBQVEsRUFBQztBQUFwQixPQUFyRDtBQUNBLE1BQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxNQUFWLENBQWlCLGdCQUFqQixvQkFBc0MsU0FBdEMsRUFBb0Q7QUFBQyxRQUFBLEtBQUssRUFBQyxHQUFQO0FBQVcsUUFBQSxRQUFRLEVBQUMsR0FBcEI7QUFBd0IsUUFBQSxRQUFRLEVBQUMsS0FBakM7QUFBdUMsUUFBQSxRQUFRLEVBQUU7QUFBakQsT0FBcEQsR0FyQ0ssQ0F1Q0w7O0FBQ0EsTUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLE1BQVYsQ0FBaUIscUJBQWpCLG9CQUEyQyxJQUEzQyxFQUFvRDtBQUFDLFFBQUEsS0FBSyxFQUFDLENBQVA7QUFBUyxRQUFBLFFBQVEsRUFBQztBQUFsQixPQUFwRDtBQUNBLE1BQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxNQUFWLENBQWlCLDhCQUFqQixvQkFBb0QsSUFBcEQsRUFBNkQ7QUFBQyxRQUFBLEtBQUssRUFBQyxFQUFQO0FBQVUsUUFBQSxRQUFRLEVBQUM7QUFBbkIsT0FBN0QsR0F6Q0ssQ0EyQ0w7O0FBQ0EsTUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLE1BQVYsQ0FBaUIsaUJBQWpCLG9CQUF1QyxJQUF2QyxFQUFnRDtBQUFDLFFBQUEsS0FBSyxFQUFDLENBQVA7QUFBUyxRQUFBLFFBQVEsRUFBQztBQUFsQixPQUFoRDtBQUNBLE1BQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxNQUFWLENBQWlCLGdCQUFqQixvQkFBc0MsSUFBdEMsRUFBK0M7QUFBQyxRQUFBLEtBQUssRUFBQyxFQUFQO0FBQVUsUUFBQSxRQUFRLEVBQUMsR0FBbkI7QUFBd0IsUUFBQSxRQUFRLEVBQUMsS0FBakM7QUFBdUMsUUFBQSxRQUFRLEVBQUU7QUFBakQsT0FBL0MsR0E3Q0ssQ0ErQ0w7O0FBQ0EsTUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLE1BQVYsQ0FBaUIsZ0JBQWpCLG9CQUFzQyxTQUF0QyxFQUFvRDtBQUFDLFFBQUEsS0FBSyxFQUFDLENBQVA7QUFBUyxRQUFBLFFBQVEsRUFBQztBQUFsQixPQUFwRDtBQUNBLE1BQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxNQUFWLENBQWlCLG1CQUFqQixvQkFBeUMsU0FBekMsRUFBdUQ7QUFBQyxRQUFBLEtBQUssRUFBQyxHQUFQO0FBQVcsUUFBQSxRQUFRLEVBQUM7QUFBcEIsT0FBdkQ7QUFDQSxNQUFBLE1BQU0sQ0FBQyxFQUFQLENBQVUsTUFBVixDQUFpQixnQkFBakIsb0JBQXNDLFNBQXRDLEVBQW9EO0FBQUMsUUFBQSxLQUFLLEVBQUMsR0FBUDtBQUFXLFFBQUEsUUFBUSxFQUFDO0FBQXBCLE9BQXBELEdBbERLLENBbURMO0FBRUE7O0FBQ0EsTUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLE1BQVYsQ0FBaUIsaUJBQWpCLG9CQUF1QyxJQUF2QyxFQUFnRDtBQUFDLFFBQUEsS0FBSyxFQUFDLENBQVA7QUFBUyxRQUFBLFFBQVEsRUFBQztBQUFsQixPQUFoRDtBQUNBLE1BQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxNQUFWLENBQWlCLGlCQUFqQixvQkFBdUMsSUFBdkMsRUFBZ0Q7QUFBQyxRQUFBLEtBQUssRUFBQyxDQUFQO0FBQVMsUUFBQSxRQUFRLEVBQUM7QUFBbEIsT0FBaEQ7QUFDQSxNQUFBLE1BQU0sQ0FBQyxFQUFQLENBQVUsTUFBVixDQUFpQixnQkFBakIsb0JBQXNDLElBQXRDLEVBQStDO0FBQUMsUUFBQSxLQUFLLEVBQUMsR0FBUDtBQUFXLFFBQUEsUUFBUSxFQUFDO0FBQXBCLE9BQS9DO0FBQ0EsTUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLE1BQVYsQ0FBaUIsZUFBakIsb0JBQXFDLElBQXJDLEVBQThDO0FBQUMsUUFBQSxLQUFLLEVBQUMsR0FBUDtBQUFXLFFBQUEsUUFBUSxFQUFDO0FBQXBCLE9BQTlDLEdBekRLLENBMkRMOztBQUNBLE1BQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxNQUFWLENBQWlCLHFCQUFqQixvQkFBMkMsSUFBM0MsRUFBb0Q7QUFBQyxRQUFBLEtBQUssRUFBQyxDQUFQO0FBQVMsUUFBQSxRQUFRLEVBQUM7QUFBbEIsT0FBcEQ7QUFDQSxNQUFBLE1BQU0sQ0FBQyxFQUFQLENBQVUsTUFBVixDQUFpQixvQkFBakIsb0JBQTBDLElBQTFDLEVBQW1EO0FBQUMsUUFBQSxLQUFLLEVBQUMsR0FBUDtBQUFXLFFBQUEsUUFBUSxFQUFDLEdBQXBCO0FBQXdCLFFBQUEsUUFBUSxFQUFFO0FBQWxDLE9BQW5ELEdBN0RLLENBK0RMOztBQUNBLE1BQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxNQUFWLENBQWlCLGlCQUFqQixvQkFBdUMsSUFBdkMsRUFBZ0Q7QUFBQyxRQUFBLEtBQUssRUFBQyxDQUFQO0FBQVMsUUFBQSxRQUFRLEVBQUM7QUFBbEIsT0FBaEQ7QUFDQSxNQUFBLE1BQU0sQ0FBQyxFQUFQLENBQVUsTUFBVixDQUFpQixnQkFBakIsb0JBQXNDLElBQXRDLEVBQStDO0FBQUMsUUFBQSxLQUFLLEVBQUMsR0FBUDtBQUFXLFFBQUEsUUFBUSxFQUFDLEdBQXBCO0FBQXdCLFFBQUEsUUFBUSxFQUFFO0FBQWxDLE9BQS9DLEdBakVLLENBbUVMOztBQUNBLE1BQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxNQUFWLENBQWlCLGlCQUFqQixvQkFBdUMsSUFBdkMsRUFBZ0Q7QUFBQyxRQUFBLEtBQUssRUFBQyxDQUFQO0FBQVMsUUFBQSxRQUFRLEVBQUM7QUFBbEIsT0FBaEQ7QUFDQSxNQUFBLE1BQU0sQ0FBQyxFQUFQLENBQVUsTUFBVixDQUFpQixpQkFBakIsb0JBQXVDLElBQXZDLEVBQWdEO0FBQUMsUUFBQSxLQUFLLEVBQUMsR0FBUDtBQUFXLFFBQUEsUUFBUSxFQUFDO0FBQXBCLE9BQWhEO0FBQ0EsTUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLE1BQVYsQ0FBaUIsZ0JBQWpCLG9CQUFzQyxJQUF0QyxFQUErQztBQUFDLFFBQUEsS0FBSyxFQUFDLEdBQVA7QUFBVyxRQUFBLFFBQVEsRUFBQztBQUFwQixPQUEvQztBQUNBLE1BQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxNQUFWLENBQWlCLGVBQWpCLG9CQUFxQyxJQUFyQyxFQUE4QztBQUFDLFFBQUEsS0FBSyxFQUFDLEdBQVA7QUFBVyxRQUFBLFFBQVEsRUFBQztBQUFwQixPQUE5QyxHQXZFSyxDQXlFTDs7QUFDQSxNQUFBLE1BQU0sQ0FBQyxFQUFQLENBQVUsTUFBVixDQUFpQixpQkFBakIsb0JBQXVDLElBQXZDLEVBQWdEO0FBQUMsUUFBQSxLQUFLLEVBQUMsQ0FBUDtBQUFTLFFBQUEsUUFBUSxFQUFDO0FBQWxCLE9BQWhEO0FBQ0EsTUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLE1BQVYsQ0FBaUIsaUJBQWpCLG9CQUF1QyxJQUF2QyxFQUFnRDtBQUFDLFFBQUEsS0FBSyxFQUFDLEdBQVA7QUFBVyxRQUFBLFFBQVEsRUFBQztBQUFwQixPQUFoRDtBQUNBLE1BQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxNQUFWLENBQWlCLGFBQWpCLG9CQUFtQyxJQUFuQyxFQUE0QztBQUFDLFFBQUEsS0FBSyxFQUFDLEdBQVA7QUFBVyxRQUFBLFFBQVEsRUFBQyxHQUFwQjtBQUF3QixRQUFBLFFBQVEsRUFBRTtBQUFsQyxPQUE1QztBQUVIOzs7O0VBdkdlLCtCOztBQTBHYixJQUFNLEtBQUssR0FBRyxJQUFJLEtBQUosRUFBZDs7Ozs7O0FDckhQOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUNBLFFBQVEsQ0FBQyxLQUFULEdBQWlCLFlBQU07QUFDbkIsU0FBTyxJQUFJLE9BQUosQ0FBWSxVQUFBLE9BQU8sRUFBSTtBQUMxQixRQUFJLFFBQVEsQ0FBQyxVQUFULEtBQXdCLFVBQTVCLEVBQXdDO0FBQ3BDLE1BQUEsT0FBTztBQUNWLEtBRkQsTUFFTztBQUNILE1BQUEsUUFBUSxDQUFDLGdCQUFULENBQTBCLGtCQUExQixFQUE4QyxPQUE5QztBQUNIO0FBQ0osR0FOTSxDQUFQO0FBT0gsQ0FSRDtBQVVBOzs7Ozs7SUFJYSxjOzs7QUFDVCw0QkFBYztBQUFBOztBQUFBOztBQUNWO0FBQ0EsUUFBSSxLQUFLLFdBQUwsS0FBcUIsY0FBekIsRUFBeUM7QUFDckMsWUFBTSxJQUFJLEtBQUosQ0FDRixvREFERSxDQUFOO0FBR0g7O0FBRUQsSUFBQSxRQUFRLENBQUMsS0FBVCxHQUFpQixJQUFqQixDQUFzQixZQUFNO0FBQ3hCLE1BQUEsS0FBSSxDQUFDLFVBQUw7QUFDSCxLQUZELEVBRUcsS0FGSCxDQUVTLFVBQUEsR0FBRztBQUFBLGFBQUksT0FBTyxDQUFDLElBQVIsQ0FBYSxHQUFiLENBQUo7QUFBQSxLQUZaO0FBR0g7Ozs7aUNBRVksQ0FBRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQzdCUCxlOzs7QUFFVCw2QkFBYztBQUFBOztBQUFBOztBQUNWLFNBQUssVUFBTCxHQUFrQjtBQUNkLE1BQUEsUUFBUSxFQUFFLElBREk7QUFFZCxNQUFBLFNBQVMsRUFBRSxJQUZHO0FBR2QsTUFBQSxrQkFBa0IsRUFBRTtBQUhOLEtBQWxCO0FBTUEsSUFBQSxNQUFNLENBQUMsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsVUFBQyxDQUFEO0FBQUEsYUFBTyxLQUFJLENBQUMsa0JBQUwsQ0FBd0IsQ0FBeEIsQ0FBUDtBQUFBLEtBQWxDO0FBQ0g7Ozs7dUNBRWtCLEMsRUFBRztBQUNsQixVQUFJLGFBQWEsR0FBRyxFQUFwQjtBQUNBLE1BQUEsYUFBYSxDQUFDLFFBQWQsR0FBeUIsQ0FBQyxNQUFNLENBQUMsV0FBUCxJQUFzQixRQUFRLENBQUMsZUFBVCxDQUF5QixTQUFoRCxLQUE4RCxRQUFRLENBQUMsZUFBVCxDQUF5QixTQUF6QixJQUFzQyxDQUFwRyxDQUF6QixDQUZrQixDQUlsQjs7QUFDQSxVQUFJLGFBQWEsQ0FBQyxRQUFkLEdBQXlCLEtBQUssVUFBTCxDQUFnQixRQUE3QyxFQUF1RDtBQUNuRCxRQUFBLGFBQWEsQ0FBQyxTQUFkLEdBQTBCLE1BQTFCO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsUUFBQSxhQUFhLENBQUMsU0FBZCxHQUEwQixJQUExQjtBQUNILE9BVGlCLENBV2xCOzs7QUFDQSxVQUFJLEtBQUssVUFBTCxDQUFnQixTQUFoQixJQUE2QixhQUFhLENBQUMsU0FBL0MsRUFBMEQ7QUFDdEQsUUFBQSxhQUFhLENBQUMsa0JBQWQsR0FBbUMsSUFBbkMsQ0FEc0QsQ0FHdEQ7O0FBQ0EsUUFBQSxNQUFNLENBQUMsYUFBUCxDQUFxQixJQUFJLFdBQUosQ0FBZ0IsMkJBQWhCLEVBQTZDO0FBQUUsVUFBQSxNQUFNLEVBQUU7QUFBVixTQUE3QyxDQUFyQjtBQUNILE9BakJpQixDQW1CbEI7OztBQUNBLE1BQUEsTUFBTSxDQUFDLGFBQVAsQ0FBcUIsSUFBSSxXQUFKLENBQWdCLHdCQUFoQixFQUEwQztBQUFFLFFBQUEsTUFBTSxFQUFFO0FBQVYsT0FBMUMsQ0FBckIsRUFwQmtCLENBc0JsQjs7QUFDQSxXQUFLLFVBQUwsR0FBa0IsYUFBbEI7QUFDSDs7Ozs7Ozs7O0FDcENMOzs7QUFJQSxJQUFJLEVBQUUsR0FBRyxNQUFNLENBQUMsV0FBUCxHQUFxQixJQUE5QixDLENBQ0E7O0FBQ0EsUUFBUSxDQUFDLGVBQVQsQ0FBeUIsS0FBekIsQ0FBK0IsV0FBL0IsQ0FBMkMsTUFBM0MsWUFBc0QsRUFBdEQsUyxDQUVBOztBQUNBLE1BQU0sQ0FBQyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxZQUFNO0FBQ3BDO0FBQ0EsTUFBSSxFQUFFLEdBQUcsTUFBTSxDQUFDLFdBQVAsR0FBcUIsSUFBOUI7QUFDQSxFQUFBLFFBQVEsQ0FBQyxlQUFULENBQXlCLEtBQXpCLENBQStCLFdBQS9CLENBQTJDLE1BQTNDLFlBQXNELEVBQXREO0FBQ0gsQ0FKRCIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsIi8qXG5EZXRhaWxzIEVsZW1lbnQgUG9seWZpbGwgMi40LjBcbkNvcHlyaWdodCDCqSAyMDE5IEphdmFuIE1ha2htYWxpXG4gKi9cbihmdW5jdGlvbigpIHtcbiAgXCJ1c2Ugc3RyaWN0XCI7XG4gIHZhciBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRldGFpbHNcIik7XG4gIHZhciBlbGVtZW50SXNOYXRpdmUgPSB0eXBlb2YgSFRNTERldGFpbHNFbGVtZW50ICE9IFwidW5kZWZpbmVkXCIgJiYgZWxlbWVudCBpbnN0YW5jZW9mIEhUTUxEZXRhaWxzRWxlbWVudDtcbiAgdmFyIHN1cHBvcnQgPSB7XG4gICAgb3BlbjogXCJvcGVuXCIgaW4gZWxlbWVudCB8fCBlbGVtZW50SXNOYXRpdmUsXG4gICAgdG9nZ2xlOiBcIm9udG9nZ2xlXCIgaW4gZWxlbWVudFxuICB9O1xuICB2YXIgc3R5bGVzID0gJ1xcbmRldGFpbHMsIHN1bW1hcnkge1xcbiAgZGlzcGxheTogYmxvY2s7XFxufVxcbmRldGFpbHM6bm90KFtvcGVuXSkgPiAqOm5vdChzdW1tYXJ5KSB7XFxuICBkaXNwbGF5OiBub25lO1xcbn1cXG5zdW1tYXJ5OjpiZWZvcmUge1xcbiAgY29udGVudDogXCLilrpcIjtcXG4gIHBhZGRpbmctcmlnaHQ6IDAuM3JlbTtcXG4gIGZvbnQtc2l6ZTogMC42cmVtO1xcbiAgY3Vyc29yOiBkZWZhdWx0O1xcbn1cXG5bb3Blbl0gPiBzdW1tYXJ5OjpiZWZvcmUge1xcbiAgY29udGVudDogXCLilrxcIjtcXG59XFxuJztcbiAgdmFyIF9yZWYgPSBbXSwgZm9yRWFjaCA9IF9yZWYuZm9yRWFjaCwgc2xpY2UgPSBfcmVmLnNsaWNlO1xuICBpZiAoIXN1cHBvcnQub3Blbikge1xuICAgIHBvbHlmaWxsU3R5bGVzKCk7XG4gICAgcG9seWZpbGxQcm9wZXJ0aWVzKCk7XG4gICAgcG9seWZpbGxUb2dnbGUoKTtcbiAgICBwb2x5ZmlsbEFjY2Vzc2liaWxpdHkoKTtcbiAgfVxuICBpZiAoc3VwcG9ydC5vcGVuICYmICFzdXBwb3J0LnRvZ2dsZSkge1xuICAgIHBvbHlmaWxsVG9nZ2xlRXZlbnQoKTtcbiAgfVxuICBmdW5jdGlvbiBwb2x5ZmlsbFN0eWxlcygpIHtcbiAgICBkb2N1bWVudC5oZWFkLmluc2VydEFkamFjZW50SFRNTChcImFmdGVyYmVnaW5cIiwgXCI8c3R5bGU+XCIgKyBzdHlsZXMgKyBcIjwvc3R5bGU+XCIpO1xuICB9XG4gIGZ1bmN0aW9uIHBvbHlmaWxsUHJvcGVydGllcygpIHtcbiAgICB2YXIgcHJvdG90eXBlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRldGFpbHNcIikuY29uc3RydWN0b3IucHJvdG90eXBlO1xuICAgIHZhciBzZXRBdHRyaWJ1dGUgPSBwcm90b3R5cGUuc2V0QXR0cmlidXRlLCByZW1vdmVBdHRyaWJ1dGUgPSBwcm90b3R5cGUucmVtb3ZlQXR0cmlidXRlO1xuICAgIHZhciBvcGVuID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihwcm90b3R5cGUsIFwib3BlblwiKTtcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydGllcyhwcm90b3R5cGUsIHtcbiAgICAgIG9wZW46IHtcbiAgICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgICAgaWYgKHRoaXMudGFnTmFtZSA9PSBcIkRFVEFJTFNcIikge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuaGFzQXR0cmlidXRlKFwib3BlblwiKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKG9wZW4gJiYgb3Blbi5nZXQpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIG9wZW4uZ2V0LmNhbGwodGhpcyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBzZXQ6IGZ1bmN0aW9uIHNldCh2YWx1ZSkge1xuICAgICAgICAgIGlmICh0aGlzLnRhZ05hbWUgPT0gXCJERVRBSUxTXCIpIHtcbiAgICAgICAgICAgIHJldHVybiB2YWx1ZSA/IHRoaXMuc2V0QXR0cmlidXRlKFwib3BlblwiLCBcIlwiKSA6IHRoaXMucmVtb3ZlQXR0cmlidXRlKFwib3BlblwiKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKG9wZW4gJiYgb3Blbi5zZXQpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIG9wZW4uc2V0LmNhbGwodGhpcywgdmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHNldEF0dHJpYnV0ZToge1xuICAgICAgICB2YWx1ZTogZnVuY3Rpb24gdmFsdWUobmFtZSwgX3ZhbHVlKSB7XG4gICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgICB2YXIgY2FsbCA9IGZ1bmN0aW9uIGNhbGwoKSB7XG4gICAgICAgICAgICByZXR1cm4gc2V0QXR0cmlidXRlLmNhbGwoX3RoaXMsIG5hbWUsIF92YWx1ZSk7XG4gICAgICAgICAgfTtcbiAgICAgICAgICBpZiAobmFtZSA9PSBcIm9wZW5cIiAmJiB0aGlzLnRhZ05hbWUgPT0gXCJERVRBSUxTXCIpIHtcbiAgICAgICAgICAgIHZhciB3YXNPcGVuID0gdGhpcy5oYXNBdHRyaWJ1dGUoXCJvcGVuXCIpO1xuICAgICAgICAgICAgdmFyIHJlc3VsdCA9IGNhbGwoKTtcbiAgICAgICAgICAgIGlmICghd2FzT3Blbikge1xuICAgICAgICAgICAgICB2YXIgc3VtbWFyeSA9IHRoaXMucXVlcnlTZWxlY3RvcihcInN1bW1hcnlcIik7XG4gICAgICAgICAgICAgIGlmIChzdW1tYXJ5KSBzdW1tYXJ5LnNldEF0dHJpYnV0ZShcImFyaWEtZXhwYW5kZWRcIiwgdHJ1ZSk7XG4gICAgICAgICAgICAgIHRyaWdnZXJUb2dnbGUodGhpcyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gY2FsbCgpO1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgcmVtb3ZlQXR0cmlidXRlOiB7XG4gICAgICAgIHZhbHVlOiBmdW5jdGlvbiB2YWx1ZShuYW1lKSB7XG4gICAgICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG4gICAgICAgICAgdmFyIGNhbGwgPSBmdW5jdGlvbiBjYWxsKCkge1xuICAgICAgICAgICAgcmV0dXJuIHJlbW92ZUF0dHJpYnV0ZS5jYWxsKF90aGlzMiwgbmFtZSk7XG4gICAgICAgICAgfTtcbiAgICAgICAgICBpZiAobmFtZSA9PSBcIm9wZW5cIiAmJiB0aGlzLnRhZ05hbWUgPT0gXCJERVRBSUxTXCIpIHtcbiAgICAgICAgICAgIHZhciB3YXNPcGVuID0gdGhpcy5oYXNBdHRyaWJ1dGUoXCJvcGVuXCIpO1xuICAgICAgICAgICAgdmFyIHJlc3VsdCA9IGNhbGwoKTtcbiAgICAgICAgICAgIGlmICh3YXNPcGVuKSB7XG4gICAgICAgICAgICAgIHZhciBzdW1tYXJ5ID0gdGhpcy5xdWVyeVNlbGVjdG9yKFwic3VtbWFyeVwiKTtcbiAgICAgICAgICAgICAgaWYgKHN1bW1hcnkpIHN1bW1hcnkuc2V0QXR0cmlidXRlKFwiYXJpYS1leHBhbmRlZFwiLCBmYWxzZSk7XG4gICAgICAgICAgICAgIHRyaWdnZXJUb2dnbGUodGhpcyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gY2FsbCgpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG4gIH1cbiAgZnVuY3Rpb24gcG9seWZpbGxUb2dnbGUoKSB7XG4gICAgb25Ub2dnbGluZ1RyaWdnZXIoZnVuY3Rpb24oZWxlbWVudCkge1xuICAgICAgZWxlbWVudC5oYXNBdHRyaWJ1dGUoXCJvcGVuXCIpID8gZWxlbWVudC5yZW1vdmVBdHRyaWJ1dGUoXCJvcGVuXCIpIDogZWxlbWVudC5zZXRBdHRyaWJ1dGUoXCJvcGVuXCIsIFwiXCIpO1xuICAgIH0pO1xuICB9XG4gIGZ1bmN0aW9uIHBvbHlmaWxsVG9nZ2xlRXZlbnQoKSB7XG4gICAgaWYgKHdpbmRvdy5NdXRhdGlvbk9ic2VydmVyKSB7XG4gICAgICBuZXcgTXV0YXRpb25PYnNlcnZlcihmdW5jdGlvbihtdXRhdGlvbnMpIHtcbiAgICAgICAgZm9yRWFjaC5jYWxsKG11dGF0aW9ucywgZnVuY3Rpb24obXV0YXRpb24pIHtcbiAgICAgICAgICB2YXIgdGFyZ2V0ID0gbXV0YXRpb24udGFyZ2V0LCBhdHRyaWJ1dGVOYW1lID0gbXV0YXRpb24uYXR0cmlidXRlTmFtZTtcbiAgICAgICAgICBpZiAodGFyZ2V0LnRhZ05hbWUgPT0gXCJERVRBSUxTXCIgJiYgYXR0cmlidXRlTmFtZSA9PSBcIm9wZW5cIikge1xuICAgICAgICAgICAgdHJpZ2dlclRvZ2dsZSh0YXJnZXQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9KS5vYnNlcnZlKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCwge1xuICAgICAgICBhdHRyaWJ1dGVzOiB0cnVlLFxuICAgICAgICBzdWJ0cmVlOiB0cnVlXG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgb25Ub2dnbGluZ1RyaWdnZXIoZnVuY3Rpb24oZWxlbWVudCkge1xuICAgICAgICB2YXIgd2FzT3BlbiA9IGVsZW1lbnQuZ2V0QXR0cmlidXRlKFwib3BlblwiKTtcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICB2YXIgaXNPcGVuID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJvcGVuXCIpO1xuICAgICAgICAgIGlmICh3YXNPcGVuICE9IGlzT3Blbikge1xuICAgICAgICAgICAgdHJpZ2dlclRvZ2dsZShlbGVtZW50KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sIDEpO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG4gIGZ1bmN0aW9uIHBvbHlmaWxsQWNjZXNzaWJpbGl0eSgpIHtcbiAgICBzZXRBY2Nlc3NpYmlsaXR5QXR0cmlidXRlcyhkb2N1bWVudCk7XG4gICAgaWYgKHdpbmRvdy5NdXRhdGlvbk9ic2VydmVyKSB7XG4gICAgICBuZXcgTXV0YXRpb25PYnNlcnZlcihmdW5jdGlvbihtdXRhdGlvbnMpIHtcbiAgICAgICAgZm9yRWFjaC5jYWxsKG11dGF0aW9ucywgZnVuY3Rpb24obXV0YXRpb24pIHtcbiAgICAgICAgICBmb3JFYWNoLmNhbGwobXV0YXRpb24uYWRkZWROb2Rlcywgc2V0QWNjZXNzaWJpbGl0eUF0dHJpYnV0ZXMpO1xuICAgICAgICB9KTtcbiAgICAgIH0pLm9ic2VydmUoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LCB7XG4gICAgICAgIHN1YnRyZWU6IHRydWUsXG4gICAgICAgIGNoaWxkTGlzdDogdHJ1ZVxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJET01Ob2RlSW5zZXJ0ZWRcIiwgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgc2V0QWNjZXNzaWJpbGl0eUF0dHJpYnV0ZXMoZXZlbnQudGFyZ2V0KTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuICBmdW5jdGlvbiBzZXRBY2Nlc3NpYmlsaXR5QXR0cmlidXRlcyhyb290KSB7XG4gICAgZmluZEVsZW1lbnRzV2l0aFRhZ05hbWUocm9vdCwgXCJTVU1NQVJZXCIpLmZvckVhY2goZnVuY3Rpb24oc3VtbWFyeSkge1xuICAgICAgdmFyIGRldGFpbHMgPSBmaW5kQ2xvc2VzdEVsZW1lbnRXaXRoVGFnTmFtZShzdW1tYXJ5LCBcIkRFVEFJTFNcIik7XG4gICAgICBzdW1tYXJ5LnNldEF0dHJpYnV0ZShcImFyaWEtZXhwYW5kZWRcIiwgZGV0YWlscy5oYXNBdHRyaWJ1dGUoXCJvcGVuXCIpKTtcbiAgICAgIGlmICghc3VtbWFyeS5oYXNBdHRyaWJ1dGUoXCJ0YWJpbmRleFwiKSkgc3VtbWFyeS5zZXRBdHRyaWJ1dGUoXCJ0YWJpbmRleFwiLCBcIjBcIik7XG4gICAgICBpZiAoIXN1bW1hcnkuaGFzQXR0cmlidXRlKFwicm9sZVwiKSkgc3VtbWFyeS5zZXRBdHRyaWJ1dGUoXCJyb2xlXCIsIFwiYnV0dG9uXCIpO1xuICAgIH0pO1xuICB9XG4gIGZ1bmN0aW9uIGV2ZW50SXNTaWduaWZpY2FudChldmVudCkge1xuICAgIHJldHVybiAhKGV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQgfHwgZXZlbnQuY3RybEtleSB8fCBldmVudC5tZXRhS2V5IHx8IGV2ZW50LnNoaWZ0S2V5IHx8IGV2ZW50LnRhcmdldC5pc0NvbnRlbnRFZGl0YWJsZSk7XG4gIH1cbiAgZnVuY3Rpb24gb25Ub2dnbGluZ1RyaWdnZXIoY2FsbGJhY2spIHtcbiAgICBhZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgIGlmIChldmVudElzU2lnbmlmaWNhbnQoZXZlbnQpKSB7XG4gICAgICAgIGlmIChldmVudC53aGljaCA8PSAxKSB7XG4gICAgICAgICAgdmFyIGVsZW1lbnQgPSBmaW5kQ2xvc2VzdEVsZW1lbnRXaXRoVGFnTmFtZShldmVudC50YXJnZXQsIFwiU1VNTUFSWVwiKTtcbiAgICAgICAgICBpZiAoZWxlbWVudCAmJiBlbGVtZW50LnBhcmVudE5vZGUgJiYgZWxlbWVudC5wYXJlbnROb2RlLnRhZ05hbWUgPT0gXCJERVRBSUxTXCIpIHtcbiAgICAgICAgICAgIGNhbGxiYWNrKGVsZW1lbnQucGFyZW50Tm9kZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgZmFsc2UpO1xuICAgIGFkZEV2ZW50TGlzdGVuZXIoXCJrZXlkb3duXCIsIGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICBpZiAoZXZlbnRJc1NpZ25pZmljYW50KGV2ZW50KSkge1xuICAgICAgICBpZiAoZXZlbnQua2V5Q29kZSA9PSAxMyB8fCBldmVudC5rZXlDb2RlID09IDMyKSB7XG4gICAgICAgICAgdmFyIGVsZW1lbnQgPSBmaW5kQ2xvc2VzdEVsZW1lbnRXaXRoVGFnTmFtZShldmVudC50YXJnZXQsIFwiU1VNTUFSWVwiKTtcbiAgICAgICAgICBpZiAoZWxlbWVudCAmJiBlbGVtZW50LnBhcmVudE5vZGUgJiYgZWxlbWVudC5wYXJlbnROb2RlLnRhZ05hbWUgPT0gXCJERVRBSUxTXCIpIHtcbiAgICAgICAgICAgIGNhbGxiYWNrKGVsZW1lbnQucGFyZW50Tm9kZSk7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sIGZhbHNlKTtcbiAgfVxuICBmdW5jdGlvbiB0cmlnZ2VyVG9nZ2xlKGVsZW1lbnQpIHtcbiAgICB2YXIgZXZlbnQgPSBkb2N1bWVudC5jcmVhdGVFdmVudChcIkV2ZW50XCIpO1xuICAgIGV2ZW50LmluaXRFdmVudChcInRvZ2dsZVwiLCBmYWxzZSwgZmFsc2UpO1xuICAgIGVsZW1lbnQuZGlzcGF0Y2hFdmVudChldmVudCk7XG4gIH1cbiAgZnVuY3Rpb24gZmluZEVsZW1lbnRzV2l0aFRhZ05hbWUocm9vdCwgdGFnTmFtZSkge1xuICAgIHJldHVybiAocm9vdC50YWdOYW1lID09IHRhZ05hbWUgPyBbIHJvb3QgXSA6IFtdKS5jb25jYXQodHlwZW9mIHJvb3QuZ2V0RWxlbWVudHNCeVRhZ05hbWUgPT0gXCJmdW5jdGlvblwiID8gc2xpY2UuY2FsbChyb290LmdldEVsZW1lbnRzQnlUYWdOYW1lKHRhZ05hbWUpKSA6IFtdKTtcbiAgfVxuICBmdW5jdGlvbiBmaW5kQ2xvc2VzdEVsZW1lbnRXaXRoVGFnTmFtZShlbGVtZW50LCB0YWdOYW1lKSB7XG4gICAgaWYgKHR5cGVvZiBlbGVtZW50LmNsb3Nlc3QgPT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICByZXR1cm4gZWxlbWVudC5jbG9zZXN0KHRhZ05hbWUpO1xuICAgIH0gZWxzZSB7XG4gICAgICB3aGlsZSAoZWxlbWVudCkge1xuICAgICAgICBpZiAoZWxlbWVudC50YWdOYW1lID09IHRhZ05hbWUpIHtcbiAgICAgICAgICByZXR1cm4gZWxlbWVudDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBlbGVtZW50ID0gZWxlbWVudC5wYXJlbnROb2RlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59KSgpO1xuIiwiLyohIEBsaWNlbnNlIFNjcm9sbFJldmVhbCB2NC4wLjZcblxuXHRDb3B5cmlnaHQgMjAyMCBGaXNzc2lvbiBMTEMuXG5cblx0TGljZW5zZWQgdW5kZXIgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIDMuMCBmb3Jcblx0Y29tcGF0aWJsZSBvcGVuIHNvdXJjZSBwcm9qZWN0cyBhbmQgbm9uLWNvbW1lcmNpYWwgdXNlLlxuXG5cdEZvciBjb21tZXJjaWFsIHNpdGVzLCB0aGVtZXMsIHByb2plY3RzLCBhbmQgYXBwbGljYXRpb25zLFxuXHRrZWVwIHlvdXIgc291cmNlIGNvZGUgcHJpdmF0ZS9wcm9wcmlldGFyeSBieSBwdXJjaGFzaW5nXG5cdGEgY29tbWVyY2lhbCBsaWNlbnNlIGZyb20gaHR0cHM6Ly9zY3JvbGxyZXZlYWxqcy5vcmcvXG4qL1xuKGZ1bmN0aW9uIChnbG9iYWwsIGZhY3RvcnkpIHtcblx0dHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnID8gbW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KCkgOlxuXHR0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQgPyBkZWZpbmUoZmFjdG9yeSkgOlxuXHQoZ2xvYmFsID0gZ2xvYmFsIHx8IHNlbGYsIGdsb2JhbC5TY3JvbGxSZXZlYWwgPSBmYWN0b3J5KCkpO1xufSh0aGlzLCBmdW5jdGlvbiAoKSB7ICd1c2Ugc3RyaWN0JztcblxuXHR2YXIgZGVmYXVsdHMgPSB7XG5cdFx0ZGVsYXk6IDAsXG5cdFx0ZGlzdGFuY2U6ICcwJyxcblx0XHRkdXJhdGlvbjogNjAwLFxuXHRcdGVhc2luZzogJ2N1YmljLWJlemllcigwLjUsIDAsIDAsIDEpJyxcblx0XHRpbnRlcnZhbDogMCxcblx0XHRvcGFjaXR5OiAwLFxuXHRcdG9yaWdpbjogJ2JvdHRvbScsXG5cdFx0cm90YXRlOiB7XG5cdFx0XHR4OiAwLFxuXHRcdFx0eTogMCxcblx0XHRcdHo6IDBcblx0XHR9LFxuXHRcdHNjYWxlOiAxLFxuXHRcdGNsZWFudXA6IGZhbHNlLFxuXHRcdGNvbnRhaW5lcjogZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LFxuXHRcdGRlc2t0b3A6IHRydWUsXG5cdFx0bW9iaWxlOiB0cnVlLFxuXHRcdHJlc2V0OiBmYWxzZSxcblx0XHR1c2VEZWxheTogJ2Fsd2F5cycsXG5cdFx0dmlld0ZhY3RvcjogMC4wLFxuXHRcdHZpZXdPZmZzZXQ6IHtcblx0XHRcdHRvcDogMCxcblx0XHRcdHJpZ2h0OiAwLFxuXHRcdFx0Ym90dG9tOiAwLFxuXHRcdFx0bGVmdDogMFxuXHRcdH0sXG5cdFx0YWZ0ZXJSZXNldDogZnVuY3Rpb24gYWZ0ZXJSZXNldCgpIHt9LFxuXHRcdGFmdGVyUmV2ZWFsOiBmdW5jdGlvbiBhZnRlclJldmVhbCgpIHt9LFxuXHRcdGJlZm9yZVJlc2V0OiBmdW5jdGlvbiBiZWZvcmVSZXNldCgpIHt9LFxuXHRcdGJlZm9yZVJldmVhbDogZnVuY3Rpb24gYmVmb3JlUmV2ZWFsKCkge31cblx0fTtcblxuXHRmdW5jdGlvbiBmYWlsdXJlKCkge1xuXHRcdGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKCdzcicpO1xuXG5cdFx0cmV0dXJuIHtcblx0XHRcdGNsZWFuOiBmdW5jdGlvbiBjbGVhbigpIHt9LFxuXHRcdFx0ZGVzdHJveTogZnVuY3Rpb24gZGVzdHJveSgpIHt9LFxuXHRcdFx0cmV2ZWFsOiBmdW5jdGlvbiByZXZlYWwoKSB7fSxcblx0XHRcdHN5bmM6IGZ1bmN0aW9uIHN5bmMoKSB7fSxcblx0XHRcdGdldCBub29wKCkge1xuXHRcdFx0XHRyZXR1cm4gdHJ1ZVxuXHRcdFx0fVxuXHRcdH1cblx0fVxuXG5cdGZ1bmN0aW9uIHN1Y2Nlc3MoKSB7XG5cdFx0ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5hZGQoJ3NyJyk7XG5cblx0XHRpZiAoZG9jdW1lbnQuYm9keSkge1xuXHRcdFx0ZG9jdW1lbnQuYm9keS5zdHlsZS5oZWlnaHQgPSAnMTAwJSc7XG5cdFx0fSBlbHNlIHtcblx0XHRcdGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdGRvY3VtZW50LmJvZHkuc3R5bGUuaGVpZ2h0ID0gJzEwMCUnO1xuXHRcdFx0fSk7XG5cdFx0fVxuXHR9XG5cblx0dmFyIG1vdW50ID0geyBzdWNjZXNzOiBzdWNjZXNzLCBmYWlsdXJlOiBmYWlsdXJlIH07XG5cblx0LyohIEBsaWNlbnNlIGlzLWRvbS1ub2RlIHYxLjAuNFxuXG5cdFx0Q29weXJpZ2h0IDIwMTggRmlzc3Npb24gTExDLlxuXG5cdFx0UGVybWlzc2lvbiBpcyBoZXJlYnkgZ3JhbnRlZCwgZnJlZSBvZiBjaGFyZ2UsIHRvIGFueSBwZXJzb24gb2J0YWluaW5nIGEgY29weVxuXHRcdG9mIHRoaXMgc29mdHdhcmUgYW5kIGFzc29jaWF0ZWQgZG9jdW1lbnRhdGlvbiBmaWxlcyAodGhlIFwiU29mdHdhcmVcIiksIHRvIGRlYWxcblx0XHRpbiB0aGUgU29mdHdhcmUgd2l0aG91dCByZXN0cmljdGlvbiwgaW5jbHVkaW5nIHdpdGhvdXQgbGltaXRhdGlvbiB0aGUgcmlnaHRzXG5cdFx0dG8gdXNlLCBjb3B5LCBtb2RpZnksIG1lcmdlLCBwdWJsaXNoLCBkaXN0cmlidXRlLCBzdWJsaWNlbnNlLCBhbmQvb3Igc2VsbFxuXHRcdGNvcGllcyBvZiB0aGUgU29mdHdhcmUsIGFuZCB0byBwZXJtaXQgcGVyc29ucyB0byB3aG9tIHRoZSBTb2Z0d2FyZSBpc1xuXHRcdGZ1cm5pc2hlZCB0byBkbyBzbywgc3ViamVjdCB0byB0aGUgZm9sbG93aW5nIGNvbmRpdGlvbnM6XG5cblx0XHRUaGUgYWJvdmUgY29weXJpZ2h0IG5vdGljZSBhbmQgdGhpcyBwZXJtaXNzaW9uIG5vdGljZSBzaGFsbCBiZSBpbmNsdWRlZCBpbiBhbGxcblx0XHRjb3BpZXMgb3Igc3Vic3RhbnRpYWwgcG9ydGlvbnMgb2YgdGhlIFNvZnR3YXJlLlxuXG5cdFx0VEhFIFNPRlRXQVJFIElTIFBST1ZJREVEIFwiQVMgSVNcIiwgV0lUSE9VVCBXQVJSQU5UWSBPRiBBTlkgS0lORCwgRVhQUkVTUyBPUlxuXHRcdElNUExJRUQsIElOQ0xVRElORyBCVVQgTk9UIExJTUlURUQgVE8gVEhFIFdBUlJBTlRJRVMgT0YgTUVSQ0hBTlRBQklMSVRZLFxuXHRcdEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFIEFORCBOT05JTkZSSU5HRU1FTlQuIElOIE5PIEVWRU5UIFNIQUxMIFRIRVxuXHRcdEFVVEhPUlMgT1IgQ09QWVJJR0hUIEhPTERFUlMgQkUgTElBQkxFIEZPUiBBTlkgQ0xBSU0sIERBTUFHRVMgT1IgT1RIRVJcblx0XHRMSUFCSUxJVFksIFdIRVRIRVIgSU4gQU4gQUNUSU9OIE9GIENPTlRSQUNULCBUT1JUIE9SIE9USEVSV0lTRSwgQVJJU0lORyBGUk9NLFxuXHRcdE9VVCBPRiBPUiBJTiBDT05ORUNUSU9OIFdJVEggVEhFIFNPRlRXQVJFIE9SIFRIRSBVU0UgT1IgT1RIRVIgREVBTElOR1MgSU4gVEhFXG5cdFx0U09GVFdBUkUuXG5cblx0Ki9cblx0ZnVuY3Rpb24gaXNEb21Ob2RlKHgpIHtcblx0XHRyZXR1cm4gdHlwZW9mIHdpbmRvdy5Ob2RlID09PSAnb2JqZWN0J1xuXHRcdFx0PyB4IGluc3RhbmNlb2Ygd2luZG93Lk5vZGVcblx0XHRcdDogeCAhPT0gbnVsbCAmJlxuXHRcdFx0XHRcdHR5cGVvZiB4ID09PSAnb2JqZWN0JyAmJlxuXHRcdFx0XHRcdHR5cGVvZiB4Lm5vZGVUeXBlID09PSAnbnVtYmVyJyAmJlxuXHRcdFx0XHRcdHR5cGVvZiB4Lm5vZGVOYW1lID09PSAnc3RyaW5nJ1xuXHR9XG5cblx0LyohIEBsaWNlbnNlIGlzLWRvbS1ub2RlLWxpc3QgdjEuMi4xXG5cblx0XHRDb3B5cmlnaHQgMjAxOCBGaXNzc2lvbiBMTEMuXG5cblx0XHRQZXJtaXNzaW9uIGlzIGhlcmVieSBncmFudGVkLCBmcmVlIG9mIGNoYXJnZSwgdG8gYW55IHBlcnNvbiBvYnRhaW5pbmcgYSBjb3B5XG5cdFx0b2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGUgXCJTb2Z0d2FyZVwiKSwgdG8gZGVhbFxuXHRcdGluIHRoZSBTb2Z0d2FyZSB3aXRob3V0IHJlc3RyaWN0aW9uLCBpbmNsdWRpbmcgd2l0aG91dCBsaW1pdGF0aW9uIHRoZSByaWdodHNcblx0XHR0byB1c2UsIGNvcHksIG1vZGlmeSwgbWVyZ2UsIHB1Ymxpc2gsIGRpc3RyaWJ1dGUsIHN1YmxpY2Vuc2UsIGFuZC9vciBzZWxsXG5cdFx0Y29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdCBwZXJzb25zIHRvIHdob20gdGhlIFNvZnR3YXJlIGlzXG5cdFx0ZnVybmlzaGVkIHRvIGRvIHNvLCBzdWJqZWN0IHRvIHRoZSBmb2xsb3dpbmcgY29uZGl0aW9uczpcblxuXHRcdFRoZSBhYm92ZSBjb3B5cmlnaHQgbm90aWNlIGFuZCB0aGlzIHBlcm1pc3Npb24gbm90aWNlIHNoYWxsIGJlIGluY2x1ZGVkIGluIGFsbFxuXHRcdGNvcGllcyBvciBzdWJzdGFudGlhbCBwb3J0aW9ucyBvZiB0aGUgU29mdHdhcmUuXG5cblx0XHRUSEUgU09GVFdBUkUgSVMgUFJPVklERUQgXCJBUyBJU1wiLCBXSVRIT1VUIFdBUlJBTlRZIE9GIEFOWSBLSU5ELCBFWFBSRVNTIE9SXG5cdFx0SU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRiBNRVJDSEFOVEFCSUxJVFksXG5cdFx0RklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UgQU5EIE5PTklORlJJTkdFTUVOVC4gSU4gTk8gRVZFTlQgU0hBTEwgVEhFXG5cdFx0QVVUSE9SUyBPUiBDT1BZUklHSFQgSE9MREVSUyBCRSBMSUFCTEUgRk9SIEFOWSBDTEFJTSwgREFNQUdFUyBPUiBPVEhFUlxuXHRcdExJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1IgT1RIRVJXSVNFLCBBUklTSU5HIEZST00sXG5cdFx0T1VUIE9GIE9SIElOIENPTk5FQ1RJT04gV0lUSCBUSEUgU09GVFdBUkUgT1IgVEhFIFVTRSBPUiBPVEhFUiBERUFMSU5HUyBJTiBUSEVcblx0XHRTT0ZUV0FSRS5cblxuXHQqL1xuXG5cdGZ1bmN0aW9uIGlzRG9tTm9kZUxpc3QoeCkge1xuXHRcdHZhciBwcm90b3R5cGVUb1N0cmluZyA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbCh4KTtcblx0XHR2YXIgcmVnZXggPSAvXlxcW29iamVjdCAoSFRNTENvbGxlY3Rpb258Tm9kZUxpc3R8T2JqZWN0KVxcXSQvO1xuXG5cdFx0cmV0dXJuIHR5cGVvZiB3aW5kb3cuTm9kZUxpc3QgPT09ICdvYmplY3QnXG5cdFx0XHQ/IHggaW5zdGFuY2VvZiB3aW5kb3cuTm9kZUxpc3Rcblx0XHRcdDogeCAhPT0gbnVsbCAmJlxuXHRcdFx0XHRcdHR5cGVvZiB4ID09PSAnb2JqZWN0JyAmJlxuXHRcdFx0XHRcdHR5cGVvZiB4Lmxlbmd0aCA9PT0gJ251bWJlcicgJiZcblx0XHRcdFx0XHRyZWdleC50ZXN0KHByb3RvdHlwZVRvU3RyaW5nKSAmJlxuXHRcdFx0XHRcdCh4Lmxlbmd0aCA9PT0gMCB8fCBpc0RvbU5vZGUoeFswXSkpXG5cdH1cblxuXHQvKiEgQGxpY2Vuc2UgVGVhbGlnaHQgdjAuMy42XG5cblx0XHRDb3B5cmlnaHQgMjAxOCBGaXNzc2lvbiBMTEMuXG5cblx0XHRQZXJtaXNzaW9uIGlzIGhlcmVieSBncmFudGVkLCBmcmVlIG9mIGNoYXJnZSwgdG8gYW55IHBlcnNvbiBvYnRhaW5pbmcgYSBjb3B5XG5cdFx0b2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGUgXCJTb2Z0d2FyZVwiKSwgdG8gZGVhbFxuXHRcdGluIHRoZSBTb2Z0d2FyZSB3aXRob3V0IHJlc3RyaWN0aW9uLCBpbmNsdWRpbmcgd2l0aG91dCBsaW1pdGF0aW9uIHRoZSByaWdodHNcblx0XHR0byB1c2UsIGNvcHksIG1vZGlmeSwgbWVyZ2UsIHB1Ymxpc2gsIGRpc3RyaWJ1dGUsIHN1YmxpY2Vuc2UsIGFuZC9vciBzZWxsXG5cdFx0Y29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdCBwZXJzb25zIHRvIHdob20gdGhlIFNvZnR3YXJlIGlzXG5cdFx0ZnVybmlzaGVkIHRvIGRvIHNvLCBzdWJqZWN0IHRvIHRoZSBmb2xsb3dpbmcgY29uZGl0aW9uczpcblxuXHRcdFRoZSBhYm92ZSBjb3B5cmlnaHQgbm90aWNlIGFuZCB0aGlzIHBlcm1pc3Npb24gbm90aWNlIHNoYWxsIGJlIGluY2x1ZGVkIGluIGFsbFxuXHRcdGNvcGllcyBvciBzdWJzdGFudGlhbCBwb3J0aW9ucyBvZiB0aGUgU29mdHdhcmUuXG5cblx0XHRUSEUgU09GVFdBUkUgSVMgUFJPVklERUQgXCJBUyBJU1wiLCBXSVRIT1VUIFdBUlJBTlRZIE9GIEFOWSBLSU5ELCBFWFBSRVNTIE9SXG5cdFx0SU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRiBNRVJDSEFOVEFCSUxJVFksXG5cdFx0RklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UgQU5EIE5PTklORlJJTkdFTUVOVC4gSU4gTk8gRVZFTlQgU0hBTEwgVEhFXG5cdFx0QVVUSE9SUyBPUiBDT1BZUklHSFQgSE9MREVSUyBCRSBMSUFCTEUgRk9SIEFOWSBDTEFJTSwgREFNQUdFUyBPUiBPVEhFUlxuXHRcdExJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1IgT1RIRVJXSVNFLCBBUklTSU5HIEZST00sXG5cdFx0T1VUIE9GIE9SIElOIENPTk5FQ1RJT04gV0lUSCBUSEUgU09GVFdBUkUgT1IgVEhFIFVTRSBPUiBPVEhFUiBERUFMSU5HUyBJTiBUSEVcblx0XHRTT0ZUV0FSRS5cblxuXHQqL1xuXG5cdGZ1bmN0aW9uIHRlYWxpZ2h0KHRhcmdldCwgY29udGV4dCkge1xuXHQgIGlmICggY29udGV4dCA9PT0gdm9pZCAwICkgeyBjb250ZXh0ID0gZG9jdW1lbnQ7IH1cblxuXHQgIGlmICh0YXJnZXQgaW5zdGFuY2VvZiBBcnJheSkgeyByZXR1cm4gdGFyZ2V0LmZpbHRlcihpc0RvbU5vZGUpOyB9XG5cdCAgaWYgKGlzRG9tTm9kZSh0YXJnZXQpKSB7IHJldHVybiBbdGFyZ2V0XTsgfVxuXHQgIGlmIChpc0RvbU5vZGVMaXN0KHRhcmdldCkpIHsgcmV0dXJuIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKHRhcmdldCk7IH1cblx0ICBpZiAodHlwZW9mIHRhcmdldCA9PT0gXCJzdHJpbmdcIikge1xuXHQgICAgdHJ5IHtcblx0ICAgICAgdmFyIHF1ZXJ5ID0gY29udGV4dC5xdWVyeVNlbGVjdG9yQWxsKHRhcmdldCk7XG5cdCAgICAgIHJldHVybiBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChxdWVyeSk7XG5cdCAgICB9IGNhdGNoIChlcnIpIHtcblx0ICAgICAgcmV0dXJuIFtdO1xuXHQgICAgfVxuXHQgIH1cblx0ICByZXR1cm4gW107XG5cdH1cblxuXHRmdW5jdGlvbiBpc09iamVjdCh4KSB7XG5cdFx0cmV0dXJuIChcblx0XHRcdHggIT09IG51bGwgJiZcblx0XHRcdHggaW5zdGFuY2VvZiBPYmplY3QgJiZcblx0XHRcdCh4LmNvbnN0cnVjdG9yID09PSBPYmplY3QgfHxcblx0XHRcdFx0T2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKHgpID09PSAnW29iamVjdCBPYmplY3RdJylcblx0XHQpXG5cdH1cblxuXHRmdW5jdGlvbiBlYWNoKGNvbGxlY3Rpb24sIGNhbGxiYWNrKSB7XG5cdFx0aWYgKGlzT2JqZWN0KGNvbGxlY3Rpb24pKSB7XG5cdFx0XHR2YXIga2V5cyA9IE9iamVjdC5rZXlzKGNvbGxlY3Rpb24pO1xuXHRcdFx0cmV0dXJuIGtleXMuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IHJldHVybiBjYWxsYmFjayhjb2xsZWN0aW9uW2tleV0sIGtleSwgY29sbGVjdGlvbik7IH0pXG5cdFx0fVxuXHRcdGlmIChjb2xsZWN0aW9uIGluc3RhbmNlb2YgQXJyYXkpIHtcblx0XHRcdHJldHVybiBjb2xsZWN0aW9uLmZvckVhY2goZnVuY3Rpb24gKGl0ZW0sIGkpIHsgcmV0dXJuIGNhbGxiYWNrKGl0ZW0sIGksIGNvbGxlY3Rpb24pOyB9KVxuXHRcdH1cblx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdFeHBlY3RlZCBlaXRoZXIgYW4gYXJyYXkgb3Igb2JqZWN0IGxpdGVyYWwuJylcblx0fVxuXG5cdGZ1bmN0aW9uIGxvZ2dlcihtZXNzYWdlKSB7XG5cdFx0dmFyIGRldGFpbHMgPSBbXSwgbGVuID0gYXJndW1lbnRzLmxlbmd0aCAtIDE7XG5cdFx0d2hpbGUgKCBsZW4tLSA+IDAgKSBkZXRhaWxzWyBsZW4gXSA9IGFyZ3VtZW50c1sgbGVuICsgMSBdO1xuXG5cdFx0aWYgKHRoaXMuY29uc3RydWN0b3IuZGVidWcgJiYgY29uc29sZSkge1xuXHRcdFx0dmFyIHJlcG9ydCA9IFwiJWNTY3JvbGxSZXZlYWw6IFwiICsgbWVzc2FnZTtcblx0XHRcdGRldGFpbHMuZm9yRWFjaChmdW5jdGlvbiAoZGV0YWlsKSB7IHJldHVybiAocmVwb3J0ICs9IFwiXFxuIOKAlCBcIiArIGRldGFpbCk7IH0pO1xuXHRcdFx0Y29uc29sZS5sb2cocmVwb3J0LCAnY29sb3I6ICNlYTY1NGI7Jyk7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tY29uc29sZVxuXHRcdH1cblx0fVxuXG5cdGZ1bmN0aW9uIHJpbnNlKCkge1xuXHRcdHZhciB0aGlzJDEgPSB0aGlzO1xuXG5cdFx0dmFyIHN0cnVjdCA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuICh7XG5cdFx0XHRhY3RpdmU6IFtdLFxuXHRcdFx0c3RhbGU6IFtdXG5cdFx0fSk7IH07XG5cblx0XHR2YXIgZWxlbWVudElkcyA9IHN0cnVjdCgpO1xuXHRcdHZhciBzZXF1ZW5jZUlkcyA9IHN0cnVjdCgpO1xuXHRcdHZhciBjb250YWluZXJJZHMgPSBzdHJ1Y3QoKTtcblxuXHRcdC8qKlxuXHRcdCAqIFRha2Ugc3RvY2sgb2YgYWN0aXZlIGVsZW1lbnQgSURzLlxuXHRcdCAqL1xuXHRcdHRyeSB7XG5cdFx0XHRlYWNoKHRlYWxpZ2h0KCdbZGF0YS1zci1pZF0nKSwgZnVuY3Rpb24gKG5vZGUpIHtcblx0XHRcdFx0dmFyIGlkID0gcGFyc2VJbnQobm9kZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtc3ItaWQnKSk7XG5cdFx0XHRcdGVsZW1lbnRJZHMuYWN0aXZlLnB1c2goaWQpO1xuXHRcdFx0fSk7XG5cdFx0fSBjYXRjaCAoZSkge1xuXHRcdFx0dGhyb3cgZVxuXHRcdH1cblx0XHQvKipcblx0XHQgKiBEZXN0cm95IHN0YWxlIGVsZW1lbnRzLlxuXHRcdCAqL1xuXHRcdGVhY2godGhpcy5zdG9yZS5lbGVtZW50cywgZnVuY3Rpb24gKGVsZW1lbnQpIHtcblx0XHRcdGlmIChlbGVtZW50SWRzLmFjdGl2ZS5pbmRleE9mKGVsZW1lbnQuaWQpID09PSAtMSkge1xuXHRcdFx0XHRlbGVtZW50SWRzLnN0YWxlLnB1c2goZWxlbWVudC5pZCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0XHRlYWNoKGVsZW1lbnRJZHMuc3RhbGUsIGZ1bmN0aW9uIChzdGFsZUlkKSB7IHJldHVybiBkZWxldGUgdGhpcyQxLnN0b3JlLmVsZW1lbnRzW3N0YWxlSWRdOyB9KTtcblxuXHRcdC8qKlxuXHRcdCAqIFRha2Ugc3RvY2sgb2YgYWN0aXZlIGNvbnRhaW5lciBhbmQgc2VxdWVuY2UgSURzLlxuXHRcdCAqL1xuXHRcdGVhY2godGhpcy5zdG9yZS5lbGVtZW50cywgZnVuY3Rpb24gKGVsZW1lbnQpIHtcblx0XHRcdGlmIChjb250YWluZXJJZHMuYWN0aXZlLmluZGV4T2YoZWxlbWVudC5jb250YWluZXJJZCkgPT09IC0xKSB7XG5cdFx0XHRcdGNvbnRhaW5lcklkcy5hY3RpdmUucHVzaChlbGVtZW50LmNvbnRhaW5lcklkKTtcblx0XHRcdH1cblx0XHRcdGlmIChlbGVtZW50Lmhhc093blByb3BlcnR5KCdzZXF1ZW5jZScpKSB7XG5cdFx0XHRcdGlmIChzZXF1ZW5jZUlkcy5hY3RpdmUuaW5kZXhPZihlbGVtZW50LnNlcXVlbmNlLmlkKSA9PT0gLTEpIHtcblx0XHRcdFx0XHRzZXF1ZW5jZUlkcy5hY3RpdmUucHVzaChlbGVtZW50LnNlcXVlbmNlLmlkKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH0pO1xuXG5cdFx0LyoqXG5cdFx0ICogRGVzdHJveSBzdGFsZSBjb250YWluZXJzLlxuXHRcdCAqL1xuXHRcdGVhY2godGhpcy5zdG9yZS5jb250YWluZXJzLCBmdW5jdGlvbiAoY29udGFpbmVyKSB7XG5cdFx0XHRpZiAoY29udGFpbmVySWRzLmFjdGl2ZS5pbmRleE9mKGNvbnRhaW5lci5pZCkgPT09IC0xKSB7XG5cdFx0XHRcdGNvbnRhaW5lcklkcy5zdGFsZS5wdXNoKGNvbnRhaW5lci5pZCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0XHRlYWNoKGNvbnRhaW5lcklkcy5zdGFsZSwgZnVuY3Rpb24gKHN0YWxlSWQpIHtcblx0XHRcdHZhciBzdGFsZSA9IHRoaXMkMS5zdG9yZS5jb250YWluZXJzW3N0YWxlSWRdLm5vZGU7XG5cdFx0XHRzdGFsZS5yZW1vdmVFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzJDEuZGVsZWdhdGUpO1xuXHRcdFx0c3RhbGUucmVtb3ZlRXZlbnRMaXN0ZW5lcigncmVzaXplJywgdGhpcyQxLmRlbGVnYXRlKTtcblx0XHRcdGRlbGV0ZSB0aGlzJDEuc3RvcmUuY29udGFpbmVyc1tzdGFsZUlkXTtcblx0XHR9KTtcblxuXHRcdC8qKlxuXHRcdCAqIERlc3Ryb3kgc3RhbGUgc2VxdWVuY2VzLlxuXHRcdCAqL1xuXHRcdGVhY2godGhpcy5zdG9yZS5zZXF1ZW5jZXMsIGZ1bmN0aW9uIChzZXF1ZW5jZSkge1xuXHRcdFx0aWYgKHNlcXVlbmNlSWRzLmFjdGl2ZS5pbmRleE9mKHNlcXVlbmNlLmlkKSA9PT0gLTEpIHtcblx0XHRcdFx0c2VxdWVuY2VJZHMuc3RhbGUucHVzaChzZXF1ZW5jZS5pZCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0XHRlYWNoKHNlcXVlbmNlSWRzLnN0YWxlLCBmdW5jdGlvbiAoc3RhbGVJZCkgeyByZXR1cm4gZGVsZXRlIHRoaXMkMS5zdG9yZS5zZXF1ZW5jZXNbc3RhbGVJZF07IH0pO1xuXHR9XG5cblx0ZnVuY3Rpb24gY2xlYW4odGFyZ2V0KSB7XG5cdFx0dmFyIHRoaXMkMSA9IHRoaXM7XG5cblx0XHR2YXIgZGlydHk7XG5cdFx0dHJ5IHtcblx0XHRcdGVhY2godGVhbGlnaHQodGFyZ2V0KSwgZnVuY3Rpb24gKG5vZGUpIHtcblx0XHRcdFx0dmFyIGlkID0gbm9kZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtc3ItaWQnKTtcblx0XHRcdFx0aWYgKGlkICE9PSBudWxsKSB7XG5cdFx0XHRcdFx0ZGlydHkgPSB0cnVlO1xuXHRcdFx0XHRcdHZhciBlbGVtZW50ID0gdGhpcyQxLnN0b3JlLmVsZW1lbnRzW2lkXTtcblx0XHRcdFx0XHRpZiAoZWxlbWVudC5jYWxsYmFja1RpbWVyKSB7XG5cdFx0XHRcdFx0XHR3aW5kb3cuY2xlYXJUaW1lb3V0KGVsZW1lbnQuY2FsbGJhY2tUaW1lci5jbG9jayk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdG5vZGUuc2V0QXR0cmlidXRlKCdzdHlsZScsIGVsZW1lbnQuc3R5bGVzLmlubGluZS5nZW5lcmF0ZWQpO1xuXHRcdFx0XHRcdG5vZGUucmVtb3ZlQXR0cmlidXRlKCdkYXRhLXNyLWlkJyk7XG5cdFx0XHRcdFx0ZGVsZXRlIHRoaXMkMS5zdG9yZS5lbGVtZW50c1tpZF07XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH0gY2F0Y2ggKGUpIHtcblx0XHRcdHJldHVybiBsb2dnZXIuY2FsbCh0aGlzLCAnQ2xlYW4gZmFpbGVkLicsIGUubWVzc2FnZSlcblx0XHR9XG5cblx0XHRpZiAoZGlydHkpIHtcblx0XHRcdHRyeSB7XG5cdFx0XHRcdHJpbnNlLmNhbGwodGhpcyk7XG5cdFx0XHR9IGNhdGNoIChlKSB7XG5cdFx0XHRcdHJldHVybiBsb2dnZXIuY2FsbCh0aGlzLCAnQ2xlYW4gZmFpbGVkLicsIGUubWVzc2FnZSlcblx0XHRcdH1cblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiBkZXN0cm95KCkge1xuXHRcdHZhciB0aGlzJDEgPSB0aGlzO1xuXG5cdFx0LyoqXG5cdFx0ICogUmVtb3ZlIGFsbCBnZW5lcmF0ZWQgc3R5bGVzIGFuZCBlbGVtZW50IGlkc1xuXHRcdCAqL1xuXHRcdGVhY2godGhpcy5zdG9yZS5lbGVtZW50cywgZnVuY3Rpb24gKGVsZW1lbnQpIHtcblx0XHRcdGVsZW1lbnQubm9kZS5zZXRBdHRyaWJ1dGUoJ3N0eWxlJywgZWxlbWVudC5zdHlsZXMuaW5saW5lLmdlbmVyYXRlZCk7XG5cdFx0XHRlbGVtZW50Lm5vZGUucmVtb3ZlQXR0cmlidXRlKCdkYXRhLXNyLWlkJyk7XG5cdFx0fSk7XG5cblx0XHQvKipcblx0XHQgKiBSZW1vdmUgYWxsIGV2ZW50IGxpc3RlbmVycy5cblx0XHQgKi9cblx0XHRlYWNoKHRoaXMuc3RvcmUuY29udGFpbmVycywgZnVuY3Rpb24gKGNvbnRhaW5lcikge1xuXHRcdFx0dmFyIHRhcmdldCA9XG5cdFx0XHRcdGNvbnRhaW5lci5ub2RlID09PSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQgPyB3aW5kb3cgOiBjb250YWluZXIubm9kZTtcblx0XHRcdHRhcmdldC5yZW1vdmVFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzJDEuZGVsZWdhdGUpO1xuXHRcdFx0dGFyZ2V0LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMkMS5kZWxlZ2F0ZSk7XG5cdFx0fSk7XG5cblx0XHQvKipcblx0XHQgKiBDbGVhciBhbGwgZGF0YSBmcm9tIHRoZSBzdG9yZVxuXHRcdCAqL1xuXHRcdHRoaXMuc3RvcmUgPSB7XG5cdFx0XHRjb250YWluZXJzOiB7fSxcblx0XHRcdGVsZW1lbnRzOiB7fSxcblx0XHRcdGhpc3Rvcnk6IFtdLFxuXHRcdFx0c2VxdWVuY2VzOiB7fVxuXHRcdH07XG5cdH1cblxuXHQvKiEgQGxpY2Vuc2UgUmVtYXRyaXggdjAuMy4wXG5cblx0XHRDb3B5cmlnaHQgMjAxOCBKdWxpYW4gTGxveWQuXG5cblx0XHRQZXJtaXNzaW9uIGlzIGhlcmVieSBncmFudGVkLCBmcmVlIG9mIGNoYXJnZSwgdG8gYW55IHBlcnNvbiBvYnRhaW5pbmcgYSBjb3B5XG5cdFx0b2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGUgXCJTb2Z0d2FyZVwiKSwgdG8gZGVhbFxuXHRcdGluIHRoZSBTb2Z0d2FyZSB3aXRob3V0IHJlc3RyaWN0aW9uLCBpbmNsdWRpbmcgd2l0aG91dCBsaW1pdGF0aW9uIHRoZSByaWdodHNcblx0XHR0byB1c2UsIGNvcHksIG1vZGlmeSwgbWVyZ2UsIHB1Ymxpc2gsIGRpc3RyaWJ1dGUsIHN1YmxpY2Vuc2UsIGFuZC9vciBzZWxsXG5cdFx0Y29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdCBwZXJzb25zIHRvIHdob20gdGhlIFNvZnR3YXJlIGlzXG5cdFx0ZnVybmlzaGVkIHRvIGRvIHNvLCBzdWJqZWN0IHRvIHRoZSBmb2xsb3dpbmcgY29uZGl0aW9uczpcblxuXHRcdFRoZSBhYm92ZSBjb3B5cmlnaHQgbm90aWNlIGFuZCB0aGlzIHBlcm1pc3Npb24gbm90aWNlIHNoYWxsIGJlIGluY2x1ZGVkIGluXG5cdFx0YWxsIGNvcGllcyBvciBzdWJzdGFudGlhbCBwb3J0aW9ucyBvZiB0aGUgU29mdHdhcmUuXG5cblx0XHRUSEUgU09GVFdBUkUgSVMgUFJPVklERUQgXCJBUyBJU1wiLCBXSVRIT1VUIFdBUlJBTlRZIE9GIEFOWSBLSU5ELCBFWFBSRVNTIE9SXG5cdFx0SU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRiBNRVJDSEFOVEFCSUxJVFksXG5cdFx0RklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UgQU5EIE5PTklORlJJTkdFTUVOVC4gSU4gTk8gRVZFTlQgU0hBTEwgVEhFXG5cdFx0QVVUSE9SUyBPUiBDT1BZUklHSFQgSE9MREVSUyBCRSBMSUFCTEUgRk9SIEFOWSBDTEFJTSwgREFNQUdFUyBPUiBPVEhFUlxuXHRcdExJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1IgT1RIRVJXSVNFLCBBUklTSU5HIEZST00sXG5cdFx0T1VUIE9GIE9SIElOIENPTk5FQ1RJT04gV0lUSCBUSEUgU09GVFdBUkUgT1IgVEhFIFVTRSBPUiBPVEhFUiBERUFMSU5HUyBJTlxuXHRcdFRIRSBTT0ZUV0FSRS5cblx0Ki9cblx0LyoqXG5cdCAqIEBtb2R1bGUgUmVtYXRyaXhcblx0ICovXG5cblx0LyoqXG5cdCAqIFRyYW5zZm9ybWF0aW9uIG1hdHJpY2VzIGluIHRoZSBicm93c2VyIGNvbWUgaW4gdHdvIGZsYXZvcnM6XG5cdCAqXG5cdCAqICAtIGBtYXRyaXhgIHVzaW5nIDYgdmFsdWVzIChzaG9ydClcblx0ICogIC0gYG1hdHJpeDNkYCB1c2luZyAxNiB2YWx1ZXMgKGxvbmcpXG5cdCAqXG5cdCAqIFRoaXMgdXRpbGl0eSBmb2xsb3dzIHRoaXMgW2NvbnZlcnNpb24gZ3VpZGVdKGh0dHBzOi8vZ29vLmdsL0VKbFVRMSlcblx0ICogdG8gZXhwYW5kIHNob3J0IGZvcm0gbWF0cmljZXMgdG8gdGhlaXIgZXF1aXZhbGVudCBsb25nIGZvcm0uXG5cdCAqXG5cdCAqIEBwYXJhbSAge2FycmF5fSBzb3VyY2UgLSBBY2NlcHRzIGJvdGggc2hvcnQgYW5kIGxvbmcgZm9ybSBtYXRyaWNlcy5cblx0ICogQHJldHVybiB7YXJyYXl9XG5cdCAqL1xuXHRmdW5jdGlvbiBmb3JtYXQoc291cmNlKSB7XG5cdFx0aWYgKHNvdXJjZS5jb25zdHJ1Y3RvciAhPT0gQXJyYXkpIHtcblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0V4cGVjdGVkIGFycmF5LicpXG5cdFx0fVxuXHRcdGlmIChzb3VyY2UubGVuZ3RoID09PSAxNikge1xuXHRcdFx0cmV0dXJuIHNvdXJjZVxuXHRcdH1cblx0XHRpZiAoc291cmNlLmxlbmd0aCA9PT0gNikge1xuXHRcdFx0dmFyIG1hdHJpeCA9IGlkZW50aXR5KCk7XG5cdFx0XHRtYXRyaXhbMF0gPSBzb3VyY2VbMF07XG5cdFx0XHRtYXRyaXhbMV0gPSBzb3VyY2VbMV07XG5cdFx0XHRtYXRyaXhbNF0gPSBzb3VyY2VbMl07XG5cdFx0XHRtYXRyaXhbNV0gPSBzb3VyY2VbM107XG5cdFx0XHRtYXRyaXhbMTJdID0gc291cmNlWzRdO1xuXHRcdFx0bWF0cml4WzEzXSA9IHNvdXJjZVs1XTtcblx0XHRcdHJldHVybiBtYXRyaXhcblx0XHR9XG5cdFx0dGhyb3cgbmV3IFJhbmdlRXJyb3IoJ0V4cGVjdGVkIGFycmF5IHdpdGggZWl0aGVyIDYgb3IgMTYgdmFsdWVzLicpXG5cdH1cblxuXHQvKipcblx0ICogUmV0dXJucyBhIG1hdHJpeCByZXByZXNlbnRpbmcgbm8gdHJhbnNmb3JtYXRpb24uIFRoZSBwcm9kdWN0IG9mIGFueSBtYXRyaXhcblx0ICogbXVsdGlwbGllZCBieSB0aGUgaWRlbnRpdHkgbWF0cml4IHdpbGwgYmUgdGhlIG9yaWdpbmFsIG1hdHJpeC5cblx0ICpcblx0ICogPiAqKlRpcDoqKiBTaW1pbGFyIHRvIGhvdyBgNSAqIDEgPT09IDVgLCB3aGVyZSBgMWAgaXMgdGhlIGlkZW50aXR5LlxuXHQgKlxuXHQgKiBAcmV0dXJuIHthcnJheX1cblx0ICovXG5cdGZ1bmN0aW9uIGlkZW50aXR5KCkge1xuXHRcdHZhciBtYXRyaXggPSBbXTtcblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IDE2OyBpKyspIHtcblx0XHRcdGkgJSA1ID09IDAgPyBtYXRyaXgucHVzaCgxKSA6IG1hdHJpeC5wdXNoKDApO1xuXHRcdH1cblx0XHRyZXR1cm4gbWF0cml4XG5cdH1cblxuXHQvKipcblx0ICogUmV0dXJucyBhIDR4NCBtYXRyaXggZGVzY3JpYmluZyB0aGUgY29tYmluZWQgdHJhbnNmb3JtYXRpb25zXG5cdCAqIG9mIGJvdGggYXJndW1lbnRzLlxuXHQgKlxuXHQgKiA+ICoqTm90ZToqKiBPcmRlciBpcyB2ZXJ5IGltcG9ydGFudC4gRm9yIGV4YW1wbGUsIHJvdGF0aW5nIDQ1wrBcblx0ICogYWxvbmcgdGhlIFotYXhpcywgZm9sbG93ZWQgYnkgdHJhbnNsYXRpbmcgNTAwIHBpeGVscyBhbG9uZyB0aGVcblx0ICogWS1heGlzLi4uIGlzIG5vdCB0aGUgc2FtZSBhcyB0cmFuc2xhdGluZyA1MDAgcGl4ZWxzIGFsb25nIHRoZVxuXHQgKiBZLWF4aXMsIGZvbGxvd2VkIGJ5IHJvdGF0aW5nIDQ1wrAgYWxvbmcgb24gdGhlIFotYXhpcy5cblx0ICpcblx0ICogQHBhcmFtICB7YXJyYXl9IG0gLSBBY2NlcHRzIGJvdGggc2hvcnQgYW5kIGxvbmcgZm9ybSBtYXRyaWNlcy5cblx0ICogQHBhcmFtICB7YXJyYXl9IHggLSBBY2NlcHRzIGJvdGggc2hvcnQgYW5kIGxvbmcgZm9ybSBtYXRyaWNlcy5cblx0ICogQHJldHVybiB7YXJyYXl9XG5cdCAqL1xuXHRmdW5jdGlvbiBtdWx0aXBseShtLCB4KSB7XG5cdFx0dmFyIGZtID0gZm9ybWF0KG0pO1xuXHRcdHZhciBmeCA9IGZvcm1hdCh4KTtcblx0XHR2YXIgcHJvZHVjdCA9IFtdO1xuXG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCA0OyBpKyspIHtcblx0XHRcdHZhciByb3cgPSBbZm1baV0sIGZtW2kgKyA0XSwgZm1baSArIDhdLCBmbVtpICsgMTJdXTtcblx0XHRcdGZvciAodmFyIGogPSAwOyBqIDwgNDsgaisrKSB7XG5cdFx0XHRcdHZhciBrID0gaiAqIDQ7XG5cdFx0XHRcdHZhciBjb2wgPSBbZnhba10sIGZ4W2sgKyAxXSwgZnhbayArIDJdLCBmeFtrICsgM11dO1xuXHRcdFx0XHR2YXIgcmVzdWx0ID1cblx0XHRcdFx0XHRyb3dbMF0gKiBjb2xbMF0gKyByb3dbMV0gKiBjb2xbMV0gKyByb3dbMl0gKiBjb2xbMl0gKyByb3dbM10gKiBjb2xbM107XG5cblx0XHRcdFx0cHJvZHVjdFtpICsga10gPSByZXN1bHQ7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0cmV0dXJuIHByb2R1Y3Rcblx0fVxuXG5cdC8qKlxuXHQgKiBBdHRlbXB0cyB0byByZXR1cm4gYSA0eDQgbWF0cml4IGRlc2NyaWJpbmcgdGhlIENTUyB0cmFuc2Zvcm1cblx0ICogbWF0cml4IHBhc3NlZCBpbiwgYnV0IHdpbGwgcmV0dXJuIHRoZSBpZGVudGl0eSBtYXRyaXggYXMgYVxuXHQgKiBmYWxsYmFjay5cblx0ICpcblx0ICogPiAqKlRpcDoqKiBUaGlzIG1ldGhvZCBpcyB1c2VkIHRvIGNvbnZlcnQgYSBDU1MgbWF0cml4IChyZXRyaWV2ZWQgYXMgYVxuXHQgKiBgc3RyaW5nYCBmcm9tIGNvbXB1dGVkIHN0eWxlcykgdG8gaXRzIGVxdWl2YWxlbnQgYXJyYXkgZm9ybWF0LlxuXHQgKlxuXHQgKiBAcGFyYW0gIHtzdHJpbmd9IHNvdXJjZSAtIGBtYXRyaXhgIG9yIGBtYXRyaXgzZGAgQ1NTIFRyYW5zZm9ybSB2YWx1ZS5cblx0ICogQHJldHVybiB7YXJyYXl9XG5cdCAqL1xuXHRmdW5jdGlvbiBwYXJzZShzb3VyY2UpIHtcblx0XHRpZiAodHlwZW9mIHNvdXJjZSA9PT0gJ3N0cmluZycpIHtcblx0XHRcdHZhciBtYXRjaCA9IHNvdXJjZS5tYXRjaCgvbWF0cml4KDNkKT9cXCgoW14pXSspXFwpLyk7XG5cdFx0XHRpZiAobWF0Y2gpIHtcblx0XHRcdFx0dmFyIHJhdyA9IG1hdGNoWzJdLnNwbGl0KCcsICcpLm1hcChwYXJzZUZsb2F0KTtcblx0XHRcdFx0cmV0dXJuIGZvcm1hdChyYXcpXG5cdFx0XHR9XG5cdFx0fVxuXHRcdHJldHVybiBpZGVudGl0eSgpXG5cdH1cblxuXHQvKipcblx0ICogUmV0dXJucyBhIDR4NCBtYXRyaXggZGVzY3JpYmluZyBYLWF4aXMgcm90YXRpb24uXG5cdCAqXG5cdCAqIEBwYXJhbSAge251bWJlcn0gYW5nbGUgLSBNZWFzdXJlZCBpbiBkZWdyZWVzLlxuXHQgKiBAcmV0dXJuIHthcnJheX1cblx0ICovXG5cdGZ1bmN0aW9uIHJvdGF0ZVgoYW5nbGUpIHtcblx0XHR2YXIgdGhldGEgPSBNYXRoLlBJIC8gMTgwICogYW5nbGU7XG5cdFx0dmFyIG1hdHJpeCA9IGlkZW50aXR5KCk7XG5cblx0XHRtYXRyaXhbNV0gPSBtYXRyaXhbMTBdID0gTWF0aC5jb3ModGhldGEpO1xuXHRcdG1hdHJpeFs2XSA9IG1hdHJpeFs5XSA9IE1hdGguc2luKHRoZXRhKTtcblx0XHRtYXRyaXhbOV0gKj0gLTE7XG5cblx0XHRyZXR1cm4gbWF0cml4XG5cdH1cblxuXHQvKipcblx0ICogUmV0dXJucyBhIDR4NCBtYXRyaXggZGVzY3JpYmluZyBZLWF4aXMgcm90YXRpb24uXG5cdCAqXG5cdCAqIEBwYXJhbSAge251bWJlcn0gYW5nbGUgLSBNZWFzdXJlZCBpbiBkZWdyZWVzLlxuXHQgKiBAcmV0dXJuIHthcnJheX1cblx0ICovXG5cdGZ1bmN0aW9uIHJvdGF0ZVkoYW5nbGUpIHtcblx0XHR2YXIgdGhldGEgPSBNYXRoLlBJIC8gMTgwICogYW5nbGU7XG5cdFx0dmFyIG1hdHJpeCA9IGlkZW50aXR5KCk7XG5cblx0XHRtYXRyaXhbMF0gPSBtYXRyaXhbMTBdID0gTWF0aC5jb3ModGhldGEpO1xuXHRcdG1hdHJpeFsyXSA9IG1hdHJpeFs4XSA9IE1hdGguc2luKHRoZXRhKTtcblx0XHRtYXRyaXhbMl0gKj0gLTE7XG5cblx0XHRyZXR1cm4gbWF0cml4XG5cdH1cblxuXHQvKipcblx0ICogUmV0dXJucyBhIDR4NCBtYXRyaXggZGVzY3JpYmluZyBaLWF4aXMgcm90YXRpb24uXG5cdCAqXG5cdCAqIEBwYXJhbSAge251bWJlcn0gYW5nbGUgLSBNZWFzdXJlZCBpbiBkZWdyZWVzLlxuXHQgKiBAcmV0dXJuIHthcnJheX1cblx0ICovXG5cdGZ1bmN0aW9uIHJvdGF0ZVooYW5nbGUpIHtcblx0XHR2YXIgdGhldGEgPSBNYXRoLlBJIC8gMTgwICogYW5nbGU7XG5cdFx0dmFyIG1hdHJpeCA9IGlkZW50aXR5KCk7XG5cblx0XHRtYXRyaXhbMF0gPSBtYXRyaXhbNV0gPSBNYXRoLmNvcyh0aGV0YSk7XG5cdFx0bWF0cml4WzFdID0gbWF0cml4WzRdID0gTWF0aC5zaW4odGhldGEpO1xuXHRcdG1hdHJpeFs0XSAqPSAtMTtcblxuXHRcdHJldHVybiBtYXRyaXhcblx0fVxuXG5cdC8qKlxuXHQgKiBSZXR1cm5zIGEgNHg0IG1hdHJpeCBkZXNjcmliaW5nIDJEIHNjYWxpbmcuIFRoZSBmaXJzdCBhcmd1bWVudFxuXHQgKiBpcyB1c2VkIGZvciBib3RoIFggYW5kIFktYXhpcyBzY2FsaW5nLCB1bmxlc3MgYW4gb3B0aW9uYWxcblx0ICogc2Vjb25kIGFyZ3VtZW50IGlzIHByb3ZpZGVkIHRvIGV4cGxpY2l0bHkgZGVmaW5lIFktYXhpcyBzY2FsaW5nLlxuXHQgKlxuXHQgKiBAcGFyYW0gIHtudW1iZXJ9IHNjYWxhciAgICAtIERlY2ltYWwgbXVsdGlwbGllci5cblx0ICogQHBhcmFtICB7bnVtYmVyfSBbc2NhbGFyWV0gLSBEZWNpbWFsIG11bHRpcGxpZXIuXG5cdCAqIEByZXR1cm4ge2FycmF5fVxuXHQgKi9cblx0ZnVuY3Rpb24gc2NhbGUoc2NhbGFyLCBzY2FsYXJZKSB7XG5cdFx0dmFyIG1hdHJpeCA9IGlkZW50aXR5KCk7XG5cblx0XHRtYXRyaXhbMF0gPSBzY2FsYXI7XG5cdFx0bWF0cml4WzVdID0gdHlwZW9mIHNjYWxhclkgPT09ICdudW1iZXInID8gc2NhbGFyWSA6IHNjYWxhcjtcblxuXHRcdHJldHVybiBtYXRyaXhcblx0fVxuXG5cdC8qKlxuXHQgKiBSZXR1cm5zIGEgNHg0IG1hdHJpeCBkZXNjcmliaW5nIFgtYXhpcyB0cmFuc2xhdGlvbi5cblx0ICpcblx0ICogQHBhcmFtICB7bnVtYmVyfSBkaXN0YW5jZSAtIE1lYXN1cmVkIGluIHBpeGVscy5cblx0ICogQHJldHVybiB7YXJyYXl9XG5cdCAqL1xuXHRmdW5jdGlvbiB0cmFuc2xhdGVYKGRpc3RhbmNlKSB7XG5cdFx0dmFyIG1hdHJpeCA9IGlkZW50aXR5KCk7XG5cdFx0bWF0cml4WzEyXSA9IGRpc3RhbmNlO1xuXHRcdHJldHVybiBtYXRyaXhcblx0fVxuXG5cdC8qKlxuXHQgKiBSZXR1cm5zIGEgNHg0IG1hdHJpeCBkZXNjcmliaW5nIFktYXhpcyB0cmFuc2xhdGlvbi5cblx0ICpcblx0ICogQHBhcmFtICB7bnVtYmVyfSBkaXN0YW5jZSAtIE1lYXN1cmVkIGluIHBpeGVscy5cblx0ICogQHJldHVybiB7YXJyYXl9XG5cdCAqL1xuXHRmdW5jdGlvbiB0cmFuc2xhdGVZKGRpc3RhbmNlKSB7XG5cdFx0dmFyIG1hdHJpeCA9IGlkZW50aXR5KCk7XG5cdFx0bWF0cml4WzEzXSA9IGRpc3RhbmNlO1xuXHRcdHJldHVybiBtYXRyaXhcblx0fVxuXG5cdHZhciBnZXRQcmVmaXhlZENzc1Byb3AgPSAoZnVuY3Rpb24gKCkge1xuXHRcdHZhciBwcm9wZXJ0aWVzID0ge307XG5cdFx0dmFyIHN0eWxlID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnN0eWxlO1xuXG5cdFx0ZnVuY3Rpb24gZ2V0UHJlZml4ZWRDc3NQcm9wZXJ0eShuYW1lLCBzb3VyY2UpIHtcblx0XHRcdGlmICggc291cmNlID09PSB2b2lkIDAgKSBzb3VyY2UgPSBzdHlsZTtcblxuXHRcdFx0aWYgKG5hbWUgJiYgdHlwZW9mIG5hbWUgPT09ICdzdHJpbmcnKSB7XG5cdFx0XHRcdGlmIChwcm9wZXJ0aWVzW25hbWVdKSB7XG5cdFx0XHRcdFx0cmV0dXJuIHByb3BlcnRpZXNbbmFtZV1cblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAodHlwZW9mIHNvdXJjZVtuYW1lXSA9PT0gJ3N0cmluZycpIHtcblx0XHRcdFx0XHRyZXR1cm4gKHByb3BlcnRpZXNbbmFtZV0gPSBuYW1lKVxuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICh0eXBlb2Ygc291cmNlWyhcIi13ZWJraXQtXCIgKyBuYW1lKV0gPT09ICdzdHJpbmcnKSB7XG5cdFx0XHRcdFx0cmV0dXJuIChwcm9wZXJ0aWVzW25hbWVdID0gXCItd2Via2l0LVwiICsgbmFtZSlcblx0XHRcdFx0fVxuXHRcdFx0XHR0aHJvdyBuZXcgUmFuZ2VFcnJvcigoXCJVbmFibGUgdG8gZmluZCBcXFwiXCIgKyBuYW1lICsgXCJcXFwiIHN0eWxlIHByb3BlcnR5LlwiKSlcblx0XHRcdH1cblx0XHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ0V4cGVjdGVkIGEgc3RyaW5nLicpXG5cdFx0fVxuXG5cdFx0Z2V0UHJlZml4ZWRDc3NQcm9wZXJ0eS5jbGVhckNhY2hlID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gKHByb3BlcnRpZXMgPSB7fSk7IH07XG5cblx0XHRyZXR1cm4gZ2V0UHJlZml4ZWRDc3NQcm9wZXJ0eVxuXHR9KSgpO1xuXG5cdGZ1bmN0aW9uIHN0eWxlKGVsZW1lbnQpIHtcblx0XHR2YXIgY29tcHV0ZWQgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShlbGVtZW50Lm5vZGUpO1xuXHRcdHZhciBwb3NpdGlvbiA9IGNvbXB1dGVkLnBvc2l0aW9uO1xuXHRcdHZhciBjb25maWcgPSBlbGVtZW50LmNvbmZpZztcblxuXHRcdC8qKlxuXHRcdCAqIEdlbmVyYXRlIGlubGluZSBzdHlsZXNcblx0XHQgKi9cblx0XHR2YXIgaW5saW5lID0ge307XG5cdFx0dmFyIGlubGluZVN0eWxlID0gZWxlbWVudC5ub2RlLmdldEF0dHJpYnV0ZSgnc3R5bGUnKSB8fCAnJztcblx0XHR2YXIgaW5saW5lTWF0Y2ggPSBpbmxpbmVTdHlsZS5tYXRjaCgvW1xcdy1dK1xccyo6XFxzKlteO10rXFxzKi9naSkgfHwgW107XG5cblx0XHRpbmxpbmUuY29tcHV0ZWQgPSBpbmxpbmVNYXRjaCA/IGlubGluZU1hdGNoLm1hcChmdW5jdGlvbiAobSkgeyByZXR1cm4gbS50cmltKCk7IH0pLmpvaW4oJzsgJykgKyAnOycgOiAnJztcblxuXHRcdGlubGluZS5nZW5lcmF0ZWQgPSBpbmxpbmVNYXRjaC5zb21lKGZ1bmN0aW9uIChtKSB7IHJldHVybiBtLm1hdGNoKC92aXNpYmlsaXR5XFxzPzpcXHM/dmlzaWJsZS9pKTsgfSlcblx0XHRcdD8gaW5saW5lLmNvbXB1dGVkXG5cdFx0XHQ6IGlubGluZU1hdGNoLmNvbmNhdCggWyd2aXNpYmlsaXR5OiB2aXNpYmxlJ10pLm1hcChmdW5jdGlvbiAobSkgeyByZXR1cm4gbS50cmltKCk7IH0pLmpvaW4oJzsgJykgKyAnOyc7XG5cblx0XHQvKipcblx0XHQgKiBHZW5lcmF0ZSBvcGFjaXR5IHN0eWxlc1xuXHRcdCAqL1xuXHRcdHZhciBjb21wdXRlZE9wYWNpdHkgPSBwYXJzZUZsb2F0KGNvbXB1dGVkLm9wYWNpdHkpO1xuXHRcdHZhciBjb25maWdPcGFjaXR5ID0gIWlzTmFOKHBhcnNlRmxvYXQoY29uZmlnLm9wYWNpdHkpKVxuXHRcdFx0PyBwYXJzZUZsb2F0KGNvbmZpZy5vcGFjaXR5KVxuXHRcdFx0OiBwYXJzZUZsb2F0KGNvbXB1dGVkLm9wYWNpdHkpO1xuXG5cdFx0dmFyIG9wYWNpdHkgPSB7XG5cdFx0XHRjb21wdXRlZDogY29tcHV0ZWRPcGFjaXR5ICE9PSBjb25maWdPcGFjaXR5ID8gKFwib3BhY2l0eTogXCIgKyBjb21wdXRlZE9wYWNpdHkgKyBcIjtcIikgOiAnJyxcblx0XHRcdGdlbmVyYXRlZDogY29tcHV0ZWRPcGFjaXR5ICE9PSBjb25maWdPcGFjaXR5ID8gKFwib3BhY2l0eTogXCIgKyBjb25maWdPcGFjaXR5ICsgXCI7XCIpIDogJydcblx0XHR9O1xuXG5cdFx0LyoqXG5cdFx0ICogR2VuZXJhdGUgdHJhbnNmb3JtYXRpb24gc3R5bGVzXG5cdFx0ICovXG5cdFx0dmFyIHRyYW5zZm9ybWF0aW9ucyA9IFtdO1xuXG5cdFx0aWYgKHBhcnNlRmxvYXQoY29uZmlnLmRpc3RhbmNlKSkge1xuXHRcdFx0dmFyIGF4aXMgPSBjb25maWcub3JpZ2luID09PSAndG9wJyB8fCBjb25maWcub3JpZ2luID09PSAnYm90dG9tJyA/ICdZJyA6ICdYJztcblxuXHRcdFx0LyoqXG5cdFx0XHQgKiBMZXTigJlzIG1ha2Ugc3VyZSBvdXIgb3VyIHBpeGVsIGRpc3RhbmNlcyBhcmUgbmVnYXRpdmUgZm9yIHRvcCBhbmQgbGVmdC5cblx0XHRcdCAqIGUuZy4geyBvcmlnaW46ICd0b3AnLCBkaXN0YW5jZTogJzI1cHgnIH0gc3RhcnRzIGF0IGB0b3A6IC0yNXB4YCBpbiBDU1MuXG5cdFx0XHQgKi9cblx0XHRcdHZhciBkaXN0YW5jZSA9IGNvbmZpZy5kaXN0YW5jZTtcblx0XHRcdGlmIChjb25maWcub3JpZ2luID09PSAndG9wJyB8fCBjb25maWcub3JpZ2luID09PSAnbGVmdCcpIHtcblx0XHRcdFx0ZGlzdGFuY2UgPSAvXi0vLnRlc3QoZGlzdGFuY2UpID8gZGlzdGFuY2Uuc3Vic3RyKDEpIDogKFwiLVwiICsgZGlzdGFuY2UpO1xuXHRcdFx0fVxuXG5cdFx0XHR2YXIgcmVmID0gZGlzdGFuY2UubWF0Y2goLyheLT9cXGQrXFwuP1xcZD8pfChlbSR8cHgkfCUkKS9nKTtcblx0XHRcdHZhciB2YWx1ZSA9IHJlZlswXTtcblx0XHRcdHZhciB1bml0ID0gcmVmWzFdO1xuXG5cdFx0XHRzd2l0Y2ggKHVuaXQpIHtcblx0XHRcdFx0Y2FzZSAnZW0nOlxuXHRcdFx0XHRcdGRpc3RhbmNlID0gcGFyc2VJbnQoY29tcHV0ZWQuZm9udFNpemUpICogdmFsdWU7XG5cdFx0XHRcdFx0YnJlYWtcblx0XHRcdFx0Y2FzZSAncHgnOlxuXHRcdFx0XHRcdGRpc3RhbmNlID0gdmFsdWU7XG5cdFx0XHRcdFx0YnJlYWtcblx0XHRcdFx0Y2FzZSAnJSc6XG5cdFx0XHRcdFx0LyoqXG5cdFx0XHRcdFx0ICogSGVyZSB3ZSB1c2UgYGdldEJvdW5kaW5nQ2xpZW50UmVjdGAgaW5zdGVhZCBvZlxuXHRcdFx0XHRcdCAqIHRoZSBleGlzdGluZyBkYXRhIGF0dGFjaGVkIHRvIGBlbGVtZW50Lmdlb21ldHJ5YFxuXHRcdFx0XHRcdCAqIGJlY2F1c2Ugb25seSB0aGUgZm9ybWVyIGluY2x1ZGVzIGFueSB0cmFuc2Zvcm1hdGlvbnNcblx0XHRcdFx0XHQgKiBjdXJyZW50IGFwcGxpZWQgdG8gdGhlIGVsZW1lbnQuXG5cdFx0XHRcdFx0ICpcblx0XHRcdFx0XHQgKiBJZiB0aGF0IGJlaGF2aW9yIGVuZHMgdXAgYmVpbmcgdW5pbnR1aXRpdmUsIHRoaXNcblx0XHRcdFx0XHQgKiBsb2dpYyBjb3VsZCBpbnN0ZWFkIHV0aWxpemUgYGVsZW1lbnQuZ2VvbWV0cnkuaGVpZ2h0YFxuXHRcdFx0XHRcdCAqIGFuZCBgZWxlbWVudC5nZW9lbWV0cnkud2lkdGhgIGZvciB0aGUgZGlzdGFuY2UgY2FsY3VsYXRpb25cblx0XHRcdFx0XHQgKi9cblx0XHRcdFx0XHRkaXN0YW5jZSA9XG5cdFx0XHRcdFx0XHRheGlzID09PSAnWSdcblx0XHRcdFx0XHRcdFx0PyAoZWxlbWVudC5ub2RlLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmhlaWdodCAqIHZhbHVlKSAvIDEwMFxuXHRcdFx0XHRcdFx0XHQ6IChlbGVtZW50Lm5vZGUuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkud2lkdGggKiB2YWx1ZSkgLyAxMDA7XG5cdFx0XHRcdFx0YnJlYWtcblx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHR0aHJvdyBuZXcgUmFuZ2VFcnJvcignVW5yZWNvZ25pemVkIG9yIG1pc3NpbmcgZGlzdGFuY2UgdW5pdC4nKVxuXHRcdFx0fVxuXG5cdFx0XHRpZiAoYXhpcyA9PT0gJ1knKSB7XG5cdFx0XHRcdHRyYW5zZm9ybWF0aW9ucy5wdXNoKHRyYW5zbGF0ZVkoZGlzdGFuY2UpKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHRyYW5zZm9ybWF0aW9ucy5wdXNoKHRyYW5zbGF0ZVgoZGlzdGFuY2UpKTtcblx0XHRcdH1cblx0XHR9XG5cblx0XHRpZiAoY29uZmlnLnJvdGF0ZS54KSB7IHRyYW5zZm9ybWF0aW9ucy5wdXNoKHJvdGF0ZVgoY29uZmlnLnJvdGF0ZS54KSk7IH1cblx0XHRpZiAoY29uZmlnLnJvdGF0ZS55KSB7IHRyYW5zZm9ybWF0aW9ucy5wdXNoKHJvdGF0ZVkoY29uZmlnLnJvdGF0ZS55KSk7IH1cblx0XHRpZiAoY29uZmlnLnJvdGF0ZS56KSB7IHRyYW5zZm9ybWF0aW9ucy5wdXNoKHJvdGF0ZVooY29uZmlnLnJvdGF0ZS56KSk7IH1cblx0XHRpZiAoY29uZmlnLnNjYWxlICE9PSAxKSB7XG5cdFx0XHRpZiAoY29uZmlnLnNjYWxlID09PSAwKSB7XG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiBUaGUgQ1NTIFRyYW5zZm9ybXMgbWF0cml4IGludGVycG9sYXRpb24gc3BlY2lmaWNhdGlvblxuXHRcdFx0XHQgKiBiYXNpY2FsbHkgZGlzYWxsb3dzIHRyYW5zaXRpb25zIG9mIG5vbi1pbnZlcnRpYmxlXG5cdFx0XHRcdCAqIG1hdHJpeGVzLCB3aGljaCBtZWFucyBicm93c2VycyB3b24ndCB0cmFuc2l0aW9uXG5cdFx0XHRcdCAqIGVsZW1lbnRzIHdpdGggemVybyBzY2FsZS5cblx0XHRcdFx0ICpcblx0XHRcdFx0ICogVGhhdOKAmXMgaW5jb252ZW5pZW50IGZvciB0aGUgQVBJIGFuZCBkZXZlbG9wZXJcblx0XHRcdFx0ICogZXhwZXJpZW5jZSwgc28gd2Ugc2ltcGx5IG51ZGdlIHRoZWlyIHZhbHVlXG5cdFx0XHRcdCAqIHNsaWdodGx5IGFib3ZlIHplcm87IHRoaXMgYWxsb3dzIGJyb3dzZXJzXG5cdFx0XHRcdCAqIHRvIHRyYW5zaXRpb24gb3VyIGVsZW1lbnQgYXMgZXhwZWN0ZWQuXG5cdFx0XHRcdCAqXG5cdFx0XHRcdCAqIGAwLjAwMDJgIHdhcyB0aGUgc21hbGxlc3QgbnVtYmVyXG5cdFx0XHRcdCAqIHRoYXQgcGVyZm9ybWVkIGFjcm9zcyBicm93c2Vycy5cblx0XHRcdFx0ICovXG5cdFx0XHRcdHRyYW5zZm9ybWF0aW9ucy5wdXNoKHNjYWxlKDAuMDAwMikpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0dHJhbnNmb3JtYXRpb25zLnB1c2goc2NhbGUoY29uZmlnLnNjYWxlKSk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0dmFyIHRyYW5zZm9ybSA9IHt9O1xuXHRcdGlmICh0cmFuc2Zvcm1hdGlvbnMubGVuZ3RoKSB7XG5cdFx0XHR0cmFuc2Zvcm0ucHJvcGVydHkgPSBnZXRQcmVmaXhlZENzc1Byb3AoJ3RyYW5zZm9ybScpO1xuXHRcdFx0LyoqXG5cdFx0XHQgKiBUaGUgZGVmYXVsdCBjb21wdXRlZCB0cmFuc2Zvcm0gdmFsdWUgc2hvdWxkIGJlIG9uZSBvZjpcblx0XHRcdCAqIHVuZGVmaW5lZCB8fCAnbm9uZScgfHwgJ21hdHJpeCgpJyB8fCAnbWF0cml4M2QoKSdcblx0XHRcdCAqL1xuXHRcdFx0dHJhbnNmb3JtLmNvbXB1dGVkID0ge1xuXHRcdFx0XHRyYXc6IGNvbXB1dGVkW3RyYW5zZm9ybS5wcm9wZXJ0eV0sXG5cdFx0XHRcdG1hdHJpeDogcGFyc2UoY29tcHV0ZWRbdHJhbnNmb3JtLnByb3BlcnR5XSlcblx0XHRcdH07XG5cblx0XHRcdHRyYW5zZm9ybWF0aW9ucy51bnNoaWZ0KHRyYW5zZm9ybS5jb21wdXRlZC5tYXRyaXgpO1xuXHRcdFx0dmFyIHByb2R1Y3QgPSB0cmFuc2Zvcm1hdGlvbnMucmVkdWNlKG11bHRpcGx5KTtcblxuXHRcdFx0dHJhbnNmb3JtLmdlbmVyYXRlZCA9IHtcblx0XHRcdFx0aW5pdGlhbDogKCh0cmFuc2Zvcm0ucHJvcGVydHkpICsgXCI6IG1hdHJpeDNkKFwiICsgKHByb2R1Y3Quam9pbignLCAnKSkgKyBcIik7XCIpLFxuXHRcdFx0XHRmaW5hbDogKCh0cmFuc2Zvcm0ucHJvcGVydHkpICsgXCI6IG1hdHJpeDNkKFwiICsgKHRyYW5zZm9ybS5jb21wdXRlZC5tYXRyaXguam9pbignLCAnKSkgKyBcIik7XCIpXG5cdFx0XHR9O1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0cmFuc2Zvcm0uZ2VuZXJhdGVkID0ge1xuXHRcdFx0XHRpbml0aWFsOiAnJyxcblx0XHRcdFx0ZmluYWw6ICcnXG5cdFx0XHR9O1xuXHRcdH1cblxuXHRcdC8qKlxuXHRcdCAqIEdlbmVyYXRlIHRyYW5zaXRpb24gc3R5bGVzXG5cdFx0ICovXG5cdFx0dmFyIHRyYW5zaXRpb24gPSB7fTtcblx0XHRpZiAob3BhY2l0eS5nZW5lcmF0ZWQgfHwgdHJhbnNmb3JtLmdlbmVyYXRlZC5pbml0aWFsKSB7XG5cdFx0XHR0cmFuc2l0aW9uLnByb3BlcnR5ID0gZ2V0UHJlZml4ZWRDc3NQcm9wKCd0cmFuc2l0aW9uJyk7XG5cdFx0XHR0cmFuc2l0aW9uLmNvbXB1dGVkID0gY29tcHV0ZWRbdHJhbnNpdGlvbi5wcm9wZXJ0eV07XG5cdFx0XHR0cmFuc2l0aW9uLmZyYWdtZW50cyA9IFtdO1xuXG5cdFx0XHR2YXIgZGVsYXkgPSBjb25maWcuZGVsYXk7XG5cdFx0XHR2YXIgZHVyYXRpb24gPSBjb25maWcuZHVyYXRpb247XG5cdFx0XHR2YXIgZWFzaW5nID0gY29uZmlnLmVhc2luZztcblxuXHRcdFx0aWYgKG9wYWNpdHkuZ2VuZXJhdGVkKSB7XG5cdFx0XHRcdHRyYW5zaXRpb24uZnJhZ21lbnRzLnB1c2goe1xuXHRcdFx0XHRcdGRlbGF5ZWQ6IChcIm9wYWNpdHkgXCIgKyAoZHVyYXRpb24gLyAxMDAwKSArIFwicyBcIiArIGVhc2luZyArIFwiIFwiICsgKGRlbGF5IC8gMTAwMCkgKyBcInNcIiksXG5cdFx0XHRcdFx0aW5zdGFudDogKFwib3BhY2l0eSBcIiArIChkdXJhdGlvbiAvIDEwMDApICsgXCJzIFwiICsgZWFzaW5nICsgXCIgMHNcIilcblx0XHRcdFx0fSk7XG5cdFx0XHR9XG5cblx0XHRcdGlmICh0cmFuc2Zvcm0uZ2VuZXJhdGVkLmluaXRpYWwpIHtcblx0XHRcdFx0dHJhbnNpdGlvbi5mcmFnbWVudHMucHVzaCh7XG5cdFx0XHRcdFx0ZGVsYXllZDogKCh0cmFuc2Zvcm0ucHJvcGVydHkpICsgXCIgXCIgKyAoZHVyYXRpb24gLyAxMDAwKSArIFwicyBcIiArIGVhc2luZyArIFwiIFwiICsgKGRlbGF5IC8gMTAwMCkgKyBcInNcIiksXG5cdFx0XHRcdFx0aW5zdGFudDogKCh0cmFuc2Zvcm0ucHJvcGVydHkpICsgXCIgXCIgKyAoZHVyYXRpb24gLyAxMDAwKSArIFwicyBcIiArIGVhc2luZyArIFwiIDBzXCIpXG5cdFx0XHRcdH0pO1xuXHRcdFx0fVxuXG5cdFx0XHQvKipcblx0XHRcdCAqIFRoZSBkZWZhdWx0IGNvbXB1dGVkIHRyYW5zaXRpb24gcHJvcGVydHkgc2hvdWxkIGJlIHVuZGVmaW5lZCwgb3Igb25lIG9mOlxuXHRcdFx0ICogJycgfHwgJ25vbmUgMHMgZWFzZSAwcycgfHwgJ2FsbCAwcyBlYXNlIDBzJyB8fCAnYWxsIDBzIDBzIGN1YmljLWJlemllcigpJ1xuXHRcdFx0ICovXG5cdFx0XHR2YXIgaGFzQ3VzdG9tVHJhbnNpdGlvbiA9XG5cdFx0XHRcdHRyYW5zaXRpb24uY29tcHV0ZWQgJiYgIXRyYW5zaXRpb24uY29tcHV0ZWQubWF0Y2goL2FsbCAwc3xub25lIDBzLyk7XG5cblx0XHRcdGlmIChoYXNDdXN0b21UcmFuc2l0aW9uKSB7XG5cdFx0XHRcdHRyYW5zaXRpb24uZnJhZ21lbnRzLnVuc2hpZnQoe1xuXHRcdFx0XHRcdGRlbGF5ZWQ6IHRyYW5zaXRpb24uY29tcHV0ZWQsXG5cdFx0XHRcdFx0aW5zdGFudDogdHJhbnNpdGlvbi5jb21wdXRlZFxuXHRcdFx0XHR9KTtcblx0XHRcdH1cblxuXHRcdFx0dmFyIGNvbXBvc2VkID0gdHJhbnNpdGlvbi5mcmFnbWVudHMucmVkdWNlKFxuXHRcdFx0XHRmdW5jdGlvbiAoY29tcG9zaXRpb24sIGZyYWdtZW50LCBpKSB7XG5cdFx0XHRcdFx0Y29tcG9zaXRpb24uZGVsYXllZCArPSBpID09PSAwID8gZnJhZ21lbnQuZGVsYXllZCA6IChcIiwgXCIgKyAoZnJhZ21lbnQuZGVsYXllZCkpO1xuXHRcdFx0XHRcdGNvbXBvc2l0aW9uLmluc3RhbnQgKz0gaSA9PT0gMCA/IGZyYWdtZW50Lmluc3RhbnQgOiAoXCIsIFwiICsgKGZyYWdtZW50Lmluc3RhbnQpKTtcblx0XHRcdFx0XHRyZXR1cm4gY29tcG9zaXRpb25cblx0XHRcdFx0fSxcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGRlbGF5ZWQ6ICcnLFxuXHRcdFx0XHRcdGluc3RhbnQ6ICcnXG5cdFx0XHRcdH1cblx0XHRcdCk7XG5cblx0XHRcdHRyYW5zaXRpb24uZ2VuZXJhdGVkID0ge1xuXHRcdFx0XHRkZWxheWVkOiAoKHRyYW5zaXRpb24ucHJvcGVydHkpICsgXCI6IFwiICsgKGNvbXBvc2VkLmRlbGF5ZWQpICsgXCI7XCIpLFxuXHRcdFx0XHRpbnN0YW50OiAoKHRyYW5zaXRpb24ucHJvcGVydHkpICsgXCI6IFwiICsgKGNvbXBvc2VkLmluc3RhbnQpICsgXCI7XCIpXG5cdFx0XHR9O1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0cmFuc2l0aW9uLmdlbmVyYXRlZCA9IHtcblx0XHRcdFx0ZGVsYXllZDogJycsXG5cdFx0XHRcdGluc3RhbnQ6ICcnXG5cdFx0XHR9O1xuXHRcdH1cblxuXHRcdHJldHVybiB7XG5cdFx0XHRpbmxpbmU6IGlubGluZSxcblx0XHRcdG9wYWNpdHk6IG9wYWNpdHksXG5cdFx0XHRwb3NpdGlvbjogcG9zaXRpb24sXG5cdFx0XHR0cmFuc2Zvcm06IHRyYW5zZm9ybSxcblx0XHRcdHRyYW5zaXRpb246IHRyYW5zaXRpb25cblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiBhbmltYXRlKGVsZW1lbnQsIGZvcmNlKSB7XG5cdFx0aWYgKCBmb3JjZSA9PT0gdm9pZCAwICkgZm9yY2UgPSB7fTtcblxuXHRcdHZhciBwcmlzdGluZSA9IGZvcmNlLnByaXN0aW5lIHx8IHRoaXMucHJpc3RpbmU7XG5cdFx0dmFyIGRlbGF5ZWQgPVxuXHRcdFx0ZWxlbWVudC5jb25maWcudXNlRGVsYXkgPT09ICdhbHdheXMnIHx8XG5cdFx0XHQoZWxlbWVudC5jb25maWcudXNlRGVsYXkgPT09ICdvbmxvYWQnICYmIHByaXN0aW5lKSB8fFxuXHRcdFx0KGVsZW1lbnQuY29uZmlnLnVzZURlbGF5ID09PSAnb25jZScgJiYgIWVsZW1lbnQuc2Vlbik7XG5cblx0XHR2YXIgc2hvdWxkUmV2ZWFsID0gZWxlbWVudC52aXNpYmxlICYmICFlbGVtZW50LnJldmVhbGVkO1xuXHRcdHZhciBzaG91bGRSZXNldCA9ICFlbGVtZW50LnZpc2libGUgJiYgZWxlbWVudC5yZXZlYWxlZCAmJiBlbGVtZW50LmNvbmZpZy5yZXNldDtcblxuXHRcdGlmIChmb3JjZS5yZXZlYWwgfHwgc2hvdWxkUmV2ZWFsKSB7XG5cdFx0XHRyZXR1cm4gdHJpZ2dlclJldmVhbC5jYWxsKHRoaXMsIGVsZW1lbnQsIGRlbGF5ZWQpXG5cdFx0fVxuXG5cdFx0aWYgKGZvcmNlLnJlc2V0IHx8IHNob3VsZFJlc2V0KSB7XG5cdFx0XHRyZXR1cm4gdHJpZ2dlclJlc2V0LmNhbGwodGhpcywgZWxlbWVudClcblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiB0cmlnZ2VyUmV2ZWFsKGVsZW1lbnQsIGRlbGF5ZWQpIHtcblx0XHR2YXIgc3R5bGVzID0gW1xuXHRcdFx0ZWxlbWVudC5zdHlsZXMuaW5saW5lLmdlbmVyYXRlZCxcblx0XHRcdGVsZW1lbnQuc3R5bGVzLm9wYWNpdHkuY29tcHV0ZWQsXG5cdFx0XHRlbGVtZW50LnN0eWxlcy50cmFuc2Zvcm0uZ2VuZXJhdGVkLmZpbmFsXG5cdFx0XTtcblx0XHRpZiAoZGVsYXllZCkge1xuXHRcdFx0c3R5bGVzLnB1c2goZWxlbWVudC5zdHlsZXMudHJhbnNpdGlvbi5nZW5lcmF0ZWQuZGVsYXllZCk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHN0eWxlcy5wdXNoKGVsZW1lbnQuc3R5bGVzLnRyYW5zaXRpb24uZ2VuZXJhdGVkLmluc3RhbnQpO1xuXHRcdH1cblx0XHRlbGVtZW50LnJldmVhbGVkID0gZWxlbWVudC5zZWVuID0gdHJ1ZTtcblx0XHRlbGVtZW50Lm5vZGUuc2V0QXR0cmlidXRlKCdzdHlsZScsIHN0eWxlcy5maWx0ZXIoZnVuY3Rpb24gKHMpIHsgcmV0dXJuIHMgIT09ICcnOyB9KS5qb2luKCcgJykpO1xuXHRcdHJlZ2lzdGVyQ2FsbGJhY2tzLmNhbGwodGhpcywgZWxlbWVudCwgZGVsYXllZCk7XG5cdH1cblxuXHRmdW5jdGlvbiB0cmlnZ2VyUmVzZXQoZWxlbWVudCkge1xuXHRcdHZhciBzdHlsZXMgPSBbXG5cdFx0XHRlbGVtZW50LnN0eWxlcy5pbmxpbmUuZ2VuZXJhdGVkLFxuXHRcdFx0ZWxlbWVudC5zdHlsZXMub3BhY2l0eS5nZW5lcmF0ZWQsXG5cdFx0XHRlbGVtZW50LnN0eWxlcy50cmFuc2Zvcm0uZ2VuZXJhdGVkLmluaXRpYWwsXG5cdFx0XHRlbGVtZW50LnN0eWxlcy50cmFuc2l0aW9uLmdlbmVyYXRlZC5pbnN0YW50XG5cdFx0XTtcblx0XHRlbGVtZW50LnJldmVhbGVkID0gZmFsc2U7XG5cdFx0ZWxlbWVudC5ub2RlLnNldEF0dHJpYnV0ZSgnc3R5bGUnLCBzdHlsZXMuZmlsdGVyKGZ1bmN0aW9uIChzKSB7IHJldHVybiBzICE9PSAnJzsgfSkuam9pbignICcpKTtcblx0XHRyZWdpc3RlckNhbGxiYWNrcy5jYWxsKHRoaXMsIGVsZW1lbnQpO1xuXHR9XG5cblx0ZnVuY3Rpb24gcmVnaXN0ZXJDYWxsYmFja3MoZWxlbWVudCwgaXNEZWxheWVkKSB7XG5cdFx0dmFyIHRoaXMkMSA9IHRoaXM7XG5cblx0XHR2YXIgZHVyYXRpb24gPSBpc0RlbGF5ZWRcblx0XHRcdD8gZWxlbWVudC5jb25maWcuZHVyYXRpb24gKyBlbGVtZW50LmNvbmZpZy5kZWxheVxuXHRcdFx0OiBlbGVtZW50LmNvbmZpZy5kdXJhdGlvbjtcblxuXHRcdHZhciBiZWZvcmVDYWxsYmFjayA9IGVsZW1lbnQucmV2ZWFsZWRcblx0XHRcdD8gZWxlbWVudC5jb25maWcuYmVmb3JlUmV2ZWFsXG5cdFx0XHQ6IGVsZW1lbnQuY29uZmlnLmJlZm9yZVJlc2V0O1xuXG5cdFx0dmFyIGFmdGVyQ2FsbGJhY2sgPSBlbGVtZW50LnJldmVhbGVkXG5cdFx0XHQ/IGVsZW1lbnQuY29uZmlnLmFmdGVyUmV2ZWFsXG5cdFx0XHQ6IGVsZW1lbnQuY29uZmlnLmFmdGVyUmVzZXQ7XG5cblx0XHR2YXIgZWxhcHNlZCA9IDA7XG5cdFx0aWYgKGVsZW1lbnQuY2FsbGJhY2tUaW1lcikge1xuXHRcdFx0ZWxhcHNlZCA9IERhdGUubm93KCkgLSBlbGVtZW50LmNhbGxiYWNrVGltZXIuc3RhcnQ7XG5cdFx0XHR3aW5kb3cuY2xlYXJUaW1lb3V0KGVsZW1lbnQuY2FsbGJhY2tUaW1lci5jbG9jayk7XG5cdFx0fVxuXG5cdFx0YmVmb3JlQ2FsbGJhY2soZWxlbWVudC5ub2RlKTtcblxuXHRcdGVsZW1lbnQuY2FsbGJhY2tUaW1lciA9IHtcblx0XHRcdHN0YXJ0OiBEYXRlLm5vdygpLFxuXHRcdFx0Y2xvY2s6IHdpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0YWZ0ZXJDYWxsYmFjayhlbGVtZW50Lm5vZGUpO1xuXHRcdFx0XHRlbGVtZW50LmNhbGxiYWNrVGltZXIgPSBudWxsO1xuXHRcdFx0XHRpZiAoZWxlbWVudC5yZXZlYWxlZCAmJiAhZWxlbWVudC5jb25maWcucmVzZXQgJiYgZWxlbWVudC5jb25maWcuY2xlYW51cCkge1xuXHRcdFx0XHRcdGNsZWFuLmNhbGwodGhpcyQxLCBlbGVtZW50Lm5vZGUpO1xuXHRcdFx0XHR9XG5cdFx0XHR9LCBkdXJhdGlvbiAtIGVsYXBzZWQpXG5cdFx0fTtcblx0fVxuXG5cdHZhciBuZXh0VW5pcXVlSWQgPSAoZnVuY3Rpb24gKCkge1xuXHRcdHZhciB1aWQgPSAwO1xuXHRcdHJldHVybiBmdW5jdGlvbiAoKSB7IHJldHVybiB1aWQrKzsgfVxuXHR9KSgpO1xuXG5cdGZ1bmN0aW9uIHNlcXVlbmNlKGVsZW1lbnQsIHByaXN0aW5lKSB7XG5cdFx0aWYgKCBwcmlzdGluZSA9PT0gdm9pZCAwICkgcHJpc3RpbmUgPSB0aGlzLnByaXN0aW5lO1xuXG5cdFx0LyoqXG5cdFx0ICogV2UgZmlyc3QgY2hlY2sgaWYgdGhlIGVsZW1lbnQgc2hvdWxkIHJlc2V0LlxuXHRcdCAqL1xuXHRcdGlmICghZWxlbWVudC52aXNpYmxlICYmIGVsZW1lbnQucmV2ZWFsZWQgJiYgZWxlbWVudC5jb25maWcucmVzZXQpIHtcblx0XHRcdHJldHVybiBhbmltYXRlLmNhbGwodGhpcywgZWxlbWVudCwgeyByZXNldDogdHJ1ZSB9KVxuXHRcdH1cblxuXHRcdHZhciBzZXEgPSB0aGlzLnN0b3JlLnNlcXVlbmNlc1tlbGVtZW50LnNlcXVlbmNlLmlkXTtcblx0XHR2YXIgaSA9IGVsZW1lbnQuc2VxdWVuY2UuaW5kZXg7XG5cblx0XHRpZiAoc2VxKSB7XG5cdFx0XHR2YXIgdmlzaWJsZSA9IG5ldyBTZXF1ZW5jZU1vZGVsKHNlcSwgJ3Zpc2libGUnLCB0aGlzLnN0b3JlKTtcblx0XHRcdHZhciByZXZlYWxlZCA9IG5ldyBTZXF1ZW5jZU1vZGVsKHNlcSwgJ3JldmVhbGVkJywgdGhpcy5zdG9yZSk7XG5cblx0XHRcdHNlcS5tb2RlbHMgPSB7IHZpc2libGU6IHZpc2libGUsIHJldmVhbGVkOiByZXZlYWxlZCB9O1xuXG5cdFx0XHQvKipcblx0XHRcdCAqIElmIHRoZSBzZXF1ZW5jZSBoYXMgbm8gcmV2ZWFsZWQgbWVtYmVycyxcblx0XHRcdCAqIHRoZW4gd2UgcmV2ZWFsIHRoZSBmaXJzdCB2aXNpYmxlIGVsZW1lbnRcblx0XHRcdCAqIHdpdGhpbiB0aGF0IHNlcXVlbmNlLlxuXHRcdFx0ICpcblx0XHRcdCAqIFRoZSBzZXF1ZW5jZSB0aGVuIGN1ZXMgYSByZWN1cnNpdmUgY2FsbFxuXHRcdFx0ICogaW4gYm90aCBkaXJlY3Rpb25zLlxuXHRcdFx0ICovXG5cdFx0XHRpZiAoIXJldmVhbGVkLmJvZHkubGVuZ3RoKSB7XG5cdFx0XHRcdHZhciBuZXh0SWQgPSBzZXEubWVtYmVyc1t2aXNpYmxlLmJvZHlbMF1dO1xuXHRcdFx0XHR2YXIgbmV4dEVsZW1lbnQgPSB0aGlzLnN0b3JlLmVsZW1lbnRzW25leHRJZF07XG5cblx0XHRcdFx0aWYgKG5leHRFbGVtZW50KSB7XG5cdFx0XHRcdFx0Y3VlLmNhbGwodGhpcywgc2VxLCB2aXNpYmxlLmJvZHlbMF0sIC0xLCBwcmlzdGluZSk7XG5cdFx0XHRcdFx0Y3VlLmNhbGwodGhpcywgc2VxLCB2aXNpYmxlLmJvZHlbMF0sICsxLCBwcmlzdGluZSk7XG5cdFx0XHRcdFx0cmV0dXJuIGFuaW1hdGUuY2FsbCh0aGlzLCBuZXh0RWxlbWVudCwgeyByZXZlYWw6IHRydWUsIHByaXN0aW5lOiBwcmlzdGluZSB9KVxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdC8qKlxuXHRcdFx0ICogSWYgb3VyIGVsZW1lbnQgaXNu4oCZdCByZXNldHRpbmcsIHdlIGNoZWNrIHRoZVxuXHRcdFx0ICogZWxlbWVudCBzZXF1ZW5jZSBpbmRleCBhZ2FpbnN0IHRoZSBoZWFkLCBhbmRcblx0XHRcdCAqIHRoZW4gdGhlIGZvb3Qgb2YgdGhlIHNlcXVlbmNlLlxuXHRcdFx0ICovXG5cdFx0XHRpZiAoXG5cdFx0XHRcdCFzZXEuYmxvY2tlZC5oZWFkICYmXG5cdFx0XHRcdGkgPT09IFtdLmNvbmNhdCggcmV2ZWFsZWQuaGVhZCApLnBvcCgpICYmXG5cdFx0XHRcdGkgPj0gW10uY29uY2F0KCB2aXNpYmxlLmJvZHkgKS5zaGlmdCgpXG5cdFx0XHQpIHtcblx0XHRcdFx0Y3VlLmNhbGwodGhpcywgc2VxLCBpLCAtMSwgcHJpc3RpbmUpO1xuXHRcdFx0XHRyZXR1cm4gYW5pbWF0ZS5jYWxsKHRoaXMsIGVsZW1lbnQsIHsgcmV2ZWFsOiB0cnVlLCBwcmlzdGluZTogcHJpc3RpbmUgfSlcblx0XHRcdH1cblxuXHRcdFx0aWYgKFxuXHRcdFx0XHQhc2VxLmJsb2NrZWQuZm9vdCAmJlxuXHRcdFx0XHRpID09PSBbXS5jb25jYXQoIHJldmVhbGVkLmZvb3QgKS5zaGlmdCgpICYmXG5cdFx0XHRcdGkgPD0gW10uY29uY2F0KCB2aXNpYmxlLmJvZHkgKS5wb3AoKVxuXHRcdFx0KSB7XG5cdFx0XHRcdGN1ZS5jYWxsKHRoaXMsIHNlcSwgaSwgKzEsIHByaXN0aW5lKTtcblx0XHRcdFx0cmV0dXJuIGFuaW1hdGUuY2FsbCh0aGlzLCBlbGVtZW50LCB7IHJldmVhbDogdHJ1ZSwgcHJpc3RpbmU6IHByaXN0aW5lIH0pXG5cdFx0XHR9XG5cdFx0fVxuXHR9XG5cblx0ZnVuY3Rpb24gU2VxdWVuY2UoaW50ZXJ2YWwpIHtcblx0XHR2YXIgaSA9IE1hdGguYWJzKGludGVydmFsKTtcblx0XHRpZiAoIWlzTmFOKGkpKSB7XG5cdFx0XHR0aGlzLmlkID0gbmV4dFVuaXF1ZUlkKCk7XG5cdFx0XHR0aGlzLmludGVydmFsID0gTWF0aC5tYXgoaSwgMTYpO1xuXHRcdFx0dGhpcy5tZW1iZXJzID0gW107XG5cdFx0XHR0aGlzLm1vZGVscyA9IHt9O1xuXHRcdFx0dGhpcy5ibG9ja2VkID0ge1xuXHRcdFx0XHRoZWFkOiBmYWxzZSxcblx0XHRcdFx0Zm9vdDogZmFsc2Vcblx0XHRcdH07XG5cdFx0fSBlbHNlIHtcblx0XHRcdHRocm93IG5ldyBSYW5nZUVycm9yKCdJbnZhbGlkIHNlcXVlbmNlIGludGVydmFsLicpXG5cdFx0fVxuXHR9XG5cblx0ZnVuY3Rpb24gU2VxdWVuY2VNb2RlbChzZXEsIHByb3AsIHN0b3JlKSB7XG5cdFx0dmFyIHRoaXMkMSA9IHRoaXM7XG5cblx0XHR0aGlzLmhlYWQgPSBbXTtcblx0XHR0aGlzLmJvZHkgPSBbXTtcblx0XHR0aGlzLmZvb3QgPSBbXTtcblxuXHRcdGVhY2goc2VxLm1lbWJlcnMsIGZ1bmN0aW9uIChpZCwgaW5kZXgpIHtcblx0XHRcdHZhciBlbGVtZW50ID0gc3RvcmUuZWxlbWVudHNbaWRdO1xuXHRcdFx0aWYgKGVsZW1lbnQgJiYgZWxlbWVudFtwcm9wXSkge1xuXHRcdFx0XHR0aGlzJDEuYm9keS5wdXNoKGluZGV4KTtcblx0XHRcdH1cblx0XHR9KTtcblxuXHRcdGlmICh0aGlzLmJvZHkubGVuZ3RoKSB7XG5cdFx0XHRlYWNoKHNlcS5tZW1iZXJzLCBmdW5jdGlvbiAoaWQsIGluZGV4KSB7XG5cdFx0XHRcdHZhciBlbGVtZW50ID0gc3RvcmUuZWxlbWVudHNbaWRdO1xuXHRcdFx0XHRpZiAoZWxlbWVudCAmJiAhZWxlbWVudFtwcm9wXSkge1xuXHRcdFx0XHRcdGlmIChpbmRleCA8IHRoaXMkMS5ib2R5WzBdKSB7XG5cdFx0XHRcdFx0XHR0aGlzJDEuaGVhZC5wdXNoKGluZGV4KTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcyQxLmZvb3QucHVzaChpbmRleCk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiBjdWUoc2VxLCBpLCBkaXJlY3Rpb24sIHByaXN0aW5lKSB7XG5cdFx0dmFyIHRoaXMkMSA9IHRoaXM7XG5cblx0XHR2YXIgYmxvY2tlZCA9IFsnaGVhZCcsIG51bGwsICdmb290J11bMSArIGRpcmVjdGlvbl07XG5cdFx0dmFyIG5leHRJZCA9IHNlcS5tZW1iZXJzW2kgKyBkaXJlY3Rpb25dO1xuXHRcdHZhciBuZXh0RWxlbWVudCA9IHRoaXMuc3RvcmUuZWxlbWVudHNbbmV4dElkXTtcblxuXHRcdHNlcS5ibG9ja2VkW2Jsb2NrZWRdID0gdHJ1ZTtcblxuXHRcdHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuXHRcdFx0c2VxLmJsb2NrZWRbYmxvY2tlZF0gPSBmYWxzZTtcblx0XHRcdGlmIChuZXh0RWxlbWVudCkge1xuXHRcdFx0XHRzZXF1ZW5jZS5jYWxsKHRoaXMkMSwgbmV4dEVsZW1lbnQsIHByaXN0aW5lKTtcblx0XHRcdH1cblx0XHR9LCBzZXEuaW50ZXJ2YWwpO1xuXHR9XG5cblx0ZnVuY3Rpb24gaW5pdGlhbGl6ZSgpIHtcblx0XHR2YXIgdGhpcyQxID0gdGhpcztcblxuXHRcdHJpbnNlLmNhbGwodGhpcyk7XG5cblx0XHRlYWNoKHRoaXMuc3RvcmUuZWxlbWVudHMsIGZ1bmN0aW9uIChlbGVtZW50KSB7XG5cdFx0XHR2YXIgc3R5bGVzID0gW2VsZW1lbnQuc3R5bGVzLmlubGluZS5nZW5lcmF0ZWRdO1xuXG5cdFx0XHRpZiAoZWxlbWVudC52aXNpYmxlKSB7XG5cdFx0XHRcdHN0eWxlcy5wdXNoKGVsZW1lbnQuc3R5bGVzLm9wYWNpdHkuY29tcHV0ZWQpO1xuXHRcdFx0XHRzdHlsZXMucHVzaChlbGVtZW50LnN0eWxlcy50cmFuc2Zvcm0uZ2VuZXJhdGVkLmZpbmFsKTtcblx0XHRcdFx0ZWxlbWVudC5yZXZlYWxlZCA9IHRydWU7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRzdHlsZXMucHVzaChlbGVtZW50LnN0eWxlcy5vcGFjaXR5LmdlbmVyYXRlZCk7XG5cdFx0XHRcdHN0eWxlcy5wdXNoKGVsZW1lbnQuc3R5bGVzLnRyYW5zZm9ybS5nZW5lcmF0ZWQuaW5pdGlhbCk7XG5cdFx0XHRcdGVsZW1lbnQucmV2ZWFsZWQgPSBmYWxzZTtcblx0XHRcdH1cblxuXHRcdFx0ZWxlbWVudC5ub2RlLnNldEF0dHJpYnV0ZSgnc3R5bGUnLCBzdHlsZXMuZmlsdGVyKGZ1bmN0aW9uIChzKSB7IHJldHVybiBzICE9PSAnJzsgfSkuam9pbignICcpKTtcblx0XHR9KTtcblxuXHRcdGVhY2godGhpcy5zdG9yZS5jb250YWluZXJzLCBmdW5jdGlvbiAoY29udGFpbmVyKSB7XG5cdFx0XHR2YXIgdGFyZ2V0ID1cblx0XHRcdFx0Y29udGFpbmVyLm5vZGUgPT09IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCA/IHdpbmRvdyA6IGNvbnRhaW5lci5ub2RlO1xuXHRcdFx0dGFyZ2V0LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHRoaXMkMS5kZWxlZ2F0ZSk7XG5cdFx0XHR0YXJnZXQuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgdGhpcyQxLmRlbGVnYXRlKTtcblx0XHR9KTtcblxuXHRcdC8qKlxuXHRcdCAqIE1hbnVhbGx5IGludm9rZSBkZWxlZ2F0ZSBvbmNlIHRvIGNhcHR1cmVcblx0XHQgKiBlbGVtZW50IGFuZCBjb250YWluZXIgZGltZW5zaW9ucywgY29udGFpbmVyXG5cdFx0ICogc2Nyb2xsIHBvc2l0aW9uLCBhbmQgdHJpZ2dlciBhbnkgdmFsaWQgcmV2ZWFsc1xuXHRcdCAqL1xuXHRcdHRoaXMuZGVsZWdhdGUoKTtcblxuXHRcdC8qKlxuXHRcdCAqIFdpcGUgYW55IGV4aXN0aW5nIGBzZXRUaW1lb3V0YCBub3dcblx0XHQgKiB0aGF0IGluaXRpYWxpemF0aW9uIGhhcyBjb21wbGV0ZWQuXG5cdFx0ICovXG5cdFx0dGhpcy5pbml0VGltZW91dCA9IG51bGw7XG5cdH1cblxuXHRmdW5jdGlvbiBpc01vYmlsZShhZ2VudCkge1xuXHRcdGlmICggYWdlbnQgPT09IHZvaWQgMCApIGFnZW50ID0gbmF2aWdhdG9yLnVzZXJBZ2VudDtcblxuXHRcdHJldHVybiAvQW5kcm9pZHxpUGhvbmV8aVBhZHxpUG9kL2kudGVzdChhZ2VudClcblx0fVxuXG5cdGZ1bmN0aW9uIGRlZXBBc3NpZ24odGFyZ2V0KSB7XG5cdFx0dmFyIHNvdXJjZXMgPSBbXSwgbGVuID0gYXJndW1lbnRzLmxlbmd0aCAtIDE7XG5cdFx0d2hpbGUgKCBsZW4tLSA+IDAgKSBzb3VyY2VzWyBsZW4gXSA9IGFyZ3VtZW50c1sgbGVuICsgMSBdO1xuXG5cdFx0aWYgKGlzT2JqZWN0KHRhcmdldCkpIHtcblx0XHRcdGVhY2goc291cmNlcywgZnVuY3Rpb24gKHNvdXJjZSkge1xuXHRcdFx0XHRlYWNoKHNvdXJjZSwgZnVuY3Rpb24gKGRhdGEsIGtleSkge1xuXHRcdFx0XHRcdGlmIChpc09iamVjdChkYXRhKSkge1xuXHRcdFx0XHRcdFx0aWYgKCF0YXJnZXRba2V5XSB8fCAhaXNPYmplY3QodGFyZ2V0W2tleV0pKSB7XG5cdFx0XHRcdFx0XHRcdHRhcmdldFtrZXldID0ge307XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRkZWVwQXNzaWduKHRhcmdldFtrZXldLCBkYXRhKTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGFyZ2V0W2tleV0gPSBkYXRhO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHR9KTtcblx0XHRcdHJldHVybiB0YXJnZXRcblx0XHR9IGVsc2Uge1xuXHRcdFx0dGhyb3cgbmV3IFR5cGVFcnJvcignVGFyZ2V0IG11c3QgYmUgYW4gb2JqZWN0IGxpdGVyYWwuJylcblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiByZXZlYWwodGFyZ2V0LCBvcHRpb25zLCBzeW5jaW5nKSB7XG5cdFx0dmFyIHRoaXMkMSA9IHRoaXM7XG5cdFx0aWYgKCBvcHRpb25zID09PSB2b2lkIDAgKSBvcHRpb25zID0ge307XG5cdFx0aWYgKCBzeW5jaW5nID09PSB2b2lkIDAgKSBzeW5jaW5nID0gZmFsc2U7XG5cblx0XHR2YXIgY29udGFpbmVyQnVmZmVyID0gW107XG5cdFx0dmFyIHNlcXVlbmNlJCQxO1xuXHRcdHZhciBpbnRlcnZhbCA9IG9wdGlvbnMuaW50ZXJ2YWwgfHwgZGVmYXVsdHMuaW50ZXJ2YWw7XG5cblx0XHR0cnkge1xuXHRcdFx0aWYgKGludGVydmFsKSB7XG5cdFx0XHRcdHNlcXVlbmNlJCQxID0gbmV3IFNlcXVlbmNlKGludGVydmFsKTtcblx0XHRcdH1cblxuXHRcdFx0dmFyIG5vZGVzID0gdGVhbGlnaHQodGFyZ2V0KTtcblx0XHRcdGlmICghbm9kZXMubGVuZ3RoKSB7XG5cdFx0XHRcdHRocm93IG5ldyBFcnJvcignSW52YWxpZCByZXZlYWwgdGFyZ2V0LicpXG5cdFx0XHR9XG5cblx0XHRcdHZhciBlbGVtZW50cyA9IG5vZGVzLnJlZHVjZShmdW5jdGlvbiAoZWxlbWVudEJ1ZmZlciwgZWxlbWVudE5vZGUpIHtcblx0XHRcdFx0dmFyIGVsZW1lbnQgPSB7fTtcblx0XHRcdFx0dmFyIGV4aXN0aW5nSWQgPSBlbGVtZW50Tm9kZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtc3ItaWQnKTtcblxuXHRcdFx0XHRpZiAoZXhpc3RpbmdJZCkge1xuXHRcdFx0XHRcdGRlZXBBc3NpZ24oZWxlbWVudCwgdGhpcyQxLnN0b3JlLmVsZW1lbnRzW2V4aXN0aW5nSWRdKTtcblxuXHRcdFx0XHRcdC8qKlxuXHRcdFx0XHRcdCAqIEluIG9yZGVyIHRvIHByZXZlbnQgcHJldmlvdXNseSBnZW5lcmF0ZWQgc3R5bGVzXG5cdFx0XHRcdFx0ICogZnJvbSB0aHJvd2luZyBvZmYgdGhlIG5ldyBzdHlsZXMsIHRoZSBzdHlsZSB0YWdcblx0XHRcdFx0XHQgKiBoYXMgdG8gYmUgcmV2ZXJ0ZWQgdG8gaXRzIHByZS1yZXZlYWwgc3RhdGUuXG5cdFx0XHRcdFx0ICovXG5cdFx0XHRcdFx0ZWxlbWVudC5ub2RlLnNldEF0dHJpYnV0ZSgnc3R5bGUnLCBlbGVtZW50LnN0eWxlcy5pbmxpbmUuY29tcHV0ZWQpO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdGVsZW1lbnQuaWQgPSBuZXh0VW5pcXVlSWQoKTtcblx0XHRcdFx0XHRlbGVtZW50Lm5vZGUgPSBlbGVtZW50Tm9kZTtcblx0XHRcdFx0XHRlbGVtZW50LnNlZW4gPSBmYWxzZTtcblx0XHRcdFx0XHRlbGVtZW50LnJldmVhbGVkID0gZmFsc2U7XG5cdFx0XHRcdFx0ZWxlbWVudC52aXNpYmxlID0gZmFsc2U7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHR2YXIgY29uZmlnID0gZGVlcEFzc2lnbih7fSwgZWxlbWVudC5jb25maWcgfHwgdGhpcyQxLmRlZmF1bHRzLCBvcHRpb25zKTtcblxuXHRcdFx0XHRpZiAoKCFjb25maWcubW9iaWxlICYmIGlzTW9iaWxlKCkpIHx8ICghY29uZmlnLmRlc2t0b3AgJiYgIWlzTW9iaWxlKCkpKSB7XG5cdFx0XHRcdFx0aWYgKGV4aXN0aW5nSWQpIHtcblx0XHRcdFx0XHRcdGNsZWFuLmNhbGwodGhpcyQxLCBlbGVtZW50KTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0cmV0dXJuIGVsZW1lbnRCdWZmZXIgLy8gc2tpcCBlbGVtZW50cyB0aGF0IGFyZSBkaXNhYmxlZFxuXHRcdFx0XHR9XG5cblx0XHRcdFx0dmFyIGNvbnRhaW5lck5vZGUgPSB0ZWFsaWdodChjb25maWcuY29udGFpbmVyKVswXTtcblx0XHRcdFx0aWYgKCFjb250YWluZXJOb2RlKSB7XG5cdFx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIGNvbnRhaW5lci4nKVxuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICghY29udGFpbmVyTm9kZS5jb250YWlucyhlbGVtZW50Tm9kZSkpIHtcblx0XHRcdFx0XHRyZXR1cm4gZWxlbWVudEJ1ZmZlciAvLyBza2lwIGVsZW1lbnRzIGZvdW5kIG91dHNpZGUgdGhlIGNvbnRhaW5lclxuXHRcdFx0XHR9XG5cblx0XHRcdFx0dmFyIGNvbnRhaW5lcklkO1xuXHRcdFx0XHR7XG5cdFx0XHRcdFx0Y29udGFpbmVySWQgPSBnZXRDb250YWluZXJJZChcblx0XHRcdFx0XHRcdGNvbnRhaW5lck5vZGUsXG5cdFx0XHRcdFx0XHRjb250YWluZXJCdWZmZXIsXG5cdFx0XHRcdFx0XHR0aGlzJDEuc3RvcmUuY29udGFpbmVyc1xuXHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0aWYgKGNvbnRhaW5lcklkID09PSBudWxsKSB7XG5cdFx0XHRcdFx0XHRjb250YWluZXJJZCA9IG5leHRVbmlxdWVJZCgpO1xuXHRcdFx0XHRcdFx0Y29udGFpbmVyQnVmZmVyLnB1c2goeyBpZDogY29udGFpbmVySWQsIG5vZGU6IGNvbnRhaW5lck5vZGUgfSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblx0XHRcdFx0ZWxlbWVudC5jb25maWcgPSBjb25maWc7XG5cdFx0XHRcdGVsZW1lbnQuY29udGFpbmVySWQgPSBjb250YWluZXJJZDtcblx0XHRcdFx0ZWxlbWVudC5zdHlsZXMgPSBzdHlsZShlbGVtZW50KTtcblxuXHRcdFx0XHRpZiAoc2VxdWVuY2UkJDEpIHtcblx0XHRcdFx0XHRlbGVtZW50LnNlcXVlbmNlID0ge1xuXHRcdFx0XHRcdFx0aWQ6IHNlcXVlbmNlJCQxLmlkLFxuXHRcdFx0XHRcdFx0aW5kZXg6IHNlcXVlbmNlJCQxLm1lbWJlcnMubGVuZ3RoXG5cdFx0XHRcdFx0fTtcblx0XHRcdFx0XHRzZXF1ZW5jZSQkMS5tZW1iZXJzLnB1c2goZWxlbWVudC5pZCk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRlbGVtZW50QnVmZmVyLnB1c2goZWxlbWVudCk7XG5cdFx0XHRcdHJldHVybiBlbGVtZW50QnVmZmVyXG5cdFx0XHR9LCBbXSk7XG5cblx0XHRcdC8qKlxuXHRcdFx0ICogTW9kaWZ5aW5nIHRoZSBET00gdmlhIHNldEF0dHJpYnV0ZSBuZWVkcyB0byBiZSBoYW5kbGVkXG5cdFx0XHQgKiBzZXBhcmF0ZWx5IGZyb20gcmVhZGluZyBjb21wdXRlZCBzdHlsZXMgaW4gdGhlIG1hcCBhYm92ZVxuXHRcdFx0ICogZm9yIHRoZSBicm93c2VyIHRvIGJhdGNoIERPTSBjaGFuZ2VzIChsaW1pdGluZyByZWZsb3dzKVxuXHRcdFx0ICovXG5cdFx0XHRlYWNoKGVsZW1lbnRzLCBmdW5jdGlvbiAoZWxlbWVudCkge1xuXHRcdFx0XHR0aGlzJDEuc3RvcmUuZWxlbWVudHNbZWxlbWVudC5pZF0gPSBlbGVtZW50O1xuXHRcdFx0XHRlbGVtZW50Lm5vZGUuc2V0QXR0cmlidXRlKCdkYXRhLXNyLWlkJywgZWxlbWVudC5pZCk7XG5cdFx0XHR9KTtcblx0XHR9IGNhdGNoIChlKSB7XG5cdFx0XHRyZXR1cm4gbG9nZ2VyLmNhbGwodGhpcywgJ1JldmVhbCBmYWlsZWQuJywgZS5tZXNzYWdlKVxuXHRcdH1cblxuXHRcdC8qKlxuXHRcdCAqIE5vdyB0aGF0IGVsZW1lbnQgc2V0LXVwIGlzIGNvbXBsZXRlLi4uXG5cdFx0ICogTGV04oCZcyBjb21taXQgYW55IGNvbnRhaW5lciBhbmQgc2VxdWVuY2UgZGF0YSB3ZSBoYXZlIHRvIHRoZSBzdG9yZS5cblx0XHQgKi9cblx0XHRlYWNoKGNvbnRhaW5lckJ1ZmZlciwgZnVuY3Rpb24gKGNvbnRhaW5lcikge1xuXHRcdFx0dGhpcyQxLnN0b3JlLmNvbnRhaW5lcnNbY29udGFpbmVyLmlkXSA9IHtcblx0XHRcdFx0aWQ6IGNvbnRhaW5lci5pZCxcblx0XHRcdFx0bm9kZTogY29udGFpbmVyLm5vZGVcblx0XHRcdH07XG5cdFx0fSk7XG5cdFx0aWYgKHNlcXVlbmNlJCQxKSB7XG5cdFx0XHR0aGlzLnN0b3JlLnNlcXVlbmNlc1tzZXF1ZW5jZSQkMS5pZF0gPSBzZXF1ZW5jZSQkMTtcblx0XHR9XG5cblx0XHQvKipcblx0XHQgKiBJZiByZXZlYWwgd2Fzbid0IGludm9rZWQgYnkgc3luYywgd2Ugd2FudCB0b1xuXHRcdCAqIG1ha2Ugc3VyZSB0byBhZGQgdGhpcyBjYWxsIHRvIHRoZSBoaXN0b3J5LlxuXHRcdCAqL1xuXHRcdGlmIChzeW5jaW5nICE9PSB0cnVlKSB7XG5cdFx0XHR0aGlzLnN0b3JlLmhpc3RvcnkucHVzaCh7IHRhcmdldDogdGFyZ2V0LCBvcHRpb25zOiBvcHRpb25zIH0pO1xuXG5cdFx0XHQvKipcblx0XHRcdCAqIFB1c2ggaW5pdGlhbGl6YXRpb24gdG8gdGhlIGV2ZW50IHF1ZXVlLCBnaXZpbmdcblx0XHRcdCAqIG11bHRpcGxlIHJldmVhbCBjYWxscyB0aW1lIHRvIGJlIGludGVycHJldGVkLlxuXHRcdFx0ICovXG5cdFx0XHRpZiAodGhpcy5pbml0VGltZW91dCkge1xuXHRcdFx0XHR3aW5kb3cuY2xlYXJUaW1lb3V0KHRoaXMuaW5pdFRpbWVvdXQpO1xuXHRcdFx0fVxuXHRcdFx0dGhpcy5pbml0VGltZW91dCA9IHdpbmRvdy5zZXRUaW1lb3V0KGluaXRpYWxpemUuYmluZCh0aGlzKSwgMCk7XG5cdFx0fVxuXHR9XG5cblx0ZnVuY3Rpb24gZ2V0Q29udGFpbmVySWQobm9kZSkge1xuXHRcdHZhciBjb2xsZWN0aW9ucyA9IFtdLCBsZW4gPSBhcmd1bWVudHMubGVuZ3RoIC0gMTtcblx0XHR3aGlsZSAoIGxlbi0tID4gMCApIGNvbGxlY3Rpb25zWyBsZW4gXSA9IGFyZ3VtZW50c1sgbGVuICsgMSBdO1xuXG5cdFx0dmFyIGlkID0gbnVsbDtcblx0XHRlYWNoKGNvbGxlY3Rpb25zLCBmdW5jdGlvbiAoY29sbGVjdGlvbikge1xuXHRcdFx0ZWFjaChjb2xsZWN0aW9uLCBmdW5jdGlvbiAoY29udGFpbmVyKSB7XG5cdFx0XHRcdGlmIChpZCA9PT0gbnVsbCAmJiBjb250YWluZXIubm9kZSA9PT0gbm9kZSkge1xuXHRcdFx0XHRcdGlkID0gY29udGFpbmVyLmlkO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9KTtcblx0XHRyZXR1cm4gaWRcblx0fVxuXG5cdC8qKlxuXHQgKiBSZS1ydW5zIHRoZSByZXZlYWwgbWV0aG9kIGZvciBlYWNoIHJlY29yZCBzdG9yZWQgaW4gaGlzdG9yeSxcblx0ICogZm9yIGNhcHR1cmluZyBuZXcgY29udGVudCBhc3luY2hyb25vdXNseSBsb2FkZWQgaW50byB0aGUgRE9NLlxuXHQgKi9cblx0ZnVuY3Rpb24gc3luYygpIHtcblx0XHR2YXIgdGhpcyQxID0gdGhpcztcblxuXHRcdGVhY2godGhpcy5zdG9yZS5oaXN0b3J5LCBmdW5jdGlvbiAocmVjb3JkKSB7XG5cdFx0XHRyZXZlYWwuY2FsbCh0aGlzJDEsIHJlY29yZC50YXJnZXQsIHJlY29yZC5vcHRpb25zLCB0cnVlKTtcblx0XHR9KTtcblxuXHRcdGluaXRpYWxpemUuY2FsbCh0aGlzKTtcblx0fVxuXG5cdHZhciBwb2x5ZmlsbCA9IGZ1bmN0aW9uICh4KSB7IHJldHVybiAoeCA+IDApIC0gKHggPCAwKSB8fCAreDsgfTtcblx0dmFyIG1hdGhTaWduID0gTWF0aC5zaWduIHx8IHBvbHlmaWxsO1xuXG5cdC8qISBAbGljZW5zZSBtaW5pcmFmIHYxLjAuMVxuXG5cdFx0Q29weXJpZ2h0IDIwMTggRmlzc3Npb24gTExDLlxuXG5cdFx0UGVybWlzc2lvbiBpcyBoZXJlYnkgZ3JhbnRlZCwgZnJlZSBvZiBjaGFyZ2UsIHRvIGFueSBwZXJzb24gb2J0YWluaW5nIGEgY29weVxuXHRcdG9mIHRoaXMgc29mdHdhcmUgYW5kIGFzc29jaWF0ZWQgZG9jdW1lbnRhdGlvbiBmaWxlcyAodGhlIFwiU29mdHdhcmVcIiksIHRvIGRlYWxcblx0XHRpbiB0aGUgU29mdHdhcmUgd2l0aG91dCByZXN0cmljdGlvbiwgaW5jbHVkaW5nIHdpdGhvdXQgbGltaXRhdGlvbiB0aGUgcmlnaHRzXG5cdFx0dG8gdXNlLCBjb3B5LCBtb2RpZnksIG1lcmdlLCBwdWJsaXNoLCBkaXN0cmlidXRlLCBzdWJsaWNlbnNlLCBhbmQvb3Igc2VsbFxuXHRcdGNvcGllcyBvZiB0aGUgU29mdHdhcmUsIGFuZCB0byBwZXJtaXQgcGVyc29ucyB0byB3aG9tIHRoZSBTb2Z0d2FyZSBpc1xuXHRcdGZ1cm5pc2hlZCB0byBkbyBzbywgc3ViamVjdCB0byB0aGUgZm9sbG93aW5nIGNvbmRpdGlvbnM6XG5cblx0XHRUaGUgYWJvdmUgY29weXJpZ2h0IG5vdGljZSBhbmQgdGhpcyBwZXJtaXNzaW9uIG5vdGljZSBzaGFsbCBiZSBpbmNsdWRlZCBpbiBhbGxcblx0XHRjb3BpZXMgb3Igc3Vic3RhbnRpYWwgcG9ydGlvbnMgb2YgdGhlIFNvZnR3YXJlLlxuXG5cdFx0VEhFIFNPRlRXQVJFIElTIFBST1ZJREVEIFwiQVMgSVNcIiwgV0lUSE9VVCBXQVJSQU5UWSBPRiBBTlkgS0lORCwgRVhQUkVTUyBPUlxuXHRcdElNUExJRUQsIElOQ0xVRElORyBCVVQgTk9UIExJTUlURUQgVE8gVEhFIFdBUlJBTlRJRVMgT0YgTUVSQ0hBTlRBQklMSVRZLFxuXHRcdEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFIEFORCBOT05JTkZSSU5HRU1FTlQuIElOIE5PIEVWRU5UIFNIQUxMIFRIRVxuXHRcdEFVVEhPUlMgT1IgQ09QWVJJR0hUIEhPTERFUlMgQkUgTElBQkxFIEZPUiBBTlkgQ0xBSU0sIERBTUFHRVMgT1IgT1RIRVJcblx0XHRMSUFCSUxJVFksIFdIRVRIRVIgSU4gQU4gQUNUSU9OIE9GIENPTlRSQUNULCBUT1JUIE9SIE9USEVSV0lTRSwgQVJJU0lORyBGUk9NLFxuXHRcdE9VVCBPRiBPUiBJTiBDT05ORUNUSU9OIFdJVEggVEhFIFNPRlRXQVJFIE9SIFRIRSBVU0UgT1IgT1RIRVIgREVBTElOR1MgSU4gVEhFXG5cdFx0U09GVFdBUkUuXG5cblx0Ki9cblx0dmFyIHBvbHlmaWxsJDEgPSAoZnVuY3Rpb24gKCkge1xuXHRcdHZhciBjbG9jayA9IERhdGUubm93KCk7XG5cblx0XHRyZXR1cm4gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG5cdFx0XHR2YXIgY3VycmVudFRpbWUgPSBEYXRlLm5vdygpO1xuXHRcdFx0aWYgKGN1cnJlbnRUaW1lIC0gY2xvY2sgPiAxNikge1xuXHRcdFx0XHRjbG9jayA9IGN1cnJlbnRUaW1lO1xuXHRcdFx0XHRjYWxsYmFjayhjdXJyZW50VGltZSk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHsgcmV0dXJuIHBvbHlmaWxsJDEoY2FsbGJhY2spOyB9LCAwKTtcblx0XHRcdH1cblx0XHR9XG5cdH0pKCk7XG5cblx0dmFyIG1pbmlyYWYgPSB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8XG5cdFx0d2luZG93LndlYmtpdFJlcXVlc3RBbmltYXRpb25GcmFtZSB8fFxuXHRcdHdpbmRvdy5tb3pSZXF1ZXN0QW5pbWF0aW9uRnJhbWUgfHxcblx0XHRwb2x5ZmlsbCQxO1xuXG5cdGZ1bmN0aW9uIGdldEdlb21ldHJ5KHRhcmdldCwgaXNDb250YWluZXIpIHtcblx0XHQvKipcblx0XHQgKiBXZSB3YW50IHRvIGlnbm9yZSBwYWRkaW5nIGFuZCBzY3JvbGxiYXJzIGZvciBjb250YWluZXIgZWxlbWVudHMuXG5cdFx0ICogTW9yZSBpbmZvcm1hdGlvbiBoZXJlOiBodHRwczovL2dvby5nbC92T1pwYnpcblx0XHQgKi9cblx0XHR2YXIgaGVpZ2h0ID0gaXNDb250YWluZXIgPyB0YXJnZXQubm9kZS5jbGllbnRIZWlnaHQgOiB0YXJnZXQubm9kZS5vZmZzZXRIZWlnaHQ7XG5cdFx0dmFyIHdpZHRoID0gaXNDb250YWluZXIgPyB0YXJnZXQubm9kZS5jbGllbnRXaWR0aCA6IHRhcmdldC5ub2RlLm9mZnNldFdpZHRoO1xuXG5cdFx0dmFyIG9mZnNldFRvcCA9IDA7XG5cdFx0dmFyIG9mZnNldExlZnQgPSAwO1xuXHRcdHZhciBub2RlID0gdGFyZ2V0Lm5vZGU7XG5cblx0XHRkbyB7XG5cdFx0XHRpZiAoIWlzTmFOKG5vZGUub2Zmc2V0VG9wKSkge1xuXHRcdFx0XHRvZmZzZXRUb3AgKz0gbm9kZS5vZmZzZXRUb3A7XG5cdFx0XHR9XG5cdFx0XHRpZiAoIWlzTmFOKG5vZGUub2Zmc2V0TGVmdCkpIHtcblx0XHRcdFx0b2Zmc2V0TGVmdCArPSBub2RlLm9mZnNldExlZnQ7XG5cdFx0XHR9XG5cdFx0XHRub2RlID0gbm9kZS5vZmZzZXRQYXJlbnQ7XG5cdFx0fSB3aGlsZSAobm9kZSlcblxuXHRcdHJldHVybiB7XG5cdFx0XHRib3VuZHM6IHtcblx0XHRcdFx0dG9wOiBvZmZzZXRUb3AsXG5cdFx0XHRcdHJpZ2h0OiBvZmZzZXRMZWZ0ICsgd2lkdGgsXG5cdFx0XHRcdGJvdHRvbTogb2Zmc2V0VG9wICsgaGVpZ2h0LFxuXHRcdFx0XHRsZWZ0OiBvZmZzZXRMZWZ0XG5cdFx0XHR9LFxuXHRcdFx0aGVpZ2h0OiBoZWlnaHQsXG5cdFx0XHR3aWR0aDogd2lkdGhcblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiBnZXRTY3JvbGxlZChjb250YWluZXIpIHtcblx0XHR2YXIgdG9wLCBsZWZ0O1xuXHRcdGlmIChjb250YWluZXIubm9kZSA9PT0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50KSB7XG5cdFx0XHR0b3AgPSB3aW5kb3cucGFnZVlPZmZzZXQ7XG5cdFx0XHRsZWZ0ID0gd2luZG93LnBhZ2VYT2Zmc2V0O1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0b3AgPSBjb250YWluZXIubm9kZS5zY3JvbGxUb3A7XG5cdFx0XHRsZWZ0ID0gY29udGFpbmVyLm5vZGUuc2Nyb2xsTGVmdDtcblx0XHR9XG5cdFx0cmV0dXJuIHsgdG9wOiB0b3AsIGxlZnQ6IGxlZnQgfVxuXHR9XG5cblx0ZnVuY3Rpb24gaXNFbGVtZW50VmlzaWJsZShlbGVtZW50KSB7XG5cdFx0aWYgKCBlbGVtZW50ID09PSB2b2lkIDAgKSBlbGVtZW50ID0ge307XG5cblx0XHR2YXIgY29udGFpbmVyID0gdGhpcy5zdG9yZS5jb250YWluZXJzW2VsZW1lbnQuY29udGFpbmVySWRdO1xuXHRcdGlmICghY29udGFpbmVyKSB7IHJldHVybiB9XG5cblx0XHR2YXIgdmlld0ZhY3RvciA9IE1hdGgubWF4KDAsIE1hdGgubWluKDEsIGVsZW1lbnQuY29uZmlnLnZpZXdGYWN0b3IpKTtcblx0XHR2YXIgdmlld09mZnNldCA9IGVsZW1lbnQuY29uZmlnLnZpZXdPZmZzZXQ7XG5cblx0XHR2YXIgZWxlbWVudEJvdW5kcyA9IHtcblx0XHRcdHRvcDogZWxlbWVudC5nZW9tZXRyeS5ib3VuZHMudG9wICsgZWxlbWVudC5nZW9tZXRyeS5oZWlnaHQgKiB2aWV3RmFjdG9yLFxuXHRcdFx0cmlnaHQ6IGVsZW1lbnQuZ2VvbWV0cnkuYm91bmRzLnJpZ2h0IC0gZWxlbWVudC5nZW9tZXRyeS53aWR0aCAqIHZpZXdGYWN0b3IsXG5cdFx0XHRib3R0b206IGVsZW1lbnQuZ2VvbWV0cnkuYm91bmRzLmJvdHRvbSAtIGVsZW1lbnQuZ2VvbWV0cnkuaGVpZ2h0ICogdmlld0ZhY3Rvcixcblx0XHRcdGxlZnQ6IGVsZW1lbnQuZ2VvbWV0cnkuYm91bmRzLmxlZnQgKyBlbGVtZW50Lmdlb21ldHJ5LndpZHRoICogdmlld0ZhY3RvclxuXHRcdH07XG5cblx0XHR2YXIgY29udGFpbmVyQm91bmRzID0ge1xuXHRcdFx0dG9wOiBjb250YWluZXIuZ2VvbWV0cnkuYm91bmRzLnRvcCArIGNvbnRhaW5lci5zY3JvbGwudG9wICsgdmlld09mZnNldC50b3AsXG5cdFx0XHRyaWdodDogY29udGFpbmVyLmdlb21ldHJ5LmJvdW5kcy5yaWdodCArIGNvbnRhaW5lci5zY3JvbGwubGVmdCAtIHZpZXdPZmZzZXQucmlnaHQsXG5cdFx0XHRib3R0b206XG5cdFx0XHRcdGNvbnRhaW5lci5nZW9tZXRyeS5ib3VuZHMuYm90dG9tICsgY29udGFpbmVyLnNjcm9sbC50b3AgLSB2aWV3T2Zmc2V0LmJvdHRvbSxcblx0XHRcdGxlZnQ6IGNvbnRhaW5lci5nZW9tZXRyeS5ib3VuZHMubGVmdCArIGNvbnRhaW5lci5zY3JvbGwubGVmdCArIHZpZXdPZmZzZXQubGVmdFxuXHRcdH07XG5cblx0XHRyZXR1cm4gKFxuXHRcdFx0KGVsZW1lbnRCb3VuZHMudG9wIDwgY29udGFpbmVyQm91bmRzLmJvdHRvbSAmJlxuXHRcdFx0XHRlbGVtZW50Qm91bmRzLnJpZ2h0ID4gY29udGFpbmVyQm91bmRzLmxlZnQgJiZcblx0XHRcdFx0ZWxlbWVudEJvdW5kcy5ib3R0b20gPiBjb250YWluZXJCb3VuZHMudG9wICYmXG5cdFx0XHRcdGVsZW1lbnRCb3VuZHMubGVmdCA8IGNvbnRhaW5lckJvdW5kcy5yaWdodCkgfHxcblx0XHRcdGVsZW1lbnQuc3R5bGVzLnBvc2l0aW9uID09PSAnZml4ZWQnXG5cdFx0KVxuXHR9XG5cblx0ZnVuY3Rpb24gZGVsZWdhdGUoXG5cdFx0ZXZlbnQsXG5cdFx0ZWxlbWVudHNcblx0KSB7XG5cdFx0dmFyIHRoaXMkMSA9IHRoaXM7XG5cdFx0aWYgKCBldmVudCA9PT0gdm9pZCAwICkgZXZlbnQgPSB7IHR5cGU6ICdpbml0JyB9O1xuXHRcdGlmICggZWxlbWVudHMgPT09IHZvaWQgMCApIGVsZW1lbnRzID0gdGhpcy5zdG9yZS5lbGVtZW50cztcblxuXHRcdG1pbmlyYWYoZnVuY3Rpb24gKCkge1xuXHRcdFx0dmFyIHN0YWxlID0gZXZlbnQudHlwZSA9PT0gJ2luaXQnIHx8IGV2ZW50LnR5cGUgPT09ICdyZXNpemUnO1xuXG5cdFx0XHRlYWNoKHRoaXMkMS5zdG9yZS5jb250YWluZXJzLCBmdW5jdGlvbiAoY29udGFpbmVyKSB7XG5cdFx0XHRcdGlmIChzdGFsZSkge1xuXHRcdFx0XHRcdGNvbnRhaW5lci5nZW9tZXRyeSA9IGdldEdlb21ldHJ5LmNhbGwodGhpcyQxLCBjb250YWluZXIsIHRydWUpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHZhciBzY3JvbGwgPSBnZXRTY3JvbGxlZC5jYWxsKHRoaXMkMSwgY29udGFpbmVyKTtcblx0XHRcdFx0aWYgKGNvbnRhaW5lci5zY3JvbGwpIHtcblx0XHRcdFx0XHRjb250YWluZXIuZGlyZWN0aW9uID0ge1xuXHRcdFx0XHRcdFx0eDogbWF0aFNpZ24oc2Nyb2xsLmxlZnQgLSBjb250YWluZXIuc2Nyb2xsLmxlZnQpLFxuXHRcdFx0XHRcdFx0eTogbWF0aFNpZ24oc2Nyb2xsLnRvcCAtIGNvbnRhaW5lci5zY3JvbGwudG9wKVxuXHRcdFx0XHRcdH07XG5cdFx0XHRcdH1cblx0XHRcdFx0Y29udGFpbmVyLnNjcm9sbCA9IHNjcm9sbDtcblx0XHRcdH0pO1xuXG5cdFx0XHQvKipcblx0XHRcdCAqIER1ZSB0byBob3cgdGhlIHNlcXVlbmNlciBpcyBpbXBsZW1lbnRlZCwgaXTigJlzXG5cdFx0XHQgKiBpbXBvcnRhbnQgdGhhdCB3ZSB1cGRhdGUgdGhlIHN0YXRlIG9mIGFsbFxuXHRcdFx0ICogZWxlbWVudHMsIGJlZm9yZSBhbnkgYW5pbWF0aW9uIGxvZ2ljIGlzXG5cdFx0XHQgKiBldmFsdWF0ZWQgKGluIHRoZSBzZWNvbmQgbG9vcCBiZWxvdykuXG5cdFx0XHQgKi9cblx0XHRcdGVhY2goZWxlbWVudHMsIGZ1bmN0aW9uIChlbGVtZW50KSB7XG5cdFx0XHRcdGlmIChzdGFsZSkge1xuXHRcdFx0XHRcdGVsZW1lbnQuZ2VvbWV0cnkgPSBnZXRHZW9tZXRyeS5jYWxsKHRoaXMkMSwgZWxlbWVudCk7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxlbWVudC52aXNpYmxlID0gaXNFbGVtZW50VmlzaWJsZS5jYWxsKHRoaXMkMSwgZWxlbWVudCk7XG5cdFx0XHR9KTtcblxuXHRcdFx0ZWFjaChlbGVtZW50cywgZnVuY3Rpb24gKGVsZW1lbnQpIHtcblx0XHRcdFx0aWYgKGVsZW1lbnQuc2VxdWVuY2UpIHtcblx0XHRcdFx0XHRzZXF1ZW5jZS5jYWxsKHRoaXMkMSwgZWxlbWVudCk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0YW5pbWF0ZS5jYWxsKHRoaXMkMSwgZWxlbWVudCk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXG5cdFx0XHR0aGlzJDEucHJpc3RpbmUgPSBmYWxzZTtcblx0XHR9KTtcblx0fVxuXG5cdGZ1bmN0aW9uIGlzVHJhbnNmb3JtU3VwcG9ydGVkKCkge1xuXHRcdHZhciBzdHlsZSA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zdHlsZTtcblx0XHRyZXR1cm4gJ3RyYW5zZm9ybScgaW4gc3R5bGUgfHwgJ1dlYmtpdFRyYW5zZm9ybScgaW4gc3R5bGVcblx0fVxuXG5cdGZ1bmN0aW9uIGlzVHJhbnNpdGlvblN1cHBvcnRlZCgpIHtcblx0XHR2YXIgc3R5bGUgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGU7XG5cdFx0cmV0dXJuICd0cmFuc2l0aW9uJyBpbiBzdHlsZSB8fCAnV2Via2l0VHJhbnNpdGlvbicgaW4gc3R5bGVcblx0fVxuXG5cdHZhciB2ZXJzaW9uID0gXCI0LjAuNlwiO1xuXG5cdHZhciBib3VuZERlbGVnYXRlO1xuXHR2YXIgYm91bmREZXN0cm95O1xuXHR2YXIgYm91bmRSZXZlYWw7XG5cdHZhciBib3VuZENsZWFuO1xuXHR2YXIgYm91bmRTeW5jO1xuXHR2YXIgY29uZmlnO1xuXHR2YXIgZGVidWc7XG5cdHZhciBpbnN0YW5jZTtcblxuXHRmdW5jdGlvbiBTY3JvbGxSZXZlYWwob3B0aW9ucykge1xuXHRcdGlmICggb3B0aW9ucyA9PT0gdm9pZCAwICkgb3B0aW9ucyA9IHt9O1xuXG5cdFx0dmFyIGludm9rZWRXaXRob3V0TmV3ID1cblx0XHRcdHR5cGVvZiB0aGlzID09PSAndW5kZWZpbmVkJyB8fFxuXHRcdFx0T2JqZWN0LmdldFByb3RvdHlwZU9mKHRoaXMpICE9PSBTY3JvbGxSZXZlYWwucHJvdG90eXBlO1xuXG5cdFx0aWYgKGludm9rZWRXaXRob3V0TmV3KSB7XG5cdFx0XHRyZXR1cm4gbmV3IFNjcm9sbFJldmVhbChvcHRpb25zKVxuXHRcdH1cblxuXHRcdGlmICghU2Nyb2xsUmV2ZWFsLmlzU3VwcG9ydGVkKCkpIHtcblx0XHRcdGxvZ2dlci5jYWxsKHRoaXMsICdJbnN0YW50aWF0aW9uIGZhaWxlZC4nLCAnVGhpcyBicm93c2VyIGlzIG5vdCBzdXBwb3J0ZWQuJyk7XG5cdFx0XHRyZXR1cm4gbW91bnQuZmFpbHVyZSgpXG5cdFx0fVxuXG5cdFx0dmFyIGJ1ZmZlcjtcblx0XHR0cnkge1xuXHRcdFx0YnVmZmVyID0gY29uZmlnXG5cdFx0XHRcdD8gZGVlcEFzc2lnbih7fSwgY29uZmlnLCBvcHRpb25zKVxuXHRcdFx0XHQ6IGRlZXBBc3NpZ24oe30sIGRlZmF1bHRzLCBvcHRpb25zKTtcblx0XHR9IGNhdGNoIChlKSB7XG5cdFx0XHRsb2dnZXIuY2FsbCh0aGlzLCAnSW52YWxpZCBjb25maWd1cmF0aW9uLicsIGUubWVzc2FnZSk7XG5cdFx0XHRyZXR1cm4gbW91bnQuZmFpbHVyZSgpXG5cdFx0fVxuXG5cdFx0dHJ5IHtcblx0XHRcdHZhciBjb250YWluZXIgPSB0ZWFsaWdodChidWZmZXIuY29udGFpbmVyKVswXTtcblx0XHRcdGlmICghY29udGFpbmVyKSB7XG5cdFx0XHRcdHRocm93IG5ldyBFcnJvcignSW52YWxpZCBjb250YWluZXIuJylcblx0XHRcdH1cblx0XHR9IGNhdGNoIChlKSB7XG5cdFx0XHRsb2dnZXIuY2FsbCh0aGlzLCBlLm1lc3NhZ2UpO1xuXHRcdFx0cmV0dXJuIG1vdW50LmZhaWx1cmUoKVxuXHRcdH1cblxuXHRcdGNvbmZpZyA9IGJ1ZmZlcjtcblxuXHRcdGlmICgoIWNvbmZpZy5tb2JpbGUgJiYgaXNNb2JpbGUoKSkgfHwgKCFjb25maWcuZGVza3RvcCAmJiAhaXNNb2JpbGUoKSkpIHtcblx0XHRcdGxvZ2dlci5jYWxsKFxuXHRcdFx0XHR0aGlzLFxuXHRcdFx0XHQnVGhpcyBkZXZpY2UgaXMgZGlzYWJsZWQuJyxcblx0XHRcdFx0KFwiZGVza3RvcDogXCIgKyAoY29uZmlnLmRlc2t0b3ApKSxcblx0XHRcdFx0KFwibW9iaWxlOiBcIiArIChjb25maWcubW9iaWxlKSlcblx0XHRcdCk7XG5cdFx0XHRyZXR1cm4gbW91bnQuZmFpbHVyZSgpXG5cdFx0fVxuXG5cdFx0bW91bnQuc3VjY2VzcygpO1xuXG5cdFx0dGhpcy5zdG9yZSA9IHtcblx0XHRcdGNvbnRhaW5lcnM6IHt9LFxuXHRcdFx0ZWxlbWVudHM6IHt9LFxuXHRcdFx0aGlzdG9yeTogW10sXG5cdFx0XHRzZXF1ZW5jZXM6IHt9XG5cdFx0fTtcblxuXHRcdHRoaXMucHJpc3RpbmUgPSB0cnVlO1xuXG5cdFx0Ym91bmREZWxlZ2F0ZSA9IGJvdW5kRGVsZWdhdGUgfHwgZGVsZWdhdGUuYmluZCh0aGlzKTtcblx0XHRib3VuZERlc3Ryb3kgPSBib3VuZERlc3Ryb3kgfHwgZGVzdHJveS5iaW5kKHRoaXMpO1xuXHRcdGJvdW5kUmV2ZWFsID0gYm91bmRSZXZlYWwgfHwgcmV2ZWFsLmJpbmQodGhpcyk7XG5cdFx0Ym91bmRDbGVhbiA9IGJvdW5kQ2xlYW4gfHwgY2xlYW4uYmluZCh0aGlzKTtcblx0XHRib3VuZFN5bmMgPSBib3VuZFN5bmMgfHwgc3luYy5iaW5kKHRoaXMpO1xuXG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsICdkZWxlZ2F0ZScsIHsgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiBib3VuZERlbGVnYXRlOyB9IH0pO1xuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLCAnZGVzdHJveScsIHsgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiBib3VuZERlc3Ryb3k7IH0gfSk7XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsICdyZXZlYWwnLCB7IGdldDogZnVuY3Rpb24gKCkgeyByZXR1cm4gYm91bmRSZXZlYWw7IH0gfSk7XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsICdjbGVhbicsIHsgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiBib3VuZENsZWFuOyB9IH0pO1xuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLCAnc3luYycsIHsgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiBib3VuZFN5bmM7IH0gfSk7XG5cblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkodGhpcywgJ2RlZmF1bHRzJywgeyBnZXQ6IGZ1bmN0aW9uICgpIHsgcmV0dXJuIGNvbmZpZzsgfSB9KTtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkodGhpcywgJ3ZlcnNpb24nLCB7IGdldDogZnVuY3Rpb24gKCkgeyByZXR1cm4gdmVyc2lvbjsgfSB9KTtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkodGhpcywgJ25vb3AnLCB7IGdldDogZnVuY3Rpb24gKCkgeyByZXR1cm4gZmFsc2U7IH0gfSk7XG5cblx0XHRyZXR1cm4gaW5zdGFuY2UgPyBpbnN0YW5jZSA6IChpbnN0YW5jZSA9IHRoaXMpXG5cdH1cblxuXHRTY3JvbGxSZXZlYWwuaXNTdXBwb3J0ZWQgPSBmdW5jdGlvbiAoKSB7IHJldHVybiBpc1RyYW5zZm9ybVN1cHBvcnRlZCgpICYmIGlzVHJhbnNpdGlvblN1cHBvcnRlZCgpOyB9O1xuXG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShTY3JvbGxSZXZlYWwsICdkZWJ1ZycsIHtcblx0XHRnZXQ6IGZ1bmN0aW9uICgpIHsgcmV0dXJuIGRlYnVnIHx8IGZhbHNlOyB9LFxuXHRcdHNldDogZnVuY3Rpb24gKHZhbHVlKSB7IHJldHVybiAoZGVidWcgPSB0eXBlb2YgdmFsdWUgPT09ICdib29sZWFuJyA/IHZhbHVlIDogZGVidWcpOyB9XG5cdH0pO1xuXG5cdFNjcm9sbFJldmVhbCgpO1xuXG5cdHJldHVybiBTY3JvbGxSZXZlYWw7XG5cbn0pKTtcbiIsIiFmdW5jdGlvbih0LG4pe1wib2JqZWN0XCI9PXR5cGVvZiBleHBvcnRzJiZcInVuZGVmaW5lZFwiIT10eXBlb2YgbW9kdWxlP21vZHVsZS5leHBvcnRzPW4oKTpcImZ1bmN0aW9uXCI9PXR5cGVvZiBkZWZpbmUmJmRlZmluZS5hbWQ/ZGVmaW5lKG4pOih0PXR8fHNlbGYpLkxhenlMb2FkPW4oKX0odGhpcywoZnVuY3Rpb24oKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiB0KCl7cmV0dXJuKHQ9T2JqZWN0LmFzc2lnbnx8ZnVuY3Rpb24odCl7Zm9yKHZhciBuPTE7bjxhcmd1bWVudHMubGVuZ3RoO24rKyl7dmFyIGU9YXJndW1lbnRzW25dO2Zvcih2YXIgaSBpbiBlKU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChlLGkpJiYodFtpXT1lW2ldKX1yZXR1cm4gdH0pLmFwcGx5KHRoaXMsYXJndW1lbnRzKX12YXIgbj1cInVuZGVmaW5lZFwiIT10eXBlb2Ygd2luZG93LGU9biYmIShcIm9uc2Nyb2xsXCJpbiB3aW5kb3cpfHxcInVuZGVmaW5lZFwiIT10eXBlb2YgbmF2aWdhdG9yJiYvKGdsZXxpbmd8cm8pYm90fGNyYXdsfHNwaWRlci9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCksaT1uJiZcIkludGVyc2VjdGlvbk9ic2VydmVyXCJpbiB3aW5kb3csbz1uJiZcImNsYXNzTGlzdFwiaW4gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInBcIiksYT1uJiZ3aW5kb3cuZGV2aWNlUGl4ZWxSYXRpbz4xLHI9e2VsZW1lbnRzX3NlbGVjdG9yOlwiaW1nXCIsY29udGFpbmVyOmV8fG4/ZG9jdW1lbnQ6bnVsbCx0aHJlc2hvbGQ6MzAwLHRocmVzaG9sZHM6bnVsbCxkYXRhX3NyYzpcInNyY1wiLGRhdGFfc3Jjc2V0Olwic3Jjc2V0XCIsZGF0YV9zaXplczpcInNpemVzXCIsZGF0YV9iZzpcImJnXCIsZGF0YV9iZ19oaWRwaTpcImJnLWhpZHBpXCIsZGF0YV9iZ19tdWx0aTpcImJnLW11bHRpXCIsZGF0YV9iZ19tdWx0aV9oaWRwaTpcImJnLW11bHRpLWhpZHBpXCIsZGF0YV9wb3N0ZXI6XCJwb3N0ZXJcIixjbGFzc19hcHBsaWVkOlwiYXBwbGllZFwiLGNsYXNzX2xvYWRpbmc6XCJsb2FkaW5nXCIsY2xhc3NfbG9hZGVkOlwibG9hZGVkXCIsY2xhc3NfZXJyb3I6XCJlcnJvclwiLGxvYWRfZGVsYXk6MCxhdXRvX3Vub2JzZXJ2ZTohMCxjYW5jZWxfb25fZXhpdDohMSxjYWxsYmFja19lbnRlcjpudWxsLGNhbGxiYWNrX2V4aXQ6bnVsbCxjYWxsYmFja19hcHBsaWVkOm51bGwsY2FsbGJhY2tfbG9hZGluZzpudWxsLGNhbGxiYWNrX2xvYWRlZDpudWxsLGNhbGxiYWNrX2Vycm9yOm51bGwsY2FsbGJhY2tfZmluaXNoOm51bGwsY2FsbGJhY2tfY2FuY2VsOm51bGwsdXNlX25hdGl2ZTohMX0sYz1mdW5jdGlvbihuKXtyZXR1cm4gdCh7fSxyLG4pfSxsPWZ1bmN0aW9uKHQsbil7dmFyIGUsaT1uZXcgdChuKTt0cnl7ZT1uZXcgQ3VzdG9tRXZlbnQoXCJMYXp5TG9hZDo6SW5pdGlhbGl6ZWRcIix7ZGV0YWlsOntpbnN0YW5jZTppfX0pfWNhdGNoKHQpeyhlPWRvY3VtZW50LmNyZWF0ZUV2ZW50KFwiQ3VzdG9tRXZlbnRcIikpLmluaXRDdXN0b21FdmVudChcIkxhenlMb2FkOjpJbml0aWFsaXplZFwiLCExLCExLHtpbnN0YW5jZTppfSl9d2luZG93LmRpc3BhdGNoRXZlbnQoZSl9LHM9ZnVuY3Rpb24odCxuKXtyZXR1cm4gdC5nZXRBdHRyaWJ1dGUoXCJkYXRhLVwiK24pfSx1PWZ1bmN0aW9uKHQsbixlKXt2YXIgaT1cImRhdGEtXCIrbjtudWxsIT09ZT90LnNldEF0dHJpYnV0ZShpLGUpOnQucmVtb3ZlQXR0cmlidXRlKGkpfSxkPWZ1bmN0aW9uKHQpe3JldHVybiBzKHQsXCJsbC1zdGF0dXNcIil9LGY9ZnVuY3Rpb24odCxuKXtyZXR1cm4gdSh0LFwibGwtc3RhdHVzXCIsbil9LF89ZnVuY3Rpb24odCl7cmV0dXJuIGYodCxudWxsKX0sZz1mdW5jdGlvbih0KXtyZXR1cm4gbnVsbD09PWQodCl9LHY9ZnVuY3Rpb24odCl7cmV0dXJuXCJkZWxheWVkXCI9PT1kKHQpfSxiPVtcImxvYWRpbmdcIixcImFwcGxpZWRcIixcImxvYWRlZFwiLFwiZXJyb3JcIl0scD1mdW5jdGlvbih0KXtyZXR1cm4gYi5pbmRleE9mKGQodCkpPi0xfSxtPWZ1bmN0aW9uKHQsbil7cmV0dXJuIHUodCxcImxsLXRpbWVvdXRcIixuKX0saD1mdW5jdGlvbih0KXtyZXR1cm4gcyh0LFwibGwtdGltZW91dFwiKX0sRT1mdW5jdGlvbih0LG4sZSxpKXt0JiYodm9pZCAwPT09aT92b2lkIDA9PT1lP3Qobik6dChuLGUpOnQobixlLGkpKX0seT1mdW5jdGlvbih0LG4pe28/dC5jbGFzc0xpc3QuYWRkKG4pOnQuY2xhc3NOYW1lKz0odC5jbGFzc05hbWU/XCIgXCI6XCJcIikrbn0sTD1mdW5jdGlvbih0LG4pe28/dC5jbGFzc0xpc3QucmVtb3ZlKG4pOnQuY2xhc3NOYW1lPXQuY2xhc3NOYW1lLnJlcGxhY2UobmV3IFJlZ0V4cChcIihefFxcXFxzKylcIituK1wiKFxcXFxzK3wkKVwiKSxcIiBcIikucmVwbGFjZSgvXlxccysvLFwiXCIpLnJlcGxhY2UoL1xccyskLyxcIlwiKX0sST1mdW5jdGlvbih0KXtyZXR1cm4gdC5sbFRlbXBJbWFnZX0saz1mdW5jdGlvbih0LG4sZSl7aWYoZSl7dmFyIGk9ZS5fb2JzZXJ2ZXI7aSYmbi5hdXRvX3Vub2JzZXJ2ZSYmaS51bm9ic2VydmUodCl9fSxBPWZ1bmN0aW9uKHQpe3QmJih0LmxvYWRpbmdDb3VudCs9MSl9LHc9ZnVuY3Rpb24odCl7Zm9yKHZhciBuLGU9W10saT0wO249dC5jaGlsZHJlbltpXTtpKz0xKVwiU09VUkNFXCI9PT1uLnRhZ05hbWUmJmUucHVzaChuKTtyZXR1cm4gZX0sej1mdW5jdGlvbih0LG4sZSl7ZSYmdC5zZXRBdHRyaWJ1dGUobixlKX0sQz1mdW5jdGlvbih0LG4pe3QucmVtb3ZlQXR0cmlidXRlKG4pfSxPPWZ1bmN0aW9uKHQpe3JldHVybiEhdC5sbE9yaWdpbmFsQXR0cnN9LHg9ZnVuY3Rpb24odCl7aWYoIU8odCkpe3ZhciBuPXt9O24uc3JjPXQuZ2V0QXR0cmlidXRlKFwic3JjXCIpLG4uc3Jjc2V0PXQuZ2V0QXR0cmlidXRlKFwic3Jjc2V0XCIpLG4uc2l6ZXM9dC5nZXRBdHRyaWJ1dGUoXCJzaXplc1wiKSx0LmxsT3JpZ2luYWxBdHRycz1ufX0sTj1mdW5jdGlvbih0KXtpZihPKHQpKXt2YXIgbj10LmxsT3JpZ2luYWxBdHRyczt6KHQsXCJzcmNcIixuLnNyYykseih0LFwic3Jjc2V0XCIsbi5zcmNzZXQpLHoodCxcInNpemVzXCIsbi5zaXplcyl9fSxNPWZ1bmN0aW9uKHQsbil7eih0LFwic2l6ZXNcIixzKHQsbi5kYXRhX3NpemVzKSkseih0LFwic3Jjc2V0XCIscyh0LG4uZGF0YV9zcmNzZXQpKSx6KHQsXCJzcmNcIixzKHQsbi5kYXRhX3NyYykpfSxSPWZ1bmN0aW9uKHQpe0ModCxcInNyY1wiKSxDKHQsXCJzcmNzZXRcIiksQyh0LFwic2l6ZXNcIil9LFQ9ZnVuY3Rpb24odCxuKXt2YXIgZT10LnBhcmVudE5vZGU7ZSYmXCJQSUNUVVJFXCI9PT1lLnRhZ05hbWUmJncoZSkuZm9yRWFjaChuKX0sRz17SU1HOmZ1bmN0aW9uKHQsbil7VCh0LChmdW5jdGlvbih0KXt4KHQpLE0odCxuKX0pKSx4KHQpLE0odCxuKX0sSUZSQU1FOmZ1bmN0aW9uKHQsbil7eih0LFwic3JjXCIscyh0LG4uZGF0YV9zcmMpKX0sVklERU86ZnVuY3Rpb24odCxuKXt3KHQpLmZvckVhY2goKGZ1bmN0aW9uKHQpe3oodCxcInNyY1wiLHModCxuLmRhdGFfc3JjKSl9KSkseih0LFwicG9zdGVyXCIscyh0LG4uZGF0YV9wb3N0ZXIpKSx6KHQsXCJzcmNcIixzKHQsbi5kYXRhX3NyYykpLHQubG9hZCgpfX0sUz1mdW5jdGlvbih0LG4sZSl7dmFyIGk9R1t0LnRhZ05hbWVdO2kmJihpKHQsbiksQShlKSx5KHQsbi5jbGFzc19sb2FkaW5nKSxmKHQsXCJsb2FkaW5nXCIpLEUobi5jYWxsYmFja19sb2FkaW5nLHQsZSksRShuLmNhbGxiYWNrX3JldmVhbCx0LGUpKX0saj1bXCJJTUdcIixcIklGUkFNRVwiLFwiVklERU9cIl0sRD1mdW5jdGlvbih0KXt0JiYodC5sb2FkaW5nQ291bnQtPTEpfSxGPWZ1bmN0aW9uKHQsbil7IW58fG4udG9Mb2FkQ291bnR8fG4ubG9hZGluZ0NvdW50fHxFKHQuY2FsbGJhY2tfZmluaXNoLG4pfSxQPWZ1bmN0aW9uKHQsbixlKXt0LmFkZEV2ZW50TGlzdGVuZXIobixlKSx0LmxsRXZMaXNucnNbbl09ZX0sVj1mdW5jdGlvbih0LG4sZSl7dC5yZW1vdmVFdmVudExpc3RlbmVyKG4sZSl9LFU9ZnVuY3Rpb24odCl7cmV0dXJuISF0LmxsRXZMaXNucnN9LCQ9ZnVuY3Rpb24odCl7aWYoVSh0KSl7dmFyIG49dC5sbEV2TGlzbnJzO2Zvcih2YXIgZSBpbiBuKXt2YXIgaT1uW2VdO1YodCxlLGkpfWRlbGV0ZSB0LmxsRXZMaXNucnN9fSxxPWZ1bmN0aW9uKHQsbixlKXshZnVuY3Rpb24odCl7ZGVsZXRlIHQubGxUZW1wSW1hZ2V9KHQpLEQoZSksTCh0LG4uY2xhc3NfbG9hZGluZyksayh0LG4sZSl9LEg9ZnVuY3Rpb24odCxuLGUpe3ZhciBpPUkodCl8fHQ7aWYoIVUoaSkpeyFmdW5jdGlvbih0LG4sZSl7VSh0KXx8KHQubGxFdkxpc25ycz17fSksUCh0LFwibG9hZFwiLG4pLFAodCxcImVycm9yXCIsZSksXCJWSURFT1wiPT09dC50YWdOYW1lJiZQKHQsXCJsb2FkZWRkYXRhXCIsbil9KGksKGZ1bmN0aW9uKG8peyFmdW5jdGlvbih0LG4sZSxpKXtxKG4sZSxpKSx5KG4sZS5jbGFzc19sb2FkZWQpLGYobixcImxvYWRlZFwiKSxFKGUuY2FsbGJhY2tfbG9hZGVkLG4saSksRihlLGkpfSgwLHQsbixlKSwkKGkpfSksKGZ1bmN0aW9uKG8peyFmdW5jdGlvbih0LG4sZSxpKXtxKG4sZSxpKSx5KG4sZS5jbGFzc19lcnJvciksZihuLFwiZXJyb3JcIiksRShlLmNhbGxiYWNrX2Vycm9yLG4saSksRihlLGkpfSgwLHQsbixlKSwkKGkpfSkpfX0sQj1mdW5jdGlvbih0KXt0JiYodC50b0xvYWRDb3VudC09MSl9LEo9ZnVuY3Rpb24odCxuLGUpeyFmdW5jdGlvbih0KXt0LmxsVGVtcEltYWdlPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIil9KHQpLEgodCxuLGUpLGZ1bmN0aW9uKHQsbixlKXt2YXIgaT1zKHQsbi5kYXRhX2JnKSxvPXModCxuLmRhdGFfYmdfaGlkcGkpLHI9YSYmbz9vOmk7ciYmKHQuc3R5bGUuYmFja2dyb3VuZEltYWdlPSd1cmwoXCInLmNvbmNhdChyLCdcIiknKSxJKHQpLnNldEF0dHJpYnV0ZShcInNyY1wiLHIpLEEoZSkseSh0LG4uY2xhc3NfbG9hZGluZyksZih0LFwibG9hZGluZ1wiKSxFKG4uY2FsbGJhY2tfbG9hZGluZyx0LGUpLEUobi5jYWxsYmFja19yZXZlYWwsdCxlKSl9KHQsbixlKSxmdW5jdGlvbih0LG4sZSl7dmFyIGk9cyh0LG4uZGF0YV9iZ19tdWx0aSksbz1zKHQsbi5kYXRhX2JnX211bHRpX2hpZHBpKSxyPWEmJm8/bzppO3ImJih0LnN0eWxlLmJhY2tncm91bmRJbWFnZT1yLHkodCxuLmNsYXNzX2FwcGxpZWQpLGYodCxcImFwcGxpZWRcIiksayh0LG4sZSksRShuLmNhbGxiYWNrX2FwcGxpZWQsdCxlKSl9KHQsbixlKX0sSz1mdW5jdGlvbih0LG4sZSl7IWZ1bmN0aW9uKHQpe3JldHVybiBqLmluZGV4T2YodC50YWdOYW1lKT4tMX0odCk/Sih0LG4sZSk6ZnVuY3Rpb24odCxuLGUpe0godCxuLGUpLFModCxuLGUpfSh0LG4sZSksQihlKSxGKG4sZSl9LFE9ZnVuY3Rpb24odCl7dmFyIG49aCh0KTtuJiYodih0KSYmXyh0KSxjbGVhclRpbWVvdXQobiksbSh0LG51bGwpKX0sVz1mdW5jdGlvbih0LG4sZSxpKXtcIklNR1wiPT09dC50YWdOYW1lJiYoJCh0KSxmdW5jdGlvbih0KXtUKHQsKGZ1bmN0aW9uKHQpe1IodCl9KSksUih0KX0odCksZnVuY3Rpb24odCl7VCh0LChmdW5jdGlvbih0KXtOKHQpfSkpLE4odCl9KHQpLEwodCxlLmNsYXNzX2xvYWRpbmcpLEQoaSksRShlLmNhbGxiYWNrX2NhbmNlbCx0LG4saSksc2V0VGltZW91dCgoZnVuY3Rpb24oKXtpLnJlc2V0RWxlbWVudFN0YXR1cyh0LGkpfSksMCkpfSxYPWZ1bmN0aW9uKHQsbixlLGkpe0UoZS5jYWxsYmFja19lbnRlcix0LG4saSkscCh0KXx8KGUubG9hZF9kZWxheT9mdW5jdGlvbih0LG4sZSl7dmFyIGk9bi5sb2FkX2RlbGF5LG89aCh0KTtvfHwobz1zZXRUaW1lb3V0KChmdW5jdGlvbigpe0sodCxuLGUpLFEodCl9KSxpKSxmKHQsXCJkZWxheWVkXCIpLG0odCxvKSl9KHQsZSxpKTpLKHQsZSxpKSl9LFk9ZnVuY3Rpb24odCxuLGUsaSl7Zyh0KXx8KGUuY2FuY2VsX29uX2V4aXQmJmZ1bmN0aW9uKHQpe3JldHVyblwibG9hZGluZ1wiPT09ZCh0KX0odCkmJlcodCxuLGUsaSksRShlLmNhbGxiYWNrX2V4aXQsdCxuLGkpLGUubG9hZF9kZWxheSYmdih0KSYmUSh0KSl9LFo9W1wiSU1HXCIsXCJJRlJBTUVcIl0sdHQ9ZnVuY3Rpb24odCl7cmV0dXJuIHQudXNlX25hdGl2ZSYmXCJsb2FkaW5nXCJpbiBIVE1MSW1hZ2VFbGVtZW50LnByb3RvdHlwZX0sbnQ9ZnVuY3Rpb24odCxuLGUpe3QuZm9yRWFjaCgoZnVuY3Rpb24odCl7LTEhPT1aLmluZGV4T2YodC50YWdOYW1lKSYmKHQuc2V0QXR0cmlidXRlKFwibG9hZGluZ1wiLFwibGF6eVwiKSxmdW5jdGlvbih0LG4sZSl7SCh0LG4sZSksUyh0LG4sZSksQihlKSxmKHQsXCJuYXRpdmVcIiksRihuLGUpfSh0LG4sZSkpfSkpLGUudG9Mb2FkQ291bnQ9MH0sZXQ9ZnVuY3Rpb24odCl7dmFyIG49dC5fc2V0dGluZ3M7aSYmIXR0KHQuX3NldHRpbmdzKSYmKHQuX29ic2VydmVyPW5ldyBJbnRlcnNlY3Rpb25PYnNlcnZlcigoZnVuY3Rpb24oZSl7IWZ1bmN0aW9uKHQsbixlKXt0LmZvckVhY2goKGZ1bmN0aW9uKHQpe3JldHVybiBmdW5jdGlvbih0KXtyZXR1cm4gdC5pc0ludGVyc2VjdGluZ3x8dC5pbnRlcnNlY3Rpb25SYXRpbz4wfSh0KT9YKHQudGFyZ2V0LHQsbixlKTpZKHQudGFyZ2V0LHQsbixlKX0pKX0oZSxuLHQpfSksZnVuY3Rpb24odCl7cmV0dXJue3Jvb3Q6dC5jb250YWluZXI9PT1kb2N1bWVudD9udWxsOnQuY29udGFpbmVyLHJvb3RNYXJnaW46dC50aHJlc2hvbGRzfHx0LnRocmVzaG9sZCtcInB4XCJ9fShuKSkpfSxpdD1mdW5jdGlvbih0KXtyZXR1cm4gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwodCl9LG90PWZ1bmN0aW9uKHQpe3JldHVybiB0LmNvbnRhaW5lci5xdWVyeVNlbGVjdG9yQWxsKHQuZWxlbWVudHNfc2VsZWN0b3IpfSxhdD1mdW5jdGlvbih0KXtyZXR1cm4gZnVuY3Rpb24odCl7cmV0dXJuXCJlcnJvclwiPT09ZCh0KX0odCl9LHJ0PWZ1bmN0aW9uKHQsbil7cmV0dXJuIGZ1bmN0aW9uKHQpe3JldHVybiBpdCh0KS5maWx0ZXIoZyl9KHR8fG90KG4pKX0sY3Q9ZnVuY3Rpb24odCl7dmFyIG4sZT10Ll9zZXR0aW5nczsobj1vdChlKSxpdChuKS5maWx0ZXIoYXQpKS5mb3JFYWNoKChmdW5jdGlvbih0KXtMKHQsZS5jbGFzc19lcnJvciksXyh0KX0pKSx0LnVwZGF0ZSgpfSxsdD1mdW5jdGlvbih0LGUpe3ZhciBpO3RoaXMuX3NldHRpbmdzPWModCksdGhpcy5sb2FkaW5nQ291bnQ9MCxldCh0aGlzKSxpPXRoaXMsbiYmd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJvbmxpbmVcIiwoZnVuY3Rpb24odCl7Y3QoaSl9KSksdGhpcy51cGRhdGUoZSl9O3JldHVybiBsdC5wcm90b3R5cGU9e3VwZGF0ZTpmdW5jdGlvbih0KXt2YXIgbixvLGE9dGhpcy5fc2V0dGluZ3Mscj1ydCh0LGEpOyh0aGlzLnRvTG9hZENvdW50PXIubGVuZ3RoLCFlJiZpKT90dChhKT9udChyLGEsdGhpcyk6KG49dGhpcy5fb2JzZXJ2ZXIsbz1yLGZ1bmN0aW9uKHQpe3QuZGlzY29ubmVjdCgpfShuKSxmdW5jdGlvbih0LG4pe24uZm9yRWFjaCgoZnVuY3Rpb24obil7dC5vYnNlcnZlKG4pfSkpfShuLG8pKTp0aGlzLmxvYWRBbGwocil9LGRlc3Ryb3k6ZnVuY3Rpb24oKXt0aGlzLl9vYnNlcnZlciYmdGhpcy5fb2JzZXJ2ZXIuZGlzY29ubmVjdCgpLGRlbGV0ZSB0aGlzLl9vYnNlcnZlcixkZWxldGUgdGhpcy5fc2V0dGluZ3MsZGVsZXRlIHRoaXMubG9hZGluZ0NvdW50LGRlbGV0ZSB0aGlzLnRvTG9hZENvdW50fSxsb2FkQWxsOmZ1bmN0aW9uKHQpe3ZhciBuPXRoaXMsZT10aGlzLl9zZXR0aW5ncztydCh0LGUpLmZvckVhY2goKGZ1bmN0aW9uKHQpe0sodCxlLG4pfSkpfSxyZXNldEVsZW1lbnRTdGF0dXM6ZnVuY3Rpb24odCl7IWZ1bmN0aW9uKHQsbil7cCh0KSYmZnVuY3Rpb24odCl7dCYmKHQudG9Mb2FkQ291bnQrPTEpfShuKSxmKHQsbnVsbCl9KHQsdGhpcyl9LGxvYWQ6ZnVuY3Rpb24odCl7Syh0LHRoaXMuX3NldHRpbmdzLHRoaXMpfX0sbHQubG9hZD1mdW5jdGlvbih0LG4pe3ZhciBlPWMobik7Syh0LGUpfSxuJiZmdW5jdGlvbih0LG4pe2lmKG4paWYobi5sZW5ndGgpZm9yKHZhciBlLGk9MDtlPW5baV07aSs9MSlsKHQsZSk7ZWxzZSBsKHQsbil9KGx0LHdpbmRvdy5sYXp5TG9hZE9wdGlvbnMpLGx0fSkpO1xyXG4iLCJpbXBvcnQgeyBET01SZWFkeU9iamVjdCB9IGZyb20gJy4uLy4uL3V0aWxzL0RPTVJlYWR5T2JqZWN0LmpzJztcclxuXHJcbi8qKlxyXG4gKiBIZWFkZXIgY29tcG9uZW50XHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgSGVhZGVyIGV4dGVuZHMgRE9NUmVhZHlPYmplY3Qge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCcxLyBIZWFkZXInLCAnY29uc3RydWN0b3IoKScpO1xyXG4gICAgICAgIHN1cGVyKCk7XHJcblx0XHR0aGlzLmNsYXNzID0gJy5oZWFkZXInO1xyXG5cdFx0dGhpcy5maXJzdE1vdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuaXNPcGVuQnVyZ2VyID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNET01SZWFkeSgpIHtcclxuICAgICAgICBzdXBlci5pc0RPTVJlYWR5KCk7XHJcbiAgICAgICAgdGhpcy5pbml0VmlldygpO1xyXG4gICAgfVxyXG5cclxuICAgIGluaXRWaWV3KCl7XHJcbiAgICAgICAgY29uc29sZS5sb2coJ0hlYWRlcicsICdpbml0VmlldygpJyk7XHJcbiAgICAgICAgdGhpcy4kZG9tID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0aGlzLmNsYXNzKTtcclxuICAgICAgICB0aGlzLiRidXJnZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuaGVhZGVyX3RvZ2dsZScpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8vU2kgb24gYSBiaWVuIHVuIGhlYWRlclxyXG4gICAgICAgIGlmICh0aGlzLiRkb20gIT0gbnVsbCl7XHJcblx0XHRcdHRoaXMuaW5pdEJ1cmdlcigpO1xyXG5cdFx0XHR0aGlzLmluaXRFdmVudHMoKTtcclxuICAgICAgICB9XHJcblx0fVxyXG5cdFxyXG5cdGluaXRFdmVudHMoKXtcdFxyXG5cclxuXHRcdC8vQXUgc2Nyb2xsIHNpIGxhIGRpcmVjdGlvbiBhIGNoYW5nw6llXHJcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ1Njcm9sbERpcmVjdGlvbl9kaXJlY3Rpb24nLCAoZSkgPT4ge1xyXG5cclxuXHRcdFx0Y29uc29sZS5sb2coZS5kZXRhaWwpO1xyXG5cclxuICAgICAgICAgICAgLy9TaSBsZSBtZW51IGVzdCBwYXMgZMOpasOgIG91dmVydFxyXG4gICAgICAgICAgICBpZiAodGhpcy4kZG9tLmNsYXNzTGlzdC5jb250YWlucygnaGVhZGVyLW9wZW4nKSA9PSBmYWxzZSkge1xyXG5cclxuICAgICAgICAgICAgICAgIC8vU2kgb24gdmEgdmVycyBsZSBoYXV0XHJcbiAgICAgICAgICAgICAgICBpZiAoZS5kZXRhaWwuZGlyZWN0aW9uID09IFwidXBcIikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC8vU2kgb24gdmEgdmVycyBsZSBiYXNcclxuICAgICAgICAgICAgICAgIC8vIG91IHF1ZSBjJ2VzdCBsZSBwcmVtaWVyIGTDqXBsYWNlbWVudCBldCBxdSdvbiBhIHN1ZmZpc2FtZW50IHNjcm9sbMOpXHJcbiAgICAgICAgICAgICAgICBlbHNlIGlmICgoZS5kZXRhaWwuZGlyZWN0aW9uID09IFwiZG93blwiKSAmJiAoZS5kZXRhaWwucG9zaXRpb24gPiAwKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cdH1cclxuXHJcblx0IC8vQWZmaWNoZXIgbGUgaGVhZGVyXHJcblx0IHNob3coKXtcclxuICAgICAgICBjb25zb2xlLmxvZygnSGVhZGVyLnNob3coKScpO1xyXG4gICAgICAgIGlmICh0aGlzLiRkb20uY2xhc3NMaXN0LmNvbnRhaW5zKCdoZWFkZXItc2hvdycpID09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLiRkb20uY2xhc3NMaXN0LmNvbnRhaW5zKCdoZWFkZXItaGlkZWZpcnN0bW92ZScpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLiRkb20uY2xhc3NMaXN0LnJlbW92ZSgnaGVhZGVyLWhpZGVmaXJzdG1vdmUnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICAgICAgdGhpcy4kZG9tLmNsYXNzTGlzdC5yZW1vdmUoJ2hlYWRlci1oaWRlJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy4kZG9tLmNsYXNzTGlzdC5hZGQoJ2hlYWRlci1zaG93Jyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vQ2FjaGVyIGxlIGhlYWRlclxyXG4gICAgaGlkZSgpe1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdIZWFkZXIuaGlkZSgpJyk7XHJcbiAgICAgICAgaWYgKHRoaXMuJGRvbS5jbGFzc0xpc3QuY29udGFpbnMoJ2hlYWRlci1zaG93JykgPT0gdHJ1ZSB8fCB0aGlzLmZpcnN0TW92ZSA9PSB0cnVlKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmZpcnN0TW92ZSA9PSB0cnVlKXtcclxuICAgICAgICAgICAgICAgIHRoaXMuJGRvbS5jbGFzc0xpc3QuYWRkKCdoZWFkZXItaGlkZWZpcnN0bW92ZScpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5maXJzdE1vdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICB0aGlzLiRkb20uY2xhc3NMaXN0LmFkZCgnaGVhZGVyLWhpZGUnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLiRkb20uY2xhc3NMaXN0LnJlbW92ZSgnaGVhZGVyLXNob3cnKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy9Ub2dnbGUgY2xhc3MgaGVhZGVyLW9wZW5idXJnZXJcclxuICAgIGluaXRCdXJnZXIoKXtcclxuICAgICAgICBjb25zb2xlLmxvZygnSGVhZGVyJywgJ2luaXRCdXJnZXInKTtcclxuICAgICAgICBpZiAodGhpcy4kYnVyZ2VyICE9IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy5ib3VuZE9uQ2xpY2tCdXJnZXIgPSBlID0+IHRoaXMub25DbGlja0J1cmdlcihlKTtcclxuICAgICAgICAgICAgdGhpcy4kYnVyZ2VyLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdGhpcy5ib3VuZE9uQ2xpY2tCdXJnZXIpO1xyXG4gICAgICAgICAgICB0aGlzLiRidXJnZXIuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLmJvdW5kT25DbGlja0J1cmdlcik7XHJcbiAgICAgICAgfSBcclxuICAgIH1cclxuXHJcbiAgICBvbkNsaWNrQnVyZ2VyKGUpe1xyXG5cclxuICAgICAgICAvL1N0YXRlXHJcbiAgICAgICAgdGhpcy5pc09wZW5CdXJnZXIgPSB0aGlzLiRkb20uY2xhc3NMaXN0LnRvZ2dsZSgnaGVhZGVyLW9wZW4nKTtcclxuICAgICAgICBjb25zb2xlLmxvZygnSGVhZGVyJywgJ29uQ2xpY2tCdXJnZXInLCAnIC8gb3BlbiBidXJnZXIgPycsIHRoaXMuaXNPcGVuQnVyZ2VyKTtcclxuXHJcbiAgICAgICAgLy9Ub2dnbGUgbm9zY3JvbGxcclxuICAgICAgICB0aGlzLnRvZ2dsZU5vU2Nyb2xsKCk7XHJcblxyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vVG9nZ2xlIG5vIHNjcm9sbCBzdXIgbGUgYm9keVxyXG4gICAgdG9nZ2xlTm9TY3JvbGwoKXtcclxuICAgICAgICBjb25zb2xlLmxvZygnSGVhZGVyLnRvZ2dsZU5vU2Nyb2xsKCknKTtcclxuICAgICAgICBpZiAodGhpcy5pc09wZW5CdXJnZXIgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgKG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiBzZXRUaW1lb3V0KHJlc29sdmUsIDUwMCkpKS50aGVuKChlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5hZGQoJ25vc2Nyb2xsJyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QucmVtb3ZlKCdub3Njcm9sbCcpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufSIsImltcG9ydCBMYXp5bG9hZCBmcm9tIFwiLi4vLi4vLi4vYnVpbGQvbm9kZV9tb2R1bGVzL3ZhbmlsbGEtbGF6eWxvYWQvZGlzdC9sYXp5bG9hZC5taW4uanNcIjtcclxuaW1wb3J0IHsgRE9NUmVhZHlPYmplY3QgfSBmcm9tIFwiLi4vLi4vdXRpbHMvRE9NUmVhZHlPYmplY3QuanNcIjtcclxuaW1wb3J0IHsgSGVhZGVyIH0gZnJvbSBcIi4uLy4uL2NvbXBvbmVudHMvaGVhZGVyL0hlYWRlclwiO1xyXG5pbXBvcnQgXCIuLi8uLi91dGlscy9WSE1vYmlsZS5qc1wiO1xyXG5pbXBvcnQgeyBTY3JvbGxEaXJlY3Rpb24gfSBmcm9tIFwiLi4vLi4vdXRpbHMvU2Nyb2xsRGlyZWN0aW9uLmpzXCI7XHJcbmltcG9ydCBEZXRhaWxzUG9seWZpbGwgZnJvbSBcIi4uLy4uLy4uL2J1aWxkL25vZGVfbW9kdWxlcy9kZXRhaWxzLWVsZW1lbnQtcG9seWZpbGwvZGlzdC9kZXRhaWxzLWVsZW1lbnQtcG9seWZpbGwuanNcIjtcclxuaW1wb3J0IFNjcm9sbFJldmVhbCBmcm9tIFwiLi4vLi4vLi4vYnVpbGQvbm9kZV9tb2R1bGVzL3Njcm9sbHJldmVhbC9kaXN0L3Njcm9sbHJldmVhbC5qc1wiO1xyXG5cclxuLyoqXHJcbiAqIEluZGV4XHJcbiAqL1xyXG5jbGFzcyBJbmRleCBleHRlbmRzIERPTVJlYWR5T2JqZWN0IHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBjb25zb2xlLmxvZygnSW5kZXguY29uc3RydWN0b3IoKScpO1xyXG4gICAgICAgIHN1cGVyKCk7XHJcblxyXG4gICAgICAgIC8vSUUgMTFcclxuICAgICAgICB0aGlzLmlzSUUxMSA9ICEhd2luZG93Lk1TSW5wdXRNZXRob2RDb250ZXh0ICYmICEhZG9jdW1lbnQuZG9jdW1lbnRNb2RlO1xyXG5cclxuICAgICAgICAvL2xhZHlsb2FkXHJcbiAgICAgICAgKHRoaXMubGF6eWxvYWQgPT09IHVuZGVmaW5lZCkgPyB0aGlzLmxhenlsb2FkID0gbmV3IExhenlsb2FkKHsgZWxlbWVudHNfc2VsZWN0b3I6IFwiLmxhenlcIiB9KSA6IHRoaXMubGF6eWxvYWQudXBkYXRlKCk7XHJcblxyXG4gICAgICAgIC8vQ3JlYXRlIGNvbXBvbmVudFxyXG4gICAgICAgIHRoaXMuc2Nyb2xsRGlyZWN0aW9uID0gbmV3IFNjcm9sbERpcmVjdGlvbigpO1xyXG4gICAgICAgIHRoaXMuaGVhZGVyICAgICAgICAgID0gbmV3IEhlYWRlcigpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHdpbmRvdy5zciAgICAgICAgICAgICAgPSBuZXcgU2Nyb2xsUmV2ZWFsKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaXNET01SZWFkeSgpIHtcclxuXHRcdGNvbnNvbGUubG9nKCdJbmRleC5pc0RPTVJlYWR5KCknKTtcclxuICAgICAgICBzdXBlci5pc0RPTVJlYWR5KCk7XHJcbiAgICAgICAgdGhpcy5hbmltYXRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgYW5pbWF0ZSgpe1xyXG4gICAgICAgIHZhciBzbGlkZURvd24gPSB7XHJcbiAgICAgICAgICAgIGRpc3RhbmNlOiAnMTUwJScsXHJcbiAgICAgICAgICAgIG9yaWdpbjogJ3RvcCcsXHJcbiAgICAgICAgICAgIGR1cmF0aW9uOiA0MDAsXHJcbiAgICAgICAgICAgIGVhc2luZzonY3ViaWMtYmV6aWVyKDAuMzkwLCAwLjU3NSwgMC41NjUsIDEuMDAwKScsXHJcbiAgICAgICAgICAgIG9wYWNpdHk6IDBcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICB2YXIgc2xpZGVVcCA9IHtcclxuICAgICAgICAgICAgZGlzdGFuY2U6ICcxNTAlJyxcclxuICAgICAgICAgICAgb3JpZ2luOiAnYm90dG9tJyxcclxuICAgICAgICAgICAgZHVyYXRpb246IDQwMCxcclxuICAgICAgICAgICAgZWFzaW5nOidjdWJpYy1iZXppZXIoMC4zOTAsIDAuNTc1LCAwLjU2NSwgMS4wMDApJyxcclxuICAgICAgICAgICAgb3BhY2l0eTogMFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHZhciBzaG93ID0ge1xyXG4gICAgICAgICAgICBkdXJhdGlvbjogMTAwMCxcclxuICAgICAgICAgICAgZWFzaW5nOidjdWJpYy1iZXppZXIoMC4zOTAsIDAuNTc1LCAwLjU2NSwgMS4wMDApJyxcclxuICAgICAgICAgICAgb3BhY2l0eTogMFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIC8vSGVyb1xyXG4gICAgICAgIHdpbmRvdy5zci5yZXZlYWwoJy5oZXJvX3RpdGxlJyxzbGlkZURvd24pO1xyXG4gICAgICAgIHdpbmRvdy5zci5yZXZlYWwoJy5oZXJvX3N1YnRpdGxlJyx7Li4uc2xpZGVEb3duLCAuLi57ZGVsYXk6NTB9fSk7XHJcbiAgICAgICAgd2luZG93LnNyLnJldmVhbCgnLmhlcm9fY3Rhcycsey4uLnNsaWRlRG93biwgLi4ue2RlbGF5OjE1MH19KTtcclxuICAgICAgICB3aW5kb3cuc3IucmV2ZWFsKCcuaGVyb19pbGx1Jyx7Li4uc2hvdywgLi4ue2RlbGF5OjAsZHVyYXRpb246NDUwfX0pO1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8vTGl2cmUgYmxhbmNcclxuICAgICAgICB3aW5kb3cuc3IucmV2ZWFsKCcubGl2cmVibGFuY19pbWFnZScsey4uLnNob3csIC4uLntkZWxheTowLGR1cmF0aW9uOjU1MH19KTtcclxuICAgICAgICB3aW5kb3cuc3IucmV2ZWFsKCcubGl2cmVibGFuY190aXRsZScsey4uLnNob3csIC4uLntkZWxheTowLGR1cmF0aW9uOjM1MH19KTtcclxuICAgICAgICB3aW5kb3cuc3IucmV2ZWFsKCcubGl2cmVibGFuY19kZXNjJyx7Li4uc2hvdywgLi4ue2RlbGF5OjE1MCxkdXJhdGlvbjozNTB9fSk7XHJcbiAgICAgICAgd2luZG93LnNyLnJldmVhbCgnLmxpdnJlYmxhbmNfY3RhJyx7Li4uc2hvdywgLi4ue2RlbGF5OjE1MCxkdXJhdGlvbjozNTB9fSk7XHJcblxyXG4gICAgICAgIC8vQXJ1Z21lbnRzXHJcbiAgICAgICAgd2luZG93LnNyLnJldmVhbCgnLmFyZ3VtZW50c19jb250YWluZXInLHsuLi5zaG93LCAuLi57ZGVsYXk6MjUwLGR1cmF0aW9uOjE1MH19KTtcclxuICAgICAgICB3aW5kb3cuc3IucmV2ZWFsKCcuYXJndW1lbnRzX2NvbCcsey4uLnNsaWRlRG93biwgLi4ue2RlbGF5OjQ1MCxkdXJhdGlvbjozNTAsZGlzdGFuY2U6JzI1JScsaW50ZXJ2YWw6IDV9fSk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy9GZWF0dXJlIHZpZMOpb1xyXG4gICAgICAgIHdpbmRvdy5zci5yZXZlYWwoJy5mZWF0dXJldmlkZW9fdGl0bGUnLHsuLi5zaG93LCAuLi57ZGVsYXk6MCxkdXJhdGlvbjozNTB9fSk7XHJcbiAgICAgICAgd2luZG93LnNyLnJldmVhbCgnLmZlYXR1cmV2aWRlb192aWRlb2NvbnRhaW5lcicsey4uLnNob3csIC4uLntkZWxheTo1MCxkdXJhdGlvbjozNTB9fSk7XHJcbiAgICBcclxuICAgICAgICAvL0ZlYXR1cmVzXHJcbiAgICAgICAgd2luZG93LnNyLnJldmVhbCgnLmZlYXR1cmVzX3RpdGxlJyx7Li4uc2hvdywgLi4ue2RlbGF5OjAsZHVyYXRpb246MzUwfX0pO1xyXG4gICAgICAgIHdpbmRvdy5zci5yZXZlYWwoJy5mZWF0dXJlc19pdGVtJyx7Li4uc2hvdywgLi4ue2RlbGF5OjUwLGR1cmF0aW9uOjM1MCwgZGlzdGFuY2U6JzUwJScsaW50ZXJ2YWw6IDV9fSk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy9HZXRsZWFkXHJcbiAgICAgICAgd2luZG93LnNyLnJldmVhbCgnLmdldGxlYWRfdGl0bGUnLHsuLi5zbGlkZURvd24sIC4uLntkZWxheTowLGR1cmF0aW9uOjM1MH19KTtcclxuICAgICAgICB3aW5kb3cuc3IucmV2ZWFsKCcuZ2V0bGVhZF9zdWJ0aXRsZScsey4uLnNsaWRlRG93biwgLi4ue2RlbGF5OjE1MCxkdXJhdGlvbjozNTB9fSk7XHJcbiAgICAgICAgd2luZG93LnNyLnJldmVhbCgnLmdldGxlYWRlcl9jdGEnLHsuLi5zbGlkZURvd24sIC4uLntkZWxheToxNTAsZHVyYXRpb246MzUwfX0pO1xyXG4gICAgICAgIC8vd2luZG93LnNyLnJldmVhbCgnLmdldGxlYWRlcl9pbGx1Jyx7Li4uc2hvdywgLi4ue2RlbGF5OjAsZHVyYXRpb246NTUwfX0pO1xyXG5cclxuICAgICAgICAvL0Fib3V0XHJcbiAgICAgICAgd2luZG93LnNyLnJldmVhbCgnLndob3dlYXJlX2ltYWdlJyx7Li4uc2hvdywgLi4ue2RlbGF5OjAsZHVyYXRpb246NTUwfX0pO1xyXG4gICAgICAgIHdpbmRvdy5zci5yZXZlYWwoJy53aG93ZWFyZV90aXRsZScsey4uLnNob3csIC4uLntkZWxheTowLGR1cmF0aW9uOjM1MH19KTtcclxuICAgICAgICB3aW5kb3cuc3IucmV2ZWFsKCcud2hvd2VhcmVfZGVzYycsey4uLnNob3csIC4uLntkZWxheToxNTAsZHVyYXRpb246MzUwfX0pO1xyXG4gICAgICAgIHdpbmRvdy5zci5yZXZlYWwoJy53aG93ZWFyZV9jdGEnLHsuLi5zaG93LCAuLi57ZGVsYXk6MTUwLGR1cmF0aW9uOjM1MH19KTtcclxuICAgICAgICBcclxuICAgICAgICAvL1Rlc3RpbW9uaWFsc1xyXG4gICAgICAgIHdpbmRvdy5zci5yZXZlYWwoJy50ZXN0aW1vbmlhbHNfdGl0bGUnLHsuLi5zaG93LCAuLi57ZGVsYXk6MCxkdXJhdGlvbjozNTB9fSk7XHJcbiAgICAgICAgd2luZG93LnNyLnJldmVhbCgnLnRlc3RpbW9uaWFsc19pdGVtJyx7Li4uc2hvdywgLi4ue2RlbGF5OjE1MCxkdXJhdGlvbjozNTAsaW50ZXJ2YWw6IDV9fSk7XHJcblxyXG4gICAgICAgIC8vUGFydG5lcnNcclxuICAgICAgICB3aW5kb3cuc3IucmV2ZWFsKCcucGFydG5lcnNfdGl0bGUnLHsuLi5zaG93LCAuLi57ZGVsYXk6MCxkdXJhdGlvbjozNTB9fSk7XHJcbiAgICAgICAgd2luZG93LnNyLnJldmVhbCgnLnBhcnRuZXJzX2l0ZW0nLHsuLi5zaG93LCAuLi57ZGVsYXk6MTUwLGR1cmF0aW9uOjM1MCxpbnRlcnZhbDogNX19KTtcclxuICAgICAgICBcclxuICAgICAgICAvL1RpbWVsaW5lXHJcbiAgICAgICAgd2luZG93LnNyLnJldmVhbCgnLnRpbWVsaW5lX3RpdGxlJyx7Li4uc2hvdywgLi4ue2RlbGF5OjAsZHVyYXRpb246MzUwfX0pO1xyXG4gICAgICAgIHdpbmRvdy5zci5yZXZlYWwoJy50aW1lbGluZV9zdGFydCcsey4uLnNob3csIC4uLntkZWxheToxNTAsZHVyYXRpb246MzUwfX0pO1xyXG4gICAgICAgIHdpbmRvdy5zci5yZXZlYWwoJy50aW1lbGluZV9pdGVtJyx7Li4uc2hvdywgLi4ue2RlbGF5OjE1MCxkdXJhdGlvbjozNTB9fSk7XHJcbiAgICAgICAgd2luZG93LnNyLnJldmVhbCgnLnRpbWVsaW5lX2VuZCcsey4uLnNob3csIC4uLntkZWxheToxNTAsZHVyYXRpb246MzUwfX0pO1xyXG5cclxuICAgICAgICAvL0FmdGVyXHJcbiAgICAgICAgd2luZG93LnNyLnJldmVhbCgnLmFmdGVyX3N1YnRpdGxlJyx7Li4uc2hvdywgLi4ue2RlbGF5OjAsZHVyYXRpb246MzUwfX0pO1xyXG4gICAgICAgIHdpbmRvdy5zci5yZXZlYWwoJy5hZnRlcl9zdWJ0aXRsZScsey4uLnNob3csIC4uLntkZWxheToxNTAsZHVyYXRpb246MzUwfX0pO1xyXG4gICAgICAgIHdpbmRvdy5zci5yZXZlYWwoJy5hZnRlcl9pdGVtJyx7Li4uc2hvdywgLi4ue2RlbGF5OjE1MCxkdXJhdGlvbjozNTAsaW50ZXJ2YWw6IDV9fSk7XHJcbiAgICAgICAgXHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBpbmRleCA9IG5ldyBJbmRleCgpOyIsImltcG9ydCBnZW5lcmFsTGF5b3V0IGZyb20gXCIuLi9sYXlvdXQvZGVmYXVsdC9JbmRleC5qc1wiOyIsIi8vRG9jdW1lbnQgcmVhZHkgUHJvbWlzZVxyXG5kb2N1bWVudC5yZWFkeSA9ICgpID0+IHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZShyZXNvbHZlID0+IHtcclxuICAgICAgICBpZiAoZG9jdW1lbnQucmVhZHlTdGF0ZSA9PT0gJ2NvbXBsZXRlJykge1xyXG4gICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIHJlc29sdmUpO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG59O1xyXG5cclxuLyoqXHJcbiAqIERPTVJlYWR5T2JqZWN0XHJcbiAqIGFic3RyYWN0IGNsYXNzXHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgRE9NUmVhZHlPYmplY3Qge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgLy9PbiBuZSBwZXV0IHBhcyBpbnN0YW5jaWVyIGxhIGNsYXNzIGRpcmVjdGVtZW50XHJcbiAgICAgICAgaWYgKHRoaXMuY29uc3RydWN0b3IgPT09IERPTVJlYWR5T2JqZWN0KSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcclxuICAgICAgICAgICAgICAgICdDYW5ub3QgY29uc3RydWN0IERPTVJlYWR5T2JqZWN0IGluc3RhbmNlcyBkaXJlY3RseSdcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGRvY3VtZW50LnJlYWR5KCkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNET01SZWFkeSgpO1xyXG4gICAgICAgIH0pLmNhdGNoKGVyciA9PiBjb25zb2xlLndhcm4oZXJyKSk7XHJcbiAgICB9XHJcblxyXG4gICAgaXNET01SZWFkeSgpIHsgfVxyXG59XHJcbiIsImV4cG9ydCBjbGFzcyBTY3JvbGxEaXJlY3Rpb24ge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMuc2Nyb2xsRGF0YSA9IHtcclxuICAgICAgICAgICAgcG9zaXRpb246IG51bGwsXHJcbiAgICAgICAgICAgIGRpcmVjdGlvbjogbnVsbCxcclxuICAgICAgICAgICAgaGFzRGlyZWN0aW9uQ2hhbmdlOiBmYWxzZVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIChlKSA9PiB0aGlzLnNldFNjcm9sbERpcmVjdGlvbihlKSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0U2Nyb2xsRGlyZWN0aW9uKGUpIHtcclxuICAgICAgICBsZXQgbmV3U2Nyb2xsRGF0YSA9IHt9O1xyXG4gICAgICAgIG5ld1Njcm9sbERhdGEucG9zaXRpb24gPSAod2luZG93LnBhZ2VZT2Zmc2V0IHx8IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3ApIC0gKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRUb3AgfHwgMCk7XHJcblxyXG4gICAgICAgIC8vT24gc3RvY2tlIGxhIGRpcmVjdGlvblxyXG4gICAgICAgIGlmIChuZXdTY3JvbGxEYXRhLnBvc2l0aW9uID4gdGhpcy5zY3JvbGxEYXRhLnBvc2l0aW9uKSB7XHJcbiAgICAgICAgICAgIG5ld1Njcm9sbERhdGEuZGlyZWN0aW9uID0gJ2Rvd24nO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIG5ld1Njcm9sbERhdGEuZGlyZWN0aW9uID0gJ3VwJztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vU2kgb24gY2hhbmdlIGRlIGRpcmVjdGlvblxyXG4gICAgICAgIGlmICh0aGlzLnNjcm9sbERhdGEuZGlyZWN0aW9uICE9IG5ld1Njcm9sbERhdGEuZGlyZWN0aW9uKSB7XHJcbiAgICAgICAgICAgIG5ld1Njcm9sbERhdGEuaGFzRGlyZWN0aW9uQ2hhbmdlID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgIC8vT24gZGlzcGF0Y2ggdW4gZXZlbnQgbG9yc3F1ZSBsYSBkaXJlY3Rpb24gZHUgc2Nyb2xsIGEgY2hhbmfDqWVcclxuICAgICAgICAgICAgd2luZG93LmRpc3BhdGNoRXZlbnQobmV3IEN1c3RvbUV2ZW50KCdTY3JvbGxEaXJlY3Rpb25fZGlyZWN0aW9uJywgeyBkZXRhaWw6IG5ld1Njcm9sbERhdGEgfSkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy9PbiBkaXNwYXRjaCB1biBldmVudCBwb3VyIGNoYXF1ZSBtb3V2ZW1lbnQgZHUgc2Nyb2xsXHJcbiAgICAgICAgd2luZG93LmRpc3BhdGNoRXZlbnQobmV3IEN1c3RvbUV2ZW50KCdTY3JvbGxEaXJlY3Rpb25fc2Nyb2xsJywgeyBkZXRhaWw6IG5ld1Njcm9sbERhdGEgfSkpO1xyXG5cclxuICAgICAgICAvL09uIHN0b2NrZSBsZXMgZG9ubsOpZXNcclxuICAgICAgICB0aGlzLnNjcm9sbERhdGEgPSBuZXdTY3JvbGxEYXRhO1xyXG4gICAgfVxyXG59IiwiLyoqXHJcbiAqIEZyb20gaHR0cHM6Ly9jc3MtdHJpY2tzLmNvbS90aGUtdHJpY2stdG8tdmlld3BvcnQtdW5pdHMtb24tbW9iaWxlL1xyXG4gKi9cclxuXHJcbmxldCB2aCA9IHdpbmRvdy5pbm5lckhlaWdodCAqIDAuMDE7XHJcbi8vIFRoZW4gd2Ugc2V0IHRoZSB2YWx1ZSBpbiB0aGUgLS12aCBjdXN0b20gcHJvcGVydHkgdG8gdGhlIHJvb3Qgb2YgdGhlIGRvY3VtZW50XHJcbmRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zdHlsZS5zZXRQcm9wZXJ0eSgnLS12aCcsIGAke3ZofXB4YCk7XHJcblxyXG4vLyBXZSBsaXN0ZW4gdG8gdGhlIHJlc2l6ZSBldmVudFxyXG53aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgKCkgPT4ge1xyXG4gICAgLy8gV2UgZXhlY3V0ZSB0aGUgc2FtZSBzY3JpcHQgYXMgYmVmb3JlXHJcbiAgICBsZXQgdmggPSB3aW5kb3cuaW5uZXJIZWlnaHQgKiAwLjAxO1xyXG4gICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnN0eWxlLnNldFByb3BlcnR5KCctLXZoJywgYCR7dmh9cHhgKTtcclxufSk7XHJcbiJdfQ==
