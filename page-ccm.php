<?php
	/*
	Template Name: Comment ca marche
	*/
	
	get_header();

	get_template_part('src/components/hero/hero');
	get_template_part('src/components/featurevideo/featurevideo');
	get_template_part('src/components/timeline/timeline');
	get_template_part('src/components/after/after');

	set_query_var( 'getlead_prefix', 'get_lead3_');
	get_template_part('src/components/getlead/getlead');
?>

<?php 

	if ( have_posts() ) : 
		while ( have_posts() ) : the_post();
			the_content();
		endwhile;  
	endif; 
?>

<?php
	get_footer();
?>